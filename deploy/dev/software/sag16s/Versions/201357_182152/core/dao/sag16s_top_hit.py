'''
This module is for SAGTopHit BLAST 16s analysis ORM objects

This is created for both BLAST 16s analysis and Contam analysis types
Created on Nov 16, 2012

@author: asyed
'''

from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, Float
from sqlalchemy.dialects.mysql import DATETIME, VARCHAR
from core.dao.base import ORMBase
from datetime import datetime



class SAGTopHit(ORMBase.globalBase):
    __tablename__ = 'sag16s_top_hit'
    __table_args__= {'mysql_engine':'InnoDB'} # PLEASE DONT REMOVE THIS, MySQL is configured by default as MyISAM on JGI-Debian

    ANALYSIS_TYPE_HOMOLOGY = "homology"
    ANALYSIS_TYPE_CONTAM   = "contam"
    
    top_hit_id       = Column(Integer, primary_key=True)
    analysis_type    = Column(VARCHAR(20), nullable=False)
    query_name         = Column(VARCHAR(200), nullable=False)
    sag_seq_id       = Column(Integer, nullable=False)
    subject_id       = Column(VARCHAR(250), nullable=False)
    percenti         = Column(Float)
    align_length     = Column(Integer)
    mismatches       = Column(Integer)
    gaps             = Column(Integer)
    q_start          = Column(Integer)
    q_end            = Column(Integer)
    s_start          = Column(Integer)
    s_end            = Column(Integer)
    evalue           = Column(Float, nullable=False)
    score            = Column(Integer, nullable=False)
    phylogeny        = Column(VARCHAR(250))
    fs_location      = Column(VARCHAR(1024), nullable=False)
    dt_data_modified  = Column(DATETIME, nullable=True, onupdate=datetime.now)


    def __init__(self, atype, query_name, ssid, subject, pi, length, mismatches, gaps, qstart, qend, sstart, send, evalue, score, phy, loc, mod):
        self.analysis_type = atype
        self.query_name = query_name
        self.sag_seq_id  = ssid
        self.subject_id = subject
        self.score = score
        self.evalue = evalue
        self.q_start = qstart
        self.q_end = qend
        self.s_start = sstart
        self.s_end = send
        self.percenti = pi
        self.mismatches = mismatches
        self.align_length = length
        self.gaps = gaps
        self.phylogeny=phy
        self.fs_location = loc
        self.dt_data_modified = mod
        
        
    def __repr__(self):
        return "Sag Seq ORM object"

#
# ORM base to create the tables
#ormBase = ORMBase()
#ormBase.getGlobalBase().metadata.create_all(ormBase.getGlobalEngine())
ORMBase.globalBase.metadata.create_all(ORMBase.globalEngine)


