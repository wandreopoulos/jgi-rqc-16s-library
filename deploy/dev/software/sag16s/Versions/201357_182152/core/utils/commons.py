'''
Created on Dec 8, 2012

@author: asyed
'''

from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from core.dao.sag16s_seq import SAGSequence
from sqlalchemy.exc import DatabaseError, SQLAlchemyError, InvalidRequestError
import MySQLdb


def getSag16sFaName(prefix):
    return prefix+".fasta"


def getSag16sQualName(prefix):
    return prefix+".fasta.qual"


def getSag16sPhdName(prefix):
    return prefix+".phd.1"


def getSag16sAb1Name(prefix):
    return prefix+".ab1"


def getSag16sSeqName(prefix):
    return prefix+".seq"


def getSag16sBlastName(prefix):
    return prefix+".blast"


def getSag16sBlastContamName(prefix):
    return prefix+".contam.blast"


def getSag16sBlastRDPName(prefix):
    return prefix+".rdp"


def getSag16sTrimFaName(prefix):
    return prefix+".fasta.trim"


def getSag16sTrimQualName(prefix):
    return prefix+".fasta.trim.qual"


def getSag16sTrimName(prefix):
    return prefix+".trim.bad.txt"
    
def getSag16sTopHitName(prefix):
    return prefix+".tophit.fasta"

def isDuplicateSagPattern(prefix, logger):
    session=None
    count = 0
    retVal  = True
    try:
        logger.info("Retrieving records for prefix: "+prefix)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        count = session.query(SAGSequence).filter(SAGSequence.seq_name == prefix).count()
        logger.info("Num or elements retrieved "+str(count))
    except DatabaseError as er:
        session.rollback()
        logger.error(  "Database Exception while looking up prefix: "+prefix)
        retVal  = False
    except SQLAlchemyError as er:
        session.rollback()
        logger.error(  "SQLAlchemy Exception while looking up prefix: "+prefix)
        retVal  = False
    except InvalidRequestError as er:
        session.rollback()
        logger.error(  "InvalidRequest Exception while looking up prefix: "+prefix)
        retVal  = False
    except:
        logger.error(  "Exception while looking up prefix: "+prefix+" Error ")
    finally:
        if(session != None):
            session.close()
            
    if( count == 0 ):
        retVal  = False
    logger.info("Return value is "+str(retVal))
    return retVal


def getHitsSQL(proposal, project, sample, userName, pcrPlate=None, sortWindow=None, wellType=None, sorti=None, sortd=None):
    sqlSearchPhrase = ''
    if (proposal != None and proposal != ''):
        sqlSearchPhrase = sqlSearchPhrase+" AND proposal_id LIKE '%"+proposal+"%' "
    if (project != None and project != ''):
        sqlSearchPhrase = sqlSearchPhrase+" AND project LIKE '%"+project+"%' "
    if (sample != None and sample != ''):
        sqlSearchPhrase = sqlSearchPhrase+" AND sample LIKE '%"+sample+"%' "
    if userName is not None and userName is not '':
        sqlSearchPhrase = sqlSearchPhrase+" AND user_name LIKE '%"+userName+"%' "
    if pcrPlate is not None and pcrPlate is not '':
        sqlSearchPhrase = sqlSearchPhrase+" AND pcr_plate LIKE '%"+pcrPlate+"%' "
    if sortWindow is not None and sortWindow is not '':
        sqlSearchPhrase = sqlSearchPhrase+" AND sort_window LIKE '%"+sortWindow+"%' "
    if wellType is not None and wellType is not '':
        sqlSearchPhrase = sqlSearchPhrase+" AND well_type LIKE '%"+wellType+"%' "

    sql = ('SELECT sag_seq_id, seq_name, user_name, proposal_id, project, sample, sort_window, plate_num, mda_plate_well_pos, well_type, sanger_well_pos, sanger_well_loc, '+
           ' q15_tlength, seq_name_marked, fs_location, dt_status_modified, pcr_plate, run_type FROM sag16s_seq WHERE (ss_status_cv_id=10) '+sqlSearchPhrase)
    
    if sorti is not None:
        sql = sql+' ORDER BY '+sorti
        if sortd is not None:
            sql = sql+' '+sortd
    else:
        sql = sql+' ORDER BY sag_seq_id'    
    return sql
    
    
if __name__ == '__main__':
    pass
