
class SAGFile(object):
    
    SAG_FILE_TYPE_PHD = "phd.1"
    SAG_FILE_TYPE_SEQ = "seq"
    SAG_FILE_TYPE_AB1 = "ab1"
    SAG_FILE_TYPE_GENERIC = "generic"
    
    
    __file_name = None  # 02-CSP407.LIU_Aquifer.E5.SYBR_.2.A8.sc_B01_013.phd.1
    __proposal = None # CSP407
    __project = None # LIU_Aquifer
    __sample = None # E5
    __sort_window = None # SYBR
    __plate_num = -1 # "2"
    __mda_plate_well_pos = None # A8
    __well_type = None # sc
    __sanger_well_pos = None # B01
    __sanger_well_loc = None # 013
    __well_position_column = None # "02"
    __file_type = None
    __valid_file_name_format = False
    __pattern = None
    
    def __init__(self, file_name):
        self.__file_name = file_name
        self.__initialize()
    
    def getFileName(self):
        return self.__file_name
    def getProposal(self):
        return self.__proposal
    def getProject(self):
        return self.__project
    def getSample(self):
        return self.__sample
    def getSortWindow(self):
        return self.__sort_window
    def getPlateNum(self):
        return self.__plate_num
    def getMDAPlateWellPos(self):
        return self.__mda_plate_well_pos
    def getWellType(self):
        return self.__well_type
    def getSangerWellPos(self):
        return self.__sanger_well_pos
    def getSangerWellLoc(self):
        return self.__sanger_well_loc
    def getWellPosColumn(self):
        return self.__well_position_column
    def getFileType(self):
        return self.__file_type
    def getPattern(self):
        return self.__pattern
    
    def setFileType(self, ftype):
        self.__file_type = ftype
    
    def isValidFormat(self):
        return self.__valid_file_name_format
        
    def __initialize(self):
        file_name = str(self.getFileName())
        if file_name.endswith(self.SAG_FILE_TYPE_PHD):
            self.setFileType(self.SAG_FILE_TYPE_PHD)
            file_name = file_name.replace('.phd.1', '')
        elif file_name.endswith(self.SAG_FILE_TYPE_SEQ):
            self.setFileType(self.SAG_FILE_TYPE_SEQ)
            file_name = file_name.replace('.seq', '')
        elif file_name.endswith(self.SAG_FILE_TYPE_AB1):
            self.setFileType(self.SAG_FILE_TYPE_AB1)
            file_name = file_name.replace('.ab1', '')
        else:
            self.setFileType(self.SAG_FILE_TYPE_GENERIC)
        self.__pattern = file_name
        try:
            self.__well_position_column = file_name.split('-')[0]
            file_name = str(file_name.split('-')[1])
            fieldAry = file_name.split('.')
            self.__proposal = fieldAry[0]
            self.__project = fieldAry[1]
            self.__sample = fieldAry[2]
            self.__sort_window = fieldAry[3]
            self.__plate_num = fieldAry[4]
            self.__mda_plate_well_pos = fieldAry[5]
            
            addendum = fieldAry[6]
            addFieldAry = addendum.split("_")
            self.__well_type = addFieldAry[0]
            self.__sanger_well_pos = addFieldAry[1]
            self.__sanger_well_loc = addFieldAry[2]
        except:
            self.__valid_file_name_format = False
            return
        self.__valid_file_name_format = True
        