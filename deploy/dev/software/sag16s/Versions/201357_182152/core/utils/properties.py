"""
$Source: /repository/rqc/Python/core/utils/properties.py,v $
$Revision: 1.2.2.6 $ $Date: 2013-01-15 02:55:52 $
$Author: asyed $

This package module serves as the meta-data source for all the modules and
scripts. Provides user specific DB connection information, user specific
file system locations, log levels, service hosts, etc

@author Aijaz Syed
@contact ASyed@lbl.gov

"""
import ConfigParser ###Python 3 ALERT ### This needs to be changed to import configparser
import getpass

uname = getpass.getuser()
config_file = "/house/groupdirs/QAQC/RQC/prod/configuration/config.properties"


class Properties:
    # Database properties
    dbUser   = None
    dbPass   = None
    dbName   = None
    dbServer = None
    dbPort   = None
    taxDBName= None
    
    # File System properties
    fsLogs   = None
    fsArchiving = None
    fsSoftware  = None
    fsAssemblyQC= None
    fsjigsaw    = None
    fsReadQC    = None
    fsReSeq     = None
    fsAlignmentQC= None
    fsSag16s    = None
    
    # Logger
    logLevel = None
    assemblyQCLogFile = None
    jigsawLogFile = None
    readQCLogFile = None
    reseqLogFile = None
    alignmentQCLogFile = None
    sag16sLogFile = None
    genericLogFile = None

    notification_email_list = None
    
    admin_email_list = None
    
    def __init__(self):
        parser = ConfigParser.SafeConfigParser()
        parser.read(config_file )
        self.dbUser   = parser.get(uname, "db_user")
        self.dbPass   = parser.get(uname, "db_pass")
        self.dbName   = parser.get(uname, "db_name")
        self.dbServer = parser.get(uname, "db_server")
        self.dbPort   = parser.get(uname, "db_port")
        self.taxDBName= parser.get(uname, "tax_db_name")
        self.fsLogs   = parser.get(uname, "fs_logs")
        self.fsSoftware= parser.get(uname, "fs_software")
        self.fsAssemblyQC= parser.get(uname, "fs_assemblyqc_pipe")
        self.fsjigsaw = parser.get(uname, "fs_jigsaw_pipe")
        self.fsReadQC = parser.get(uname, "fs_readqc_pipe")
        self.fsReSeq  = parser.get(uname, "fs_reseq_pipe")
        self.fsAlignmentQC= parser.get(uname, "fs_alignmentqc_pipe")
        self.fsSag16s = parser.get(uname, "fs_sag16s_pipe")
        self.logLevel = parser.get(uname, "log_level")
        self.alignmentQCLogFile=parser.get(uname, "alignmentqc_log_file")
        self.assemblyQCLogFile=parser.get(uname, "assemblyqc_log_file")
        self.jigsawLogFile=parser.get(uname, "jigsaw_log_file")
        self.readQCLogFile=parser.get(uname, "readqc_log_file")
        self.reseqLogFile=parser.get(uname, "reseq_log_file")
        self.sag16sLogFile=parser.get(uname, "sag_16s_log_file")
        self.genericLogFile=parser.get(uname, "generic_log_file")
        self.sag16sURL = parser.get(uname, "sag16s_url")
    

#    def __new__(cls, *args, **kwargs):
#        if not cls._instance:
#            cls._instance = super(Properties, cls).__new__(cls, *args, **kwargs)
#        return cls._instance
        
    def getDBName(self):
        return self.dbName

    def getDBPass(self):
        return self.dbPass
    
    def getDBUser(self):
        return self.dbUser
    
    def getDBServer(self):
        return self.dbServer
    
    def getDBPort(self):
        return self.dbPort
    
    def getFSSoftware(self):
        return self.fsSoftware

    # Log file FS location
    def getFSLogs(self):
        return self.fsLogs
    def getLogLevel(self):
        return self.logLevel
    def getLogFile(self):
        return self.fsLogs+self.genericLogFile
    
    # AssemblyQC File System locations    
    def getFSAssemblyQC(self):
        return self.fsAssemblyQC
    def getFSSAssemblyQCSoftware(self):
        return self.fsSoftware+"assemblyQC/"
    def getFSSAssemblyQCBin(self):
        return self.getFSSAssemblyQCSoftware()+"bin/"
    def getFSSAssemblyQCLib(self):
        return self.getFSSAssemblyQCSoftware()+"lib/"
    def getFSSAssemblyQCSupportScript(self):
        return self.getFSSAssemblyQCSoftware()+"support/"
    def getFSAssemblyQCDataDir(self):
        return self.fsAssemblyQC+"data/"
    def getFSAssemblyQCProcessDir(self):
        return self.fsAssemblyQC+"processing/"
    def getFSAssemblyQCReportsDir(self):
        return self.fsAssemblyQC+"reports/"
    def getAssemblyQCLogFile(self):
        return self.fsLogs+self.assemblyQCLogFile    
    
    # ReadQC File system locations
    def getFSReadQC(self):
        return self.fsReadQC
    def getFSSReadQCSoftware(self):
        return self.fsSoftware+"readQC/"
    def getFSSReadQCBin(self):
        return self.getFSSReadQCSoftware()+"bin/"
    def getFSSReadQCLib(self):
        return self.getFSSReadQCSoftware()+"lib/"
    def getFSSReadQCSupportScript(self):
        return self.getFSSReadQCSoftware()+"support/"
    def getFSReadQCDataDir(self):
        return self.fsReadQC+"data/"
    def getFSReadQCProcessDir(self):
        return self.fsReadQC+"processing/"
    def getFSReadQCReportsDir(self):
        return self.fsReadQC+"reports/"
    def getReadQCLogFile(self):
        return self.fsLogs+self.readQCLogFile

    # Sag16s File system locations
    def getFSSag16s(self):
        return self.fsSag16s
    def getFSSag16sSoftware(self):
        return self.fsSoftware+"sag16s/"
    def getFSSag16sBin(self):
        return self.getFSSag16sSoftware()+"bin/"
    def getFSSag16sLib(self):
        return self.getFSSag16sSoftware()+"lib/"
    def getFSSag16sSupportScript(self):
        return self.getFSSag16sSoftware()+"support/"
    def getFSSag16sDataDir(self):
        return self.fsSag16s+"data/"
    def getFSSag16sStagingDir(self):
        return self.fsSag16s+"staging/"
    def getFSSag16sProcessDir(self):
        return self.fsSag16s+"processing/"
    def getSag16sLogFile(self):
        return self.fsLogs+self.sag16sLogFile
    
    
    def getJigsawLogFile(self):
        return self.fsLogs+self.jigsawLogFile
    def getReseqLogFile(self):
        return self.fsLogs+self.reseqLogFile
    def getAlignmentQCLogFile(self):
        return self.fsLogs+self.alignmentQCLogFile


    def getAdminEmail(self):
        return self.admin_email_list
    def getNotificationEmailList(self):
        return self.notification_email_list
    
    def getSag16sURL(self):
        return self.sag16sURL
        
    
