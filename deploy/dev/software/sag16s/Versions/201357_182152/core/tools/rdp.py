'''
Created on Nov 25, 2012

@author: asyed
'''

from core.utils.rqc_logger import RQCLogger, Properties
import os, commands

class RDPTool(object):
    '''
    classdocs
    '''
    _input = None
    _output = None
    _prop  = None
    _logger = None
    

    def __init__(self, inFile, outFile):
        '''
        Constructor
        '''
        self._input = inFile
        self._output = outFile
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("RDPTool")
        
    
    def run(self):
        if (self._input == None or self._output== None):
            self._logger.error("Input file or output file is not specified, so nothing done")
            return False
        
        if(not os.path.exists(self._input)):
            self._logger.error("Input file specified does not exist in the path. Nothing done. (file: "+self._input+")")
            return False
        
        try:
            f = open(self._output, "w")
            f.close()
        except Exception as e:
            self._logger.error("Output file is not writable. Nothing done. (file: "+self._output+")")
            return False
        #
        # make RDP command
        rdp_cmd = ("/house/groupdirs/QAQC/RQC/prod/misc/software/rdp/rdp_classifier-2.3.jar  "+
                   " -t /house/groupdirs/QAQC/RQC/prod/repo/sag16s/rdp/rRNAClassifier.properties "+
                   " -q "+self._input+" -o "+self._output)
        
        outputTuple = None
        try:
            #
            # run RDP command and get status and output
            outputTuple = commands.getstatusoutput(rdp_cmd)
        except Exception as e:
            self._logger.error("Command FAILED. Cmd: "+rdp_cmd)
            outputTuple = (-1, "Failed")
            
        self._logger.debug("Command: "+rdp_cmd)
        
        #
        # status and output
        status = outputTuple[0]
        output = outputTuple[1]
        
        if( status == 0):
            self._logger.debug("RDP command SUCCESSFULLY completed.")
            return True
        else:
            self._logger.error("RDP command FAILED.")
            return False
        
        return True
        
        