"""
$Source: /repository/rqc/Python/core/utils/rqcdb.py,v $
$Revision: 1.2.2.1 $ $Date: 2012-12-11 22:56:50 $
$Author: asyed $

This package module serves as the meta-data source for all the modules and
scripts. Provides user specific DB connection information, user specific
file system locations, log levels, service hosts, etc

@author Aijaz Syed
@contact ASyed@lbl.gov

"""
import MySQLdb, time
from core.utils.properties import Properties
from core.utils.rqc_logger import RQCLogger
import threading


class RQCdb(object):
    
    __cursor = None
    __dbConn = None
    __logger = None
    __semaphore = None
    __user = None
    __pass = None
    __host = None
    __name = None
    
    '''
    Create a RQC MySQL DB Connection object
    '''
    def __init__(self, user=None, password=None, host=None, name=None): 
        prop    = Properties()
        logger  = RQCLogger()
        self.__logger = logger.getGenericLogger("RQCdb")
        self.__semaphore = threading.BoundedSemaphore()
        #
        # If user does not provide userName, Password, hostName, dbName
        # retrieve them from the properties file
        if user is None: self.__user = prop.getDBUser()
        else: self.__user=user
        if password is None: self.__pass = prop.getDBPass()
        else: self.__pass = password
        if host is None: self.__host = prop.getDBServer()
        else: self.__host=host
        if name is None: self.__name = prop.getDBName()
        else: self.__name=name
        self.__logger.debug("Object(instance) for rqcDB created.")

        #
        # Don't return anything for the object to be operable.
        
    def connect(self):
        self.__logger.debug("Conn. info. Server: "+self.__host+", User: "+self.__user+
                            ", Pass: "+self.__pass+", DB: "+self.__name)
        #
        # max three attempts for the connection
        for i in range(1,4) :
            self.__get_connect(self.__user, self.__pass, self.__host, self.__name)
            if self.__dbConn is None :
                self.__logger.debug("The DB connection wasn't established in this attempt. Try: "+str(i))
                time.sleep(5)
            else:
                # Since connection is established. now just break
                break
        else:
            self.__logger.error("Completed three attempts to acquire the DB connection, but failed.")
                
    '''
    Private class
    '''
    def __get_connect(self, dbUser, dbPass, dbHost, dbName):
        try:
            self.__dbConn = MySQLdb.connect(
                                        host = dbHost,
                                        user = dbUser,
                                        passwd = dbPass,
                                        db = dbName
                                         )
            self.__dbConn.autocommit(True)
            self.__logger.debug("Connection SUCCESSFULLY established with the DB Server.")
            self.__cursor = self.__dbConn.cursor()
            self.__logger.debug("Cursor successfully created with the DB Server.")
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            return None
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            return None
    
    
    '''
    Destructor object, should be called automatically
    '''
    #def __del__(self):
    #    try:
    #        if self.__cursor is not None: self.__cursor.close()
    #        if self.__dbConn is not None: self.__dbConn.close()
    #    except MySQLdb.Error:
    #        self.__logger.error("MySQLdb Error")
    #    except:
    #        self.__logger.error("Unknown Error during DB Connection or cursor creation")
        
        
    '''
    Executes the SQL:
        DMLs both read and update
    '''
    def execute(self, stm):
        try:
            #
            # default is blocking mode so no worries
            #self.__semaphore.acquire()
            #
            # Execute SQL query using execute() method
            self.__cursor.execute(stm);
            #
            # release the shared lock
            #self.__semaphore.release()
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            return False;
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            return False;
        return True


    '''
    Fetch next record for the database
    '''
    def fetch_next(self):
        return self.fetchone()


    '''
    
    '''
    def fetchall(self):
        result = None
        try:
            result = self.__cursor.fetchall()
            self.__logger.debug("Number of records retrieved: "+str(self.__cursor.rowcount))
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            return None;
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            return None; 
        return result


    '''
    Use this to fetch only one record
    '''
    def fetchone(self):
        result = None
        try:
            result = self.__cursor.fetchone()
            self.__logger.debug(result)
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            return None;
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            return None; 
        return result
    
    
    '''
    Returns how many rows were affected by the cursor's update event
    ''' 
    def get_rows_affected(self):
        result = -1
        try:
            result = self.__cursor.rowcount
            self.__logger.debug("Row count: "+str(result))
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            result = -1
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            result = -1
        return result
            
    '''
    Use this to close the connection
    '''
    def disconnect(self):
        try:
            self.__cursor.close()
            self.__dbConn.close()
            self.__logger.debug("Disconnect from the database")
        except MySQLdb.Error, e:
            self.__logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            return False;
        except:
            self.__logger.error("Unknown Error during DB Connection or cursor creation")
            return False;
        return True
  

#
# Use this for testing purpose
if __name__ == "__main__":
    rqcDB = RQCdb()
    rqcDB.connect()
    rqcDB.execute("SELECT * FROM seq_units LIMIT 3")
    print rqcDB.fetchone()
    print rqcDB.fetchone()
    print rqcDB.fetchone()
    rqcDB.execute("SELECT * FROM seq_units LIMIT 3")
    print rqcDB.fetchall()
    rqcDB.execute("SELECT * FROM seq_units LIMIT 3")
    print rqcDB.fetch_next()
    print rqcDB.fetch_next()
    print rqcDB.fetch_next()
    print "Number of rows: "+str(rqcDB.get_rows_affected())
    rqcDB.disconnect()