'''
This module is for SAG16s RDP analysis ORM objects

Created on Nov 16, 2012

@author: asyed
'''

from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, Float
from sqlalchemy.dialects.mysql import DATETIME, VARCHAR
from core.dao.base import ORMBase
from datetime import datetime



class SAGRDPHit(ORMBase.globalBase):
    __tablename__ = 'sag16s_rdp'
    __table_args__= {'mysql_engine':'InnoDB'} # PLEASE DONT REMOVE THIS, MySQL is configured by default as MyISAM on JGI-Debian
    
    rdp_hit_id    = Column(Integer, primary_key=True)
    sag_seq_id    = Column(Integer, nullable=False)
    query_name    = Column(VARCHAR(100), nullable=False)
    domain_name   = Column(VARCHAR(100), nullable=False)
    domain_score  = Column(Float, nullable=False)
    kingdom_name  = Column(VARCHAR(100), nullable=False)
    kingdom_score = Column(Float, nullable=False)
    phylum_name   = Column(VARCHAR(100), nullable=False)
    phylum_score  = Column(Float, nullable=False)
    class_name    = Column(VARCHAR(100), nullable=False)
    class_score   = Column(Float, nullable=False)
    order_name    = Column(VARCHAR(100), nullable=False)
    order_score   = Column(Float, nullable=False)
    family_name   = Column(VARCHAR(100), nullable=False)
    family_score  = Column(Float, nullable=False)
    genus_name    = Column(VARCHAR(100), nullable=False)
    genus_score   = Column(Float, nullable=False)
    fs_location      = Column(VARCHAR(1024), nullable=False)
    dt_data_modified  = Column(DATETIME, nullable=True, onupdate=datetime.now)


    def __init__(self, ssid, query_name, dname, dscore, kname, kscore, pname, pscore, cname, cscore, oname, oscore, fname, fscore, gname, gscore, loc, mod):
        self.sag_seq_id = ssid
        self.query_name = query_name
        self.domain_name = dname
        self.domain_score = dscore
        self.kingdom_name = kname
        self.kingdom_score = kscore
        self.phylum_name = pname
        self.phylum_score = pscore
        self.class_name = cname
        self.class_score = cscore
        self.order_name = oname
        self.order_score = oscore
        self.family_name = fname
        self.family_score = fscore
        self.genus_name = gname
        self.genus_score = gscore
        self.fs_location = loc
        self.dt_data_modified = mod
        
        
    def __repr__(self):
        return "Sag Seq ORM object"

#
# ORM base to create the tables
#ormBase = ORMBase()
#ormBase.getGlobalBase().metadata.create_all(ormBase.getGlobalEngine())
ORMBase.globalBase.metadata.create_all(ORMBase.globalEngine)


