
class SAG16sConstants(object):
    FS_GREEN_GENE_BLAST_DB="/house/groupdirs/QAQC/RQC/prod/repo/sag16s/blastdb/gg16s/ggDB"
    FS_SAG16S_CONTAM_BLAST_DB = "/house/groupdirs/QAQC/RQC/prod/repo/sag16s/blastdb/contam16s/contam16s"
    FS_GREEN_GENE_FASTA ='/house/groupdirs/QAQC/RQC/prod/repo/sag16s/blastdb/gg16s/greengenes_corrected_synth_dna_chloro_mito_euk.MOD.fasta'
    
    PHD2FASTA_CMD = "/jgi/tools/bin/phd2fasta "
    JAZZ_TRIM_CMD = "/jgi/tools/bin/jazz_trim "
    TAXA_LOOKUP_CMD = "/house/groupdirs/QAQC/RQC/prod/misc/software/taxdb/whatdatax.pl "
    BLASTN_CMD = "/jgi/tools/bin/blastall  -p blastn "
    RDP_CMD = "/house/groupdirs/QAQC/RQC/prod/misc/software/rdp/rdp_classifier-2.3.jar -t /house/groupdirs/QAQC/RQC/prod/repo/sag16s/rdp/rRNAClassifier.properties "
