'''
Created on Nov 22, 2012

@author: asyed
'''
from core.utils.rqc_logger import RQCLogger, Properties
from core.dao.sag16s_rdp import SAGRDPHit
from sqlalchemy.exc import IntegrityError, DatabaseError, SQLAlchemyError, InvalidRequestError
from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from datetime import datetime
import os

class RDPParser(object):
    '''
    classdocs
    '''
    _parse_flag = False
    _rdp_output = None
    _rdp_hit = None
    
    _ssid = -1
    _query_name = None
    _domain_name = None
    _domain_score = 0.0
    _kingdom_name = None
    _kingdom_score = 0.0
    _phylum_name = None
    _phylum_score = 0.0
    _class_name = None
    _class_score = 0.0
    _order_name = None
    _order_score = 0.0
    _family_name = None
    _family_score = 0.0
    _genus_name = None
    _genus_score = 0.0
    


    def __init__(self, rdp_op_file, ssid):
        '''
        Constructor
        '''
        self._rdp_output = rdp_op_file
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("RDPParser")
        self._ssid = ssid
    
    
    def parse(self):
        if(self._rdp_output == None or not os.path.exists(self._rdp_output)):
            self._logger.error("RDP output file is not specified or not existing, so nothing done")
            return False
        f = file(self._rdp_output, "r")
        rdphit = f.readline()
        if rdphit == None or rdphit == '' or rdphit== ' ':
            self._logger.debug("Nothing to parse")
            return True
        ary = rdphit.split('\t')
        #
        # Now gather the parsed data
        # example data:
        #    09-.000110CP.rhiz.CviEnd.CviEnd1060.N4.pyrotag.FW_A02_016		Root	Domain	1.0	k__Bacteria	Kingdom	1.0	p__Actinobacteria	Phylum	1.0	c__Actinobacteria (class)	Class	1.0	o__Actinomycetales	Order	1.0	f__Propionibacteriaceae	Family	1.0	g__PropionibacteriumGenus	1.0
        try:
            self._query_name  = ary[0]
            self._domain_name = ary[2]
            self._domain_score = float(ary[4])
            self._kingdom_name = ary[5]
            self._kingdom_score = float(ary[7])
            self._phylum_name = ary[8]
            self._phylum_score = float(ary[10])
            self._class_name = ary[11]
            self._class_score = float(ary[13])
            self._order_name = ary[14]
            self._order_score = float(ary[16])
            self._family_name = ary[17]
            self._family_score = float(ary[19])
            self._genus_name = ary[20]
            self._genus_score = float(ary[22])
        except:
            self._logger.error("Unable to parse RDP output: "+rdphit)
            return False
        
        self._parse_flag = True
        return True
    
    
    def load(self):
        
        retVal = True
        if(not self._parse_flag):
            self._logger.error("Cannot load the hit into DB without parsing, please parse and then load")
            return True
        session = None
        try:
            self._logger.info("Registering RDP hit")
            Session = sessionmaker(bind = ORMBase.globalEngine)
            session = Session()
            seq = SAGRDPHit(self._ssid,
                            self._query_name,
                            self._domain_name,
                            self._domain_score,
                            self._kingdom_name,
                            self._kingdom_score,
                            self._phylum_name,
                            self._phylum_score,
                            self._class_name,
                            self._class_score,
                            self._order_name,
                            self._order_score,
                            self._family_name,
                            self._family_score,
                            self._genus_name,
                            self._genus_score,
                            self._rdp_output, 
                            datetime.now())
            session.add(seq)
            session.commit()
            self._logger.info("Successfully registered RDP hit")
        except IntegrityError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except DatabaseError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except SQLAlchemyError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except InvalidRequestError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        finally:
            if(session != None):
                session.close()
        
        # return to caller
        self._logger.debug("DB update complete")
        return retVal
