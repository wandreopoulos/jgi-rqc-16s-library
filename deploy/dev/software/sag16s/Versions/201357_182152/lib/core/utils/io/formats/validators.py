
from core.utils.rqc_logger import RQCLogger, Properties

class Sag16sFileName(object):
    
    _pattern = None
    _prop = None
    _logger = None
    _wellPosColumn = None
    _cherry = None
    _proposal = None
    _cellMaterial = None
    _pcr = None
    _well = None
    _protocol = None
    _direction = None
    _wellLocation = None
    _wellRowPos = None
    
    #
    # pattern: this could be either file name or just the prefix pattern without the file name extension
    #      WellPositionByColumn-.Cherry.Proposal.CellMaterial.PCR.Well.Protocol.FW/RV_WellLocation_WellRowPosition.[ab1/phd.1/seq]
    #
    def __init__(self, pattern):
        self._prop    = Properties()
        ext = pattern[-4:]
        ext1 = pattern[-6:]
        self._pattern = None
        #
        # get the pattern and file type
        if(ext == ".seq"):
            self._pattern = pattern[:-4]
        elif(ext == ".ab1"):
            self._pattern = pattern[:-4]
        elif(ext1 == ".phd.1"):
            self._pattern = pattern[:-6]
        else:
            self._pattern = pattern
        #
        # Init logger object
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("Sag16sFileName")
        self._logger.debug("Input pattern: "+pattern+"; Actual pattern: "+self._pattern)
    
    #
    # Validates for the following format to be present
    #     WellPositionByColumn-.Cherry.Proposal.CellMaterial.PCR.Well.Protocol.FW/RV_WellLocation_WellRowPosition
    #
    def isValid(self):
        if self._pattern == None :
            self._logger.error("No pattern specified to check against. Please specify the pattern and try again.")
            return False
        #
        # split by '.'
        elementAry = self._pattern.split(".")
        self._logger.debug("Length of the elementAry is "+str(len(elementAry)))
        #
        # Length of the element array must be equal to 7
        if( len(elementAry) != 8):
            self._logger.error("The length of the elementAry split by '.' is not equal to 8")
            self._logger.error("Expected format: WellPositionByColumn-.Cherry.Proposal.CellMaterial.PCR.Well.Protocol.FW/RV_WellLocation_WellRowPosition")
            return False
        self._wellPosColumn = elementAry[0]
        self._cherry = elementAry[1]
        self._proposal = elementAry[2]
        self._cellMaterial = elementAry[3]
        self._pcr = elementAry[4]
        self._well = elementAry[5]
        self._protocol = elementAry[6]
        
        #
        # Eighth element should be of FW/RV_WellLocation_WellRowPosition format
        locAry = elementAry[7].split("_")
        if( len(locAry) != 3):
            self._logger.error("Unexpected format for the well position sub pattern. Expected pattern:  FW/RV_WellLocation_WellRowPosition")
            return False
        self._direction = locAry[0]
        self._wellLocation = locAry[1]
        self._wellRowPos = locAry[2]
        #
        # Looks like everything is valid
        return True
    
    #
    # Retrieve the Pattern
    def getPattern(self):
        return self._pattern
    
    
    def getWellPosColumn(self):
        return self._wellPosColumn
        
        
    def getCherry(self):
        return self._cherry
    
        
    def getProposal(self):
        return self._proposal
    
        
    def getCellMaterial(self):
        return self._cellMaterial
    
        
    def getPCRPlate(self):
        return self._pcr
    
        
    def getPCRWell(self):
        return self._well
    
        
    def getProtocol(self):
        return self._protocol
    
        
    def getDirection(self):
        return self._direction
    
        
    def getSeqWell(self):
        return self._wellLocation
    
    
    def getSeqWellPos(self):
        return self._wellRowPos


class FASTAFormat(object):
    
    def __init__(self):
        pass
    
    