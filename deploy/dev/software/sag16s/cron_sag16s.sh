#!/bin/bash -l

psout=`ps aux | grep sag16sPipeline | grep -v grep`
if [ "x$psout" != "x" ]; then
echo "cron_sag16s is already running"
exit 1
fi

module load python
module load mysql
module load consed
module load jazz_trim
module load blast

export PYTHONPATH=/global/projectb/sandbox/rqc/andreopo/sag16s_deploy/prod/jgi-rqc-16s-library/deploy/dev/software/sag16s/RQC.Core.Prod.Egg:/global/dna/projectdirs/PI/rqc/prod/rqc_software/Python:$PYTHONPATH

python /global/projectb/sandbox/rqc/andreopo/sag16s_deploy/prod/jgi-rqc-16s-library/deploy/dev/software/sag16s/sag16sPipeline.py

