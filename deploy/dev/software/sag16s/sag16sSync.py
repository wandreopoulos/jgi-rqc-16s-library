#!/usr/bin/env python

###/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

'''
Syncing script does the actual copy of the data into the respective 16s FS location.
It does the following stuff:
    1. Creates the various directories for the SAG16S sequence name
    2. Copy the Seq, AB1, PHD.1 files into the sequence directory
    3. Create FASTA sequence and qual files
    4. Runs jazz trimmer and extracts the trim length
    5. Update or Sync into the database
Created on Dec 7, 2012

@author: asyed
'''

from optparse import OptionParser
from core.utils.rqc_logger import RQCLogger, Properties
import os, sys, time, shutil, commands
from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from core.dao.sag16s_seq import SAGSequence, SAGSequenceStatusCV
from sqlalchemy.exc import IntegrityError, DatabaseError, SQLAlchemyError, InvalidRequestError
from datetime import datetime
###import time, datetime
from core.utils.io.formats.validators import Sag16sFileName
from core.utils.rqc_sag_file import SAGFile
from core.utils.rqc_16s_constants import SAG16sConstants
from core.utils.rqcdb import RQCdb
import core.utils.commons



'''
Creates the SAG16S sequence directory
@param dirName: Directory name to be created
@param propObj: Property object
@param logger: Logging object
@return : Returns the SAG16S directory which is successfully created
'''
def makeDirs(dirName, propObj, logger):
    logger.debug("Making directory : "+dirName)
    ltime = time.localtime()
    sag16sDir = prop.getFSSag16sDataDir()+"/"+str(ltime.tm_year)+"/"+str(ltime.tm_mon)+"/"+dirName+"/"
    try:
        if not os.path.exists(sag16sDir):
            os.makedirs(sag16sDir)
    except:
        logger.error("Error occurred while creating dir: "+sag16sDir)
        return None
    # things worked well
    logger.debug("Making directory SUCCESS: "+dirName)
    return sag16sDir


'''
Copies the data from staging into the SAG16s FS location
@param sagDir: SAG16S directory
@param phdFile: the PHD.1 file absolute path
@param abiFile: AB1 file absolute path
@param seqFile: Seq file absolute path
@param logger: Logging object
@return: Returns True if copied successfully, otherwise returns False
'''
def copyData(sagDir, phdFile, abiFile, seqFile, logger):
    logger.debug("Copy data in-progress PhD:"+phdFile+" AB1: "+abiFile+" Seq: "+seqFile)
    try:
        shutil.copy(phdFile, sagDir)
        logger.debug("Copied the file "+phdFile+" to dir "+sagDir)
        shutil.copy(abiFile, sagDir)
        logger.debug("Copied the file "+abiFile+" to dir "+sagDir)
        shutil.copy(seqFile, sagDir)
        logger.debug("Copied the file "+seqFile+" to dir "+sagDir)
        if not createFASTA(phdFile, sagDir, logger):
            return False
    except:
        logger.error("Error occurred while copying files to dir: "+sagDir)
        return False
    logger.debug("Copy data completed")
    return True

'''
Rename a directory from src to dest.
This is useful in case an uploaded file is a duplicate
'''
def moveData(srcdir, destdir):
    logger.debug("Move data in-progress from: " + srcdir + " to dest: " + destdir)
    try:
        os.rename(srcdir, destdir)
        logger.debug("Moved data from: " + srcdir + " to dest: " + destdir)
    except:
        logger.error("Error occurred while moving files to dir: "+destdir)
        return False
    logger.debug("Move data completed")
    return True


'''
Creates the FASTA sequence and the quality files
@param phdFile: PHD.1 file path
@param seqDir: Sequence directory
@param logger: Logger object
@return: Returns True if FASTA sequence and quality are created, otherwise returns False
'''
def createFASTA(phdFile, seqDir, logger):
    phdName = os.path.basename(phdFile)
    name = phdName[:-6]
    fastaName = seqDir+"/"+core.utils.commons.getSag16sFaName(name)
    qualName  = seqDir+"/"+core.utils.commons.getSag16sQualName(name)
    #fastaName = seqDir+"/"+name+".qual"
    phd2faCmd = "%s -os '%s'  -oq '%s'  '%s'" %(SAG16sConstants.PHD2FASTA_CMD,fastaName,qualName,phdFile)
    outputTuple = None
    try:
        logger.debug("Command: "+phd2faCmd)
        #
        # run PHD to FASTA command and get status and output
        outputTuple = commands.getstatusoutput(phd2faCmd)
    except:
        logger.error("Command FAILED. Cmd: "+phd2faCmd)
        outputTuple = (-1, "Failed")
    #
    # status and output
    status = outputTuple[0]
    output = outputTuple[1]
    if( status == 0):
        logger.debug("PHD2FA command SUCCESSFULLY completed.")
    else:
        logger.error("PHD2FA command FAILED.(message: "+output+")")
        return False
    #
    # Quality trimmed file
    trimFile = seqDir+"/"+core.utils.commons.getSag16sTrimName(name)
    trimCmd = "%s %s  15 100  '%s'"%(SAG16sConstants.JAZZ_TRIM_CMD,fastaName,trimFile)

    outputTuple = None
    try:
        logger.debug("Command: "+trimCmd)
        #
        # run PHD to FASTA command and get status and output
        outputTuple = commands.getstatusoutput(trimCmd)
    except:
        logger.error("Command FAILED. Cmd: "+trimCmd)
        outputTuple = (-1, "Failed")
    #
    # status and output
    status = outputTuple[0]
    output = outputTuple[1]
    if( status == 0):
        logger.debug("Trim command SUCCESSFULLY completed.")
    else:
        logger.error("Trim command FAILED.(message: "+output+")")
        return False
    
    return True
    

'''
Extract the Q15 trim length for the SAG16s sequence
@param seqDir: Sequence directory
@param logger: Logger object
@return: Returns the Q15 trim length
'''
def getQ15TrimLength(seqDir, logger):
    trimFile = seqDir+"/*.fasta.trim"
    cmd = "cat  "+trimFile+"  | perl -ne 'next if /^\>/; chomp; print' | wc -m"
    logger.debug("Command to calculate the trimmed length: "+cmd)
    result = None
    try:
        result = commands.getstatusoutput(cmd)
    except:
        return 0
    status = result[0]
    output = result[1]
    if( status != 0 ):
        return 0
    logger.debug("Trim output: "+str(output))
    return output
    

'''
For a fresh new record, register it into RQC Sag16s database
'''
def updateDB(sagFileNameObj, seqFSLoc, propObj, userName, pcrPlateName, runType, logger):
    retVal = True
    logger.debug("DB update in-progress")
    proposalId = sagFileNameObj.getProposal()
    project = sagFileNameObj.getProject()
    sample = sagFileNameObj.getSample()
    sortw = sagFileNameObj.getSortWindow()
    plate_num = sagFileNameObj.getPlateNum()
    mda_wp = sagFileNameObj.getMDAPlateWellPos()
    wtype = sagFileNameObj.getWellType()
    sanger_wp = sagFileNameObj.getSangerWellPos()
    sanger_wl = sagFileNameObj.getSangerWellLoc()
    wpos = sagFileNameObj.getWellPosColumn()
    session = None
    try:
        logger.debug("Retrieving trimmed length")
        trimLen = getQ15TrimLength(seqFSLoc, logger)
        logger.debug("Retrieved trimmed length: "+str(trimLen))
        status = SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID
        if trimLen < 100 :
            status = SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID
            
        logger.debug("Registering seq: "+sagFileNameObj.getPattern())
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        seq = SAGSequence(sagFileNameObj.getPattern(),
                          status, 
                          userName,
                          proposalId,
                          project,
                          sample,
                          sortw,
                          plate_num,
                          pcrPlateName,
                          mda_wp,
                          wtype,
                          sanger_wp,
                          sanger_wl,
                          wpos,
                          trimLen,
                          seqFSLoc,
                          runType,
                          datetime.now(), 
                          datetime.now())
        session.add(seq)
        session.commit()
        logger.info("Successfully registered regular seq: "+sagFileNameObj.getPattern())
    except IntegrityError as er:
        session.rollback()
        logger.error("Updating(regular) the tuple Integrity Error: "+sagFileNameObj.getPattern()+" FAILED")
        retVal  = False
    except DatabaseError as er:
        session.rollback()
        logger.error("Updating(regular) the tuple DB Error: "+sagFileNameObj.getPattern()+" FAILED")
        logger.error(er)
        retVal  = False
    except SQLAlchemyError as er:
        session.rollback()
        logger.error("Updating(regular) the tuple SQL Alchemy Error: "+sagFileNameObj.getPattern()+" FAILED")
        retVal  = False
    except InvalidRequestError as er:
        session.rollback()
        logger.error("Updating(regular) the tuple Invalid Request Error: "+sagFileNameObj.getPattern()+" FAILED")
        retVal  = False
    finally:
        if(session != None):
            session.close()
    #
    # return to caller
    logger.debug("DB update complete")
    return retVal

'''
Update the RQC Sag16s table with status change and username updated in case of duplicate record
'''
def updateDuplicate(sagFileNameObj, seqFSLoc, propObj, userName, pcrPlateName, runType, logger):
    logger.info("Updating the tuple : "+sagFileNameObj.getPattern())
    dbObj = None
    retVal = True
    trimLen = getQ15TrimLength(seqFSLoc, logger)
    logger.debug("Retrieved trimmed length: "+str(trimLen))
    status = SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID
    if trimLen < 100 :
        status = SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID

    sql = "UPDATE sag16s_seq SET ss_status_cv_id= "+str(status)+" , q15_tlength= " + str(trimLen) + " , user_name='"+userName+"', pcr_plate='"+pcrPlateName+"', run_type='"+runType+"', fs_location='"+seqFSLoc+"', dt_status_modified='"+ str(datetime.now()) +"', dt_data_modified='"+ str(datetime.now()) +"' WHERE seq_name='"+sagFileNameObj.getPattern() +"'"
    #####%(SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID, userName, pcrPlateName, runType, sagFileNameObj.getPattern() )
    logger.debug("SQL to retrieve all Hits: "+ sql)

    try:
        logger.info("SAGSequenceStatusCV.SEQ_REGISTERED_STATUS " + str(SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID))
        logger.info("username " + userName)
        dbObj = RQCdb()
        logger.info("pcrPlateName " + pcrPlateName)
        dbObj.connect()
        logger.info("runType " + runType)

        dbObj.execute(sql)
        logger.info("Updating dup tuple : "+sagFileNameObj.getPattern()+" Successfully completed")
        logger.info("Number of rows affected: %s"%(dbObj.get_rows_affected()) )
    except:
        logger.error("Updating dup tuple InvalidRequest Error: "+sagFileNameObj.getPattern()+" FAILED")
        retVal  = False
    finally:
        if(dbObj != None):
            dbObj.disconnect()
    return retVal


#
# Create a timestamp with the format YYYYMMDDHHMMSS.
#
def create_timestamp():
    # 14 digits YYYYMMDDHHMMSS
    year = datetime.now().year
    month = datetime.now().month
    day = datetime.now().day
    hour = datetime.now().hour
    minute = datetime.now().minute
    second = datetime.now().second
    if (month < 10):
        month = "0" + str(month);
    if (day < 10):
        day = "0" + str(day);
    if (hour < 10):
        hour = "0" + str(hour);
    if (minute < 10):
        minute = "0" + str(minute);
    if (second < 10):
        second = "0" + str(second);
    res = str(year) + str(month) + str(day) + str(hour) + str(minute) + str(second);
    return res;

'''
Main for the script is here
@param phdFile: PHD file absolute path
@param ab1File: AB1 file absolute path
@param seqFile: Sequence  file absolute path
@param userName: User name (uploading user name)
@param pcrPlateName: PCR plate name
@param run_type: Run type either RnD or CSP
@param logger: Logger object
'''
def main(phdFile, ab1File, seqFile, userName, pcrPlateName, run_type, logger):
    #
    # extract the actual pattern for the directory; nothing special about the seqFile, just choosing one arbitrarily   
    fileName = os.path.basename(seqFile)
    dirName = fileName#[:-4]
    # flag for if this is duplicate
    is_duplicate = False
    #
    # Check if the file format is valid
    sagFileObj = SAGFile(dirName)
    if not sagFileObj.isValidFormat():
        logger.error("ERROR: invalid format "+dirName+". Related set of files are not uploaded/registered.")
        sys.exit(1)
    #
    # Now check if this is a duplicate entry
    logger.debug("is this dir name a duplicate: " + dirName)
    if core.utils.commons.isDuplicateSagPattern(dirName[:-4], logger) :
        logger.warning("WARNING: duplicate pattern "+dirName+". Related set of files are not uploaded/registered.")
        is_duplicate = True
    #
    # Valid file name format, so we'll go ahead with Registration
    seqDir = makeDirs(dirName, prop, logger)
    if seqDir == None :
        logger.error("ERROR: failed to create RQC dirs for "+dirName+". Related set of files are not uploaded/registered.")
        sys.exit(1)
    #
    # if this is a duplicate filename, then move contents under seqDir to seqDir + "/timestamp"
    if is_duplicate:
        timestamp = create_timestamp()
        ###srcfiles = seqDir + "/" + dirName[:-4] + "*"
        dest = seqDir[:-1] + "_" + timestamp
        if not moveData(seqDir, dest):
            logger.error( "ERROR: failed to move contents of RQC DB for " + seqDir + " to " + dest + ". Related set of files are not uploaded/registered.")
            sys.exit(1)

    #
    # Valid file name format, so we'll go ahead with Registration
    seqDir = makeDirs(dirName, prop, logger)
    if seqDir == None :
        logger.error("ERROR: failed to create RQC dirs for "+dirName+". Related set of files are not uploaded/registered.")
        sys.exit(1)

    #
    # Copy the files into the newly created directory
    if not copyData(seqDir, phdFile, ab1File, seqFile, logger):
        logger.error( "ERROR: failed to copy data to RQC dirs for "+dirName+". Related set of files are not uploaded/registered.")
        sys.exit(1)
    #
    # update the Database, register and set status
    if not is_duplicate :
        if not updateDB(sagFileObj, seqDir, prop, userName, pcrPlateName, run_type, logger):
            logger.error( "ERROR: failed to update RQC DB for "+dirName+". Related set of files are not uploaded/registered.")
            sys.exit(1)
    else:
        if not updateDuplicate(sagFileObj, seqDir, prop, userName, pcrPlateName, run_type, logger):
            logger.error( "ERROR: failed to update Duplicate RQC 16s record for "+dirName)
            sys.exit(1)
    logger.debug("Deleting the staging files")
    os.remove(phdFile)
    os.remove(ab1File)
    os.remove(seqFile)
    logger.debug("Succesfully deleted the staging file")
    logger.debug( "SUCCESS: files registered/uploaded for pattern: "+dirName)
    sys.exit(0)


if __name__ == '__main__':
    prop    = Properties()
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("Sag16sSync")
    phdFile = None
    abiFile = None
    seqFile = None
    #
    # Create Command line options parser object
    parser = OptionParser()
    #
    # Now add options that would be made available from the command line
    #parser.add_option("-c","--core-deploy-dir", dest="coreDeployDir", help="The directory where RQC.Core Egg file will be copied to..",metavar="DEPLOY-DIRECTORY-ABSOLUTE-PATH", type="string", action="store", default=False)
    parser.add_option("-p","--phd-file-path", dest="phdFile", help="PHD file location",metavar="PHD_FILE_LOCATION", type="string", action="store", default=None)
    parser.add_option("-a","--abi-file-path", dest="abiFile", help="ABI file location",metavar="ABI_FILE_LOCATION", type="string", action="store", default=None)
    parser.add_option("-s","--seq-file-path", dest="seqFile", help="Seq file location",metavar="SEQ_FILE_LOCATION", type="string", action="store", default=None)
    parser.add_option("-u","--user-name", dest="userName", help="User name to be registered into the database", type="string", action="store", default=None)
    parser.add_option("-i","--pcr-plate", dest="pcrPlateName", help="user provided PCR Plate name to be registered into the database", type="string", action="store", default=None)
    parser.add_option("-r","--run-type", dest="runType", help="Type of the Run", type="string", action="store", default=None)
    
    #
    # Now actually parse the arguments
    (options, args) = parser.parse_args()
    if(not os.path.exists(options.phdFile) or not os.path.exists(options.abiFile) or not os.path.exists(options.seqFile)):
        logger.error("The path to the files (at least one of) PHD, ABI,or Seq files doesn't exist.")
        sys.exit(1)
    logger.debug("Input Data PhD:"+options.phdFile+" AB1: "+options.abiFile+" Seq: "+options.seqFile)    
    #
    # Call the main
    main(options.phdFile, options.abiFile, options.seqFile, options.userName, options.pcrPlateName, options.runType, logger)
    
