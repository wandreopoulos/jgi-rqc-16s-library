
from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from core.dao.sag16s_seq import SAGSequence, SAGSequenceStatusCV, SAGSequenceHist
from sqlalchemy.exc import IntegrityError, DatabaseError, SQLAlchemyError, InvalidRequestError
from core.utils.rqc_logger import RQCLogger, Properties
import core.utils.commons
from core.tools.blast import BlastNTool
from core.tools.rdp import RDPTool
from core.parsers.blast import BlastTopHitParser
from core.parsers.rdp import RDPParser
from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_16s_constants import SAG16sConstants
import sys, commands, re, os, glob
import datetime
from subprocess import Popen, PIPE
from email.mime.text import MIMEText

'''
make sure only the sequences with length more than 100 basepairs are selected for the analysis
by setting these sequences to status SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID
'''
def getRecordsByStatusLowQ15TLen(logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records by q15_tlength<=100")
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with length more than 100 basepairs are selected for the analysis
        records = session.query(SAGSequence).filter(SAGSequence.q15_tlength<=100,SAGSequence.ss_status_cv_id == SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records



def runSag16sLowQ15TLen(logger):
    sag16sList = getRecordsByStatusLowQ15TLen(logger)
    logger.debug("Processing LowQ15TLen task for: %s" %(len(sag16sList)))
    for seq in sag16sList :
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID, logger)
        setSeqEmailSent(seq.seq_name, 0, logger)

    return True


'''
Get the records that will actually be analyzed.
Please make sure only the sequences with length more than 100 basepairs are selected for the analysis
'''
def getRecordsByStatus(status_id, logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records by status: "+str(status_id))
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with length more than 100 basepairs are selected for the analysis
        records = session.query(SAGSequence).filter(SAGSequence.ss_status_cv_id == status_id,SAGSequence.q15_tlength>100).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records

#
#This function will return jobs that failed at a particular step of analysis.
#Note it will only return whatever has not been retried yet.
#
def getRecordsByStatusFailed(status_id, logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records by status: "+str(status_id))
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with retried_flag=0 are selected for the analysis
        records = session.query(SAGSequence).filter(SAGSequence.ss_status_cv_id == status_id,SAGSequence.retried_flag == 0).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records


def getRecordsByStatusModTime(logger):
    records = []
    session=None
    try:
        current_time = datetime.datetime.utcnow()
        one_day_ago = current_time - datetime.timedelta(days=1)
        logger.info("Retrieving records from: "+ str(one_day_ago) + " to: " + str(current_time) )
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with length more than 100 basepairs are selected for the analysis
        records = session.query(SAGSequence).filter(SAGSequence.dt_data_modified > one_day_ago).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records


'''
Get record by email_sent_flag == 0
Note we should not return all records including sequences that may have just been uploaded.
'''
def getRecordsByEmailSentFlag(logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records for EmailSentFlag false")
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with email sent flag false are retrieved
        current_time = datetime.datetime.utcnow()
        start_date = current_time - datetime.timedelta(days=93)
        records = session.query(SAGSequence).filter(SAGSequence.dt_data_modified > start_date, SAGSequence.ss_status_cv_id != SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records


'''
During processing various stages, this sets the status of the Sag16s Sequence

@param prefix: sag16s_seq_name (actual sequence name pattern stored in the RQC database)
@param status_id: The status ID to be set for this sequence name
@param logger: logging object
@return Returns True for a successfully updated status, otherwise returns False
'''
def setSeqStatus(prefix, status_id, logger):
    session=None    
    try:
        logger.info("Setting status: "+str(status_id)+" for seq_name: "+prefix)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        session.query(SAGSequence).filter_by(seq_name=prefix).update({"ss_status_cv_id": status_id})
        session.commit()
        seqhist = SAGSequenceHist(prefix,
                            status_id,
                            datetime.datetime.utcnow(),
                            datetime.datetime.utcnow())
        session.add(seqhist)
        session.commit()
    except IntegrityError, e:
        session.rollback()
        session.close()
        logger.error("Failed updated Status ID "+str(status_id)+" for sequence: "+prefix)
        print("Integrity error! %s", e)
        return False
    except:
        session.rollback()
        session.close()
        logger.error("Failed updated Status ID "+str(status_id)+" for sequence: "+prefix)
        logger.error("An unknown exception occurred!")
        return False
    finally:
        if(session != None):
            session.close()
    logger.debug("Successfully updated Status ID "+str(status_id)+" for sequence: "+prefix)
    return True


def setSeqRetried(prefix, flag, logger):
    session=None
    try:
        logger.info("Setting retried_flag: "+str(flag)+" for seq_name: "+prefix)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        session.query(SAGSequence).filter_by(seq_name=prefix).update({"retried_flag": flag})
        session.commit()
    except IntegrityError, e:
        session.rollback()
        session.close()
        logger.error("Failed updated retried_flag "+str(flag)+" for sequence: "+prefix)
        print("Integrity error! %s", e)
        return False
    except:
        session.rollback()
        session.close()
        logger.error("Failed updated retried_flag "+str(flag)+" for sequence: "+prefix)
        logger.error("An unknown exception occurred!")
        return False
    finally:
        if(session != None):
            session.close()
    logger.debug("Successfully updated retried_flag "+str(flag)+" for sequence: "+prefix)
    return True



def setSeqEmailSent(prefix, flag, logger):
    session=None
    try:
        logger.info("Setting email_sent_flag: "+str(flag)+" for seq_name: "+prefix)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        session.query(SAGSequence).filter_by(seq_name=prefix).update({"email_sent_flag": flag})
        session.commit()
    except IntegrityError, e:
        session.rollback()
        session.close()
        logger.error("Failed updated email_sent_flag "+str(flag)+" for sequence: "+prefix)
        print("Integrity error! %s", e)
        return False
    except:
        session.rollback()
        session.close()
        logger.error("Failed updated email_sent_flag "+str(flag)+" for sequence: "+prefix)
        logger.error("An unknown exception occurred!")
        return False
    finally:
        if(session != None):
            session.close()
    logger.debug("Successfully updated email_sent_flag "+str(flag)+" for sequence: "+prefix)
    return True




'''
Runs an instance of Blast against SAG16S database. Specifically green gene database
    1. Pull all the records that are of status ready
    2. For each record, run BLAST over SAG16S database
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: BLAST output is stored in the standard location as PATH_TO_seq_name_prefix/sequence_name_prefix.blast
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
          returns False
'''
def runSag16sBlastTask(logger):

    #rerun the failed ones from last time
    runSag16sBlastTaskFailed(logger)

    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID, logger)    
    logger.debug("Processing Blast task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sBlastTaskBody(seq, logger)
        result = result and resultb

        ###set retried = false and email_sent flag = false
        setSeqRetried(seq.seq_name, 0, logger)
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result


def runSag16sBlastTaskBody(seq, logger):
        # First run Blast against nt.
        # Set the status to in progress
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastName(seq.seq_name)
        db = SAG16sConstants.FS_NT_BLAST_DB
        # debugging
        logger.debug("BLAST FA: "+faSeq+" Out: "+outFile+" DB "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        if not blastTool.run():
            logger.error("Unable to run the BLAST hit for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
            return False
        else:
            logger.debug("Parsing the BLAST NT output")
            outFiles = glob.glob(os.path.join(outFile, "*parsed"))
            if len(outFiles) > 0:
                outFile = outFiles[0]
            else:
                outFile = None
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_HOMOLOGY, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST  hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                phylogeny = getPhylogeny(outFile, logger)
                if bParser.load(phylogeny):
                    logger.debug("Successfully loaded BLAST  hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST  hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                    return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no BLAST data retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                
        # Second run Blast against greengenes.
        # Set the status to in progress
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastGreengenesName(seq.seq_name)
        db = SAG16sConstants.FS_SILVA_BLAST_DB
        # debugging
        logger.debug("BLAST FA: "+faSeq+" Out: "+outFile+" DB "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        if not blastTool.run():
            logger.error("Unable to run the BLAST hit on greengenes for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
            return False
        else:
            logger.debug("Parsing the BLAST greengenes output")
            outFiles = glob.glob(os.path.join(outFile, "*parsed"))
            if len(outFiles) > 0:
                outFile = outFiles[0]
            else:
                outFile = None
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_GREENGENES, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST greengenes hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                phylogeny = getPhylogeny(outFile, logger)
                if bParser.load(phylogeny):
                    logger.debug("Successfully loaded BLAST greengenes hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST greengenes hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                    return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no BLAST data on greengenes retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)


        # Third run Blast against img.
        # Set the status to in progress
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastImgName(seq.seq_name)
        db = SAG16sConstants.FS_IMG_BLAST_DB
        # debugging
        logger.debug("BLAST FA: "+faSeq+" Out: "+outFile+" DB "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        if not blastTool.run():
            logger.error("Unable to run the BLAST hit on IMG for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
            return False
        else:
            logger.debug("Parsing the BLAST IMG output")
            outFiles = glob.glob(os.path.join(outFile, "*parsed"))
            if len(outFiles) > 0:
                outFile = outFiles[0]
            else:
                outFile = None
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_IMG, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST IMG hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                phylogeny = getPhylogeny(outFile, logger)
                if bParser.load(phylogeny):
                    logger.debug("Successfully loaded BLAST IMG hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST IMG hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                    return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no BLAST data on IMG retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)

        # You are here because parsing for Blast successfully completed
        return True


def runSag16sBlastTaskFailed(logger):
    ###get sequences with SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID and retried = false, then run the same as runSag16sBlastTask
    sag16sList = getRecordsByStatusFailed(SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
    logger.debug("Processing Failed Blast task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sBlastTaskBody(seq, logger)
        result = result and resultb

        ###set retried = true
        setSeqRetried(seq.seq_name, 1, logger)
        ###set email_sent flag = false
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result





'''
Runs an instance of Blast against SAG16S Contamination database.
    1. Pull all the records that are of status BLAST 16s completed
    2. For each record, run BLAST over SAG16S Contamination database
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: BLAST output is stored in the standard location PATH_TO_seq_name_prefix/sequence_name_prefix.blast
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
        returns False
'''
def runSag16sBlastContamTask(logger):

    #rerun the failed ones from last time
    runSag16sBlastContamTaskFailed(logger)

    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
    logger.debug("Processing BlastContam task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sBlastContamTaskBody(seq, logger)
        result = result and resultb

        ###set retried = false and email_sent flag = false
        setSeqRetried(seq.seq_name, 0, logger)
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result


def runSag16sBlastContamTaskBody(seq, logger):
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastContamName(seq.seq_name)
        db = SAG16sConstants.FS_SAG16S_CONTAM_BLAST_DB
        # debugging
        logger.debug("Calling BLAST contam tool with FASTA: "+faSeq+" Out: "+outFile+" DB: "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        blastTool.setPI(95)
        if not blastTool.run():
            logger.error("Unable to run the BLAST Contam hit for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
            #return False
        else:
            logger.debug("Parsing the BLAST contam16s output")
            outFiles = glob.glob(os.path.join(outFile, "*parsed"))
            if len(outFiles) > 0:
                outFile = outFiles[0]
            else:
                outFile = None
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_CONTAM, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST Contam hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
                #return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                if bParser.load(None):
                    logger.debug("Successfully loaded BLAST contam hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST Contam hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
                    #return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no data retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
        return True


def runSag16sBlastContamTaskFailed(logger):
    ###get sequences with SEQ_CONTAM_SEARCH_FAILED_STATUS_ID and retried = false, then run the same as runSag16sBlastContamTask
    sag16sList = getRecordsByStatusFailed(SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
    logger.debug("Processing Failed BlastContam task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sBlastContamTaskBody(seq, logger)
        result = result and resultb

        ###set retried = true
        setSeqRetried(seq.seq_name, 1, logger)
        ###set email_sent flag = false
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result



'''
Runs RDP classifier on sequences that successfully completed BLAST on 16S contam database.
    1. Pull all the records that are of status BLAST 16s CONTAM completed
    2. For each record, run RDP classifier
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: RDP output is stored in the standard location PATH_TO_seq_name_prefix/sequence_name_prefix.rdp
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
        returns False
'''
def runSag16sRDPTask(logger):

    #rerun the failed ones from last time
    runSag16sRDPTaskFailed(logger)

    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
    logger.debug("Processing RDP task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sRDPTaskBody(seq, logger)
        result = result and resultb

        ###set retried = false and email_sent flag = false
        setSeqRetried(seq.seq_name, 0, logger)
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result


def runSag16sRDPTaskBody(seq, logger):
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_IN_PROGRESS_STATUS_ID,logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastRDPName(seq.seq_name)
        rdpTool = RDPTool(faSeq, outFile)
        if not rdpTool.run():
            logger.error("Failed to run the RDP tool.")
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
            return False
        else:
            rParser = RDPParser(outFile, seq.sag_seq_id)
            if not rParser.parse():
                logger.error("Unable to parse the RDP file : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
                return False
            
            if rParser.load():
                logger.debug("Successfully loaded RDP hit for : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_COMPLETE_STATUS_ID, logger)
            else:
                logger.error("Unable to load the RDP hit for file : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
                return False
        return True


def runSag16sRDPTaskFailed(logger):
    ###get sequences with SEQ_RDP_FAILED_STATUS_ID and retried = false, then run the same as runSag16sRDPTask
    sag16sList = getRecordsByStatusFailed(SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
    logger.debug("Processing Failed RDP task for: %s" %(len(sag16sList)))
    result = True
    for seq in sag16sList :
        resultb = runSag16sRDPTaskBody(seq, logger)
        result = result and resultb

        ###set retried = true
        setSeqRetried(seq.seq_name, 1, logger)
        ###set email_sent flag = false
        setSeqEmailSent(seq.seq_name, 0, logger)

    # return true if all sequences finished the step successfully, if any failed return false
    return result


def mapStatusToString(status):
    if status == SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID:
        return "REGISTERED"
    elif status == SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID:
        return "HOMOLOGY_SEARCH_IN_PROGRESS"
    elif status == SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID:
        return "HOMOLOGY_SEARCH_FAILED"
    elif status == SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID:
        return "HOMOLOGY_SEARCH_COMPLETE"
    elif status == SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_IN_PROGRESS_STATUS_ID:
        return "CONTAM_SEARCH_IN_PROGRESS"
    elif status == SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID:
        return "CONTAM_SEARCH_FAILED"
    elif status == SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID:
        return "CONTAM_SEARCH_COMPLETE"
    elif status == SAGSequenceStatusCV.SEQ_RDP_IN_PROGRESS_STATUS_ID:
        return "MOTHUR_IN_PROGRESS"
    elif status == SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID:
        return "MOTHUR_FAILED"
    elif status == SAGSequenceStatusCV.SEQ_RDP_COMPLETE_STATUS_ID:
        return "MOTHUR_COMPLETE - SUCCESS"
    elif status == SAGSequenceStatusCV.SEQ_FINALIZED:
        return "FINALIZED"
    elif status == SAGSequenceStatusCV.SEQ_ON_HOLD_ID:
        return "ON_HOLD"
    elif status == SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID:
        return "LOW_TRIM_LENGTH"
    else:
        return "UNKNOWN_STATUS"





'''
This function will retrieve jobs that have not had emails sent to the owners 
and are not in state newly registered. 
Condition: SAGSequence.email_sent_flag == 0,SAGSequence.ss_status_cv_id != SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID. 
'''
def emailSeqStatuses(logger):
    ###get sequences with email_sent flag = false
    sag16sList = getRecordsByEmailSentFlag(logger)  ###getRecordsByStatusModTime(logger)
    logger.debug("Sending emails for the latest SAG16S analysis runs")
    emails_bodies = {}
    emails_q15lenzero = {}
    emails_succ = {}
    emails_failedwillrerun = {}
    emails_failednorerun = {}

    for seq in sag16sList :
        emailTo = seq.user_name
        if not emails_bodies.has_key(emailTo):
            emails_bodies[emailTo] = "<tr><th>seq_name</th><th>ss_status_cv_id</th><th>q15_tlength</th><th>fs_location</th></tr>"
        if seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_LOW_TRIM_LEN_HOLD_ID:  ###seq.q15_tlength <= 100 and
             emails_q15lenzero[emailTo] = emails_q15lenzero.get(emailTo, 0) + 1
        if seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_RDP_COMPLETE_STATUS_ID or seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_FINALIZED:
             emails_succ[emailTo] = emails_succ.get(emailTo, 0) + 1
        if (seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID or seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID or seq.ss_status_cv_id == SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID):
            if seq.retried_flag == 0:
                emails_failedwillrerun[emailTo] = emails_failedwillrerun.get(emailTo, 0) + 1
            if seq.retried_flag == 1:
                emails_failednorerun[emailTo] = emails_failednorerun.get(emailTo, 0) + 1

        body_row = "<tr><td>" + str(seq.seq_name) + "</td><td>" + str(mapStatusToString(seq.ss_status_cv_id)) + "</td><td>" + str(seq.q15_tlength) + "</td><td>" + str(seq.fs_location) + "</td></tr>"
        emails_bodies[emailTo] += body_row

        ###set email_sent flag = true
        setSeqEmailSent(seq.seq_name, 1, logger)

    toEmails = ["wandreopoulos@lbl.gov","rrmalmstrom@lbl.gov","ksingh@lbl.gov","brycefoster@lbl.gov"]
    email_body = "<html><body><h2>Quarterly summary of SAG16S analysis results</h2>"
    for k in emails_bodies.keys():
        emails_bodies[k] = "<h4>User: " + k + "</h4><h4>Skipped because q15_tlength is zero: " + str(emails_q15lenzero.get(k, 0)) + "</h4><h4>Successfully finished: " + str(emails_succ.get(k, 0)) + "</h4><h4>Failed first time and will retry: " + str(emails_failedwillrerun.get(k, 0)) + "</h4><h4>Failed second time and will not retry: " + str(emails_failednorerun.get(k, 0)) + "</h4> <table cellpadding=2 cellspacing=0 border=1>" + emails_bodies[k] + "</table>"
        email_body += emails_bodies[k]
        if not k in toEmails:
           toEmails.append(k)

    email_body += "</body></html>"
    sendEmail(",".join(toEmails), "RQC - Quarterly summary of SAG16S pipeline results", email_body)

    return True


'''
send out email
@param emailTo: email receipient (e.g. brycefoster@lbl.gov)
@param emailSubject: subject line for the email
@param emailBody: content of the email
@param emailFrom: optional email from
- Alex Boyd has a new version of this
'''
def sendEmail(emailTo, emailSubject, emailBody, emailFrom = 'rqc@jgi-psf.org', log = None):

    msg = ""
    errFlag = 0
    
    if not emailTo:
        msg = "- send_email: email_to parameter missing!"

    if not emailSubject:
        msg = "- send_email: email_subject parameter missing!"

    if not emailBody:
        msg = "- send_email: email_body parameter missing!"
    
    
    if errFlag == 0:        
        msg = "- sending email to: %s" % (emailTo)
    
    if log:
        log.info(msg)
    else:
        print msg

    if errFlag == 1:
        return 0

    # assume html
    emailMsg = MIMEText(emailBody, "html") # vs "plain"
    emailMsg['Subject'] = emailSubject
    emailMsg['From'] = emailFrom
    emailMsg['To'] = emailTo
    emailMsg['Cc'] = "wandreopoulos@lbl.gov,brycefoster@lbl.gov"
    
    p = Popen(["/usr/sbin/sendmail", "-t"], stdin = PIPE)
    p.communicate(emailMsg.as_string())
    
    return errFlag


'''
Creates a TopHit FASTA sequence Id and FASTA sequence
@param seq_name: 16S sequence name for which FASTA is to be created
@param fasta_file_name: FASTA file name for the top hit FASTA
@param logger: Logging object
@return: returns True for successfully created FASTA, otherwise returns False
'''
def createTopHitFasta(seq_name, fasta_file_name, logger):
    allFasta = SAG16sConstants.FS_GREEN_GENE_FASTA
    hitName = None
    try:
        logger.info("Retrieving top hit record for query_name: "+seq_name)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        records = session.query(SAGTopHit).filter(SAGTopHit.query_name == seq_name).all()
        rowResult = records[0]
        hitName = rowResult.subject_id
    except:
        logger.error("Error occurred while retrieving the top_hit name")
        return False
    # Now the hitName is populated with the BLAST Top Hit
    try:
        f = open(fasta_file_name,'w')
        with open(allFasta) as fp:
            for name, seq in read_fasta(fp):
                if( name == '>'+hitName ):
                    f.write(name+'\n')
                    f.write(seq)
        f.close()
    except:
        logger.error("TopHit FASTA file not successfully created")
        return False
    return True


'''
A sequence iterator to get the FASTA of interest
@param fp: file pointer
@return: A tuple (SequenceId, FASTASequence)
'''
def read_fasta(fp):
    name, seq = None, []
    for line in fp:
        line = line.rstrip()
        if line.startswith(">"):
            if name: yield (name, ''.join(seq))
            name, seq = line, []
        else:
            seq.append(line)
    if name: yield (name, ''.join(seq))


'''
Run the Phylogeny lookup module and return the Phylogeny information for the 
BLAST top-hit.

@param blastOutFile : BLAST output file, to pull out the Blast hit name
@param logger: logging object
@return : Returns the phylogeny information for the top hit
'''
def getPhylogeny(blastOutFile, logger):
    phylogeny = None
    subject_id = None
    try:
        f = file(blastOutFile, "r")
        #f.readline()
        #f.readline()
        #f.readline()
        #f.readline()
        blastop = f.readline()
        while blastop.startswith("#"):
            blastop = f.readline()
        ary = blastop.split("\t")
        subject_id = ary[1]
    except:
        logger.error("Phylogeny-TopHit output is not successfully parsed to find the Phylogeny")
        return None
    # extract anything after 593322_FN421897.1_ which will be proteogenomics_physiology_phyllosphere_clover_clone_2_H11_k_Bacteria;_p_Proteobacteria;_c_Betaproteobacteria;_o_Burkholderiales;_f_Comamonadaceae;_g_Variovorax;_s_Variovorax_paradoxus;_otu_3057
    logger.debug("Phylogeny-Matching the pattern Subject: "+subject_id)
    m = re.search(':(.*)',subject_id)
    pattern = None
    if m != None:
        pattern = m.group(1)
    else:
        pattern = subject_id
    #     try:
    #         # This needs to be done to extract a useable hit name, otherwise, we end up with absolutely no hits
    #         subject_id = subject_id.replace("'","")
    #         regexCmd = "echo '"+subject_id+"' | perl -nle 's/;\w_$//;s/.+;[\w]_//; print'"
    #         output = commands.getstatusoutput(regexCmd)
    #         pattern = output[1]
    #     except:
    #         logger.error("Error while matching [perl regex] for the pattern Subject: "+subject_id)
    #         return None

    if pattern == None or pattern == '' or pattern == ' ' or pattern == '  ':
        logger.error("Phylogeny-Nothing matching group[1] for the pattern Subject: "+subject_id)
        return None
    # replace the single quotes so they don't interfere with the command line single quotes
    pattern = pattern.replace("'","") # avoid unwanted termination of the command line string
    pattern = pattern.replace("-","_") # can pretend to be a command line option, so replace the - with _
    # For now we just return the pattern, which is the NT database string that Blast hit.
    return pattern

    '''
    ###Even if this fails we still  return pattern. The SAG16sConstants.TAXA_LOOKUP_CMD is not used anymore
    phyCmd = SAG16sConstants.TAXA_LOOKUP_CMD+" -n '%s'"%(pattern)
    try:
        outputTuple = commands.getstatusoutput(phyCmd)
    except:
        logger.error("Phylogeny command was not successfully performed. Cmd: "+phyCmd)
        return pattern
    #
    # status and output
    status = outputTuple[0]
    output = outputTuple[1]
    if( status == 0):
        logger.debug("Phylogeny command SUCCESSFULLY completed. Cmd: "+phyCmd)
        logger.debug("Phylogeny for topHit : " + output + " . Status: " + str(status))
        if output == '' or output == ' ' or output == '  ':  ###"find kingdom" in output or "unknown":
            output = pattern
        return output
    else:
        logger.error("Phylogeny command FAILED with status: "+str(status))
        return pattern
    # should never be here, but however
    return None
    '''


#
# Main program that will run when this script is executed from the command line
#
if __name__ == '__main__':
    prop    = Properties()
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("Sag16sPipeline")
    '''
    # Find sequences with low q15 t length that will not be run anyway
    runSag16sLowQ15TLen(logger)

    # Run Blast on 16s and 16sContam DBs
    if not runSag16sBlastTask(logger):
        logger.error("FAILED processing the BLASTNTool on ggDB, so exiting")
        #sys.exit(1)
        
    if not runSag16sBlastContamTask(logger):
        logger.error("FAILED processing the BLASTNTool on Contam16s, so exiting")
        #sys.exit(1)
        
    # Run RDP 
    if not runSag16sRDPTask(logger):
        logger.error("FAILED processing the RDP, so exiting")
        #sys.exit(1)
    '''
        
    # Send emails
    emailSeqStatuses(logger)

    sys.exit(0)
    # clean up
    
