var checkboxHeight = "20";
var selectWidth = "20";
var initialized = false;

/* No need to change anything after this */
document.write('<style type="text/css">input.styled_checkbox { display: none; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>');

var StyledChkBox = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if(	inputs[a].type == "checkbox" 					&& 
				inputs[a].className == "styled_checkbox"		&&
				(!initialized || inputs[a].id.indexOf('seg_qc') < 0)){
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;
				span[a].style.cursor = "pointer";

				if(inputs[a].checked == true) {
					position = "0 -" + (checkboxHeight*2) + "px";
					span[a].style.backgroundPosition = position;
					span[a].title = "Yes";
				} else {
					span[a].title = "No";
				}

				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				if(inputs[a].id.indexOf('seg_qc') >= 0){
				//for segQC page checkbox that do not uase onchage() for UI sync
					inputs[a].onchange = StyledChkBox.clear;
				}
				if(!inputs[a].getAttribute("disabled")) {
					span[a].onmousedown = StyledChkBox.pushed;
					span[a].onmouseup = StyledChkBox.check;
				} else {
					span[a].className = span[a].className += " disabled";
				}
			}
		}
		document.onmouseup = StyledChkBox.clear;
		initialized = true;
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true){
			this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
		} else {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} 
	},
	check: function() {
		element = this.nextSibling;
		if(element.checked == true) {
			this.style.backgroundPosition = "0 0";
			this.title = "No";
			element.checked = false;
		} else {
			this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			this.title = "Yes";
			element.checked = true;
		}

		if(element.onchange)	//perform checkbox onchange
			element.onchange();
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].className == "styled_checkbox") {
				if(inputs[b].checked == true){
					inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
				}
				else {
					inputs[b].previousSibling.style.backgroundPosition = "0 0";
				}
			}
		}
	},
}

