
$(document).ready(function() {
    
    $("#banner").banner(); 
    $().banner_title({
        type:'major',
        data:'RollingQC SAG16s UI'
    });
    $().banner_title({
        type:'minor',
        data:'RQC Sag16s Analysis Results',
    });
    $().banner_title({type:'info', data: ''});
    

    $('#analysisTable').each( function(){
        table_makeSortable( $(this) )
    });
});


function table_makeSortable(tableElement) {
    var firstRow=null;
    if (tableElement.rows && tableElement.rows.length > 0)
    {
        firstRow = tableElement.rows[0];
    }
    // This table doesnt have any rows..unfortunately
    if (!firstRow) return;
    // Now make the first row elements sortable
    for (var i=0; i < firstRow.cells.length ; i++)
    {
        var firstRowElement = firstRow.cells[i];
        // innerText should be fine because none of the cell is another element..
        var txt = ts_getInnerText(firstRowElement);
        if ( i < firstRow.cells.length-1)
        {
            firstRowElement.innerHTML = '<a href="#" class="sortheader" onclick="table_resort(this, '+i+');return false;"> '+txt+'<span class="sortarrow"></span></a>';

        }
        else
        {
            firstRowElement.innerHTML = txt;
        }
        firstRowElement.style.fontFamily="verdana";
        firstRowElement.style.fontSize=12;
    }
    colorTableRows(tableElement);
};

function colorTableRows(tableElement)
{
    var tableRows, tableRow;
    var start = 1;
    tableRows = tableElement.rows;
    if(tableRows.length == 1 )
    {
        tableRows = tableElement.tBodies[0].rows;
        start = 0;
    }
    // For now just return true, since there is a bug and this is not critical component
    return true;
    for(var i=start; i < tableRows.length; i++)
    {
        tableRow = tableRows[i];
        if (i%2 == 0)
        {
            if( ! tableRow.className.indexOf('odd') == -1)
            {
                tableRow.className = tableRow.className.replace('odd', 'even');
            }
            else
            {
                if( tableRow.className.indexOf('even') == -1 )
                {
                    tableRow.className += " even";
                }
            }
        }
        else
        {
            if( !tableRow.className.indexOf('even') == -1)
            {
                tableRow.className = tableRow.className.replace('even', 'odd');
            }
            else
            {
                if( tableRow.className.indexOf('odd') == -1 )
                {
                    tableRow.className += " odd";
                }
            }
        }
    }
};

function init()
{
    var elementsStr  = "$seq_unit_to_analyst_id_str";
    var nameIdTokens = elementsStr.split(",");
    for( var i = 0; i < nameIdTokens.length-1; i++ )
    {
        if(nameIdTokens[i] != null )
        {
            var leafTokens   = nameIdTokens[i].split("::");
            var elementId    = leafTokens[0];
            var elementValue = leafTokens[1];

            document.getElementById( elementId+'' ).value = leafTokens[1]+'';
        }
    }

    //making sortable
    // Find all tables with class sortable and make them sortable
    if (!document.getElementsByTagName)
    {
        return;
    }


    var makeSortableTable = document.getElementById("listTable");
    table_makeSortable(makeSortableTable);
};


function table_resort(lnk, column)
{
    var span, tableData, table, item, i, sortableRows;
    SORT_COLUMN_INDEX = column;

    tableData = lnk.parentNode;
    table     = getParent(tableData,'TABLE');
    // If only one row is present or no row is present no sorting initiated
    if (table.rows.length <= 1) return;

    // Get the first element of the column
    item = "";
    // index is 1 since we dont want the header actually using "tHead and <thead> <th>
    // tBody and <tbody> would make sense
    i = 1;
    while (item == "" && i < table.rows.length)
    {
        var itm = ts_getInnerText(table.rows[i].cells[column]);
        item = trim(itm);
        i++;
    }

    if (item == "") return;

    // Identify the appropriate sort method to be applied to the table
    sortfn = ts_sort_caseinsensitive;
    if (item.match(/^-?(\\d+[,\\.]?)+(E[-+][\\d]+)?%?\$\/)) sortfn = ts_sort_numeric;

    sortableRows = new Array();
    // Skip the first row
    for (var j=1;j<table.rows.length;j++)
    {
        sortableRows[j-1] = table.rows[j];
    }

    for (var ci=0;ci<lnk.childNodes.length;ci++)
    {
        if (lnk.childNodes[ci].tagName && lnk.childNodes[ci].tagName.toLowerCase() == 'span')
        {
           span = lnk.childNodes[ci];
        }
    }

    // Sort the table here
    sortableRows.sort(sortfn);

    // Properly place the arrow
    if (span.getAttribute("sortdir") == 'down')
    {
        ARROW = "&darr";
        sortableRows.reverse();
        span.setAttribute('sortdir','up');
    }
    else
    {
        ARROW = "&uarr";
        span.setAttribute('sortdir','down');
    }

    // Populate the Table
    for (i=0; i<sortableRows.length; i++)
    {
        table.tBodies[0].appendChild(sortableRows[i]);
    }

    // Delete any other arrows there may be showing
    var allspans = document.getElementsByTagName("span");
    for (var ci=0;ci<allspans.length;ci++)
    {
        if (allspans[ci].className == 'sortarrow')
        {
            if (getParent(allspans[ci],"table") == getParent(lnk,"table"))
            { // in the same table as us?
                allspans[ci].innerHTML = '';
            }
        }
    }

    span.innerHTML = ARROW;
    colorTableRows(table);
};


/*
    gets the parent Element with a specified tag name
    el HTML element
    pTagName is the tag name of the parent you are searching for
*/
function getParent(el, pTagName)
{
    if (el == null)
    {
        return null;
    }
    else
    {
        if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())
        {
            return el;
        }
        else
        {
            return getParent(el.parentNode, pTagName);
        }
    }
};


function ts_sort_numeric(a,b)
{
    var aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
    aa = clean_num(aa);
    var bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
    bb = clean_num(bb);
    return compare_numeric(aa,bb);
};


function compare_numeric(a,b)
{
    var a = parseFloat(a);
    a = (isNaN(a) ? 0 : a);
    var b = parseFloat(b);
    b = (isNaN(b) ? 0 : b);
    return a - b;
};


function ts_sort_caseinsensitive(a,b)
{
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).toLowerCase();
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).toLowerCase();
    if (aa==bb)
    {
        return 0;
    }
    if (aa<bb)
    {
        return -1;
    }
    return 1;
};


function ts_sort_default(a,b)
{
    aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
    bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
    if (aa==bb)
    {
        return 0;
    }
    if (aa<bb)
    {
        return -1;
    }
    return 1;
};


function clean_num(str)
{
    str = str.replace(new RegExp(/[^-?0-9.]/g),"");
    return str;
};


function trim(str)
{
    return str.replace(/^\\s+|\\s+\$\/g, "");
};
