/*
 * javascript html util 
 */

//helper to construct a html tag's property pair string : name='value'
function html_tag_attr(name, value){
	return name + "='" + value + "' ";
}
	
function html_tag_attr_ext(obj){
	var attr = ''
	for (key in obj){
		attr += key + "='" + obj[key] + "' "
	}
	return attr 
}

function html_tag(tag, attr, text){
	return '<' + tag + ' ' + attr + '>' + text + '</' + tag + '>';
}

//return n '&nbsp'
function html_nbsp(n){
	var NBSP = '&nbsp;';
	var rtn = NBSP; 
	for(var i=2; i<=n; i++)
		rtn += NBSP
	return rtn;
}

 
