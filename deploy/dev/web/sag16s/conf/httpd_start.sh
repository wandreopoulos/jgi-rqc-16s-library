#!/bin/bash -l
module load python
module load mysql
module load consed
module load jazz_trim
module load blast

export PYTHONPATH=/global/projectb/sandbox/rqc/andreopo/sag16s_deploy/dev/jgi-rqc-16s-library/deploy/dev/software/sag16s/RQC.Core.Prod.Egg:/global/projectb/projectdirs/PI/rqc/prod/rqc_software/Python:$PYTHONPATH

/global/projectb/sandbox/rqc/andreopo/sag16s_deploy/dev/jgi-rqc-16s-library/deploy/dev/web/sag16s/bin/httpd -k start -f /global/projectb/sandbox/rqc/andreopo/sag16s_deploy/dev/jgi-rqc-16s-library/deploy/dev/web/sag16s/conf/httpd.conf

