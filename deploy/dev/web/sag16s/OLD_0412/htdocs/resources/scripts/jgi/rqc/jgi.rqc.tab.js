/*
 * A jQuery plugin javascript library for menu bar.
 * Requirement: jQuery, 
 *              resources/scripts/jgi/rqc/jgi.rqc.html_util.js
 *              resources/styles/jgi/rqc/div_tab.css
 *
 * Support multiple instances of the tabbed blocks 
 *
 * Author: Shijie Yao
 * Last modified : 12/8/2012
 * 
 * configuration 
 * { 
 * }
 *   
 */
 
 /* 
  * An anonymous self-invoking function that creates a namespace
  */
 (function (base){
	'use restrict'
	
	var $ = base

	var tab_class_active = 'tabberactive'
	var tab_pane_class = 'tabbertab'
	var tab_pane_class_hide = 'tabbertab tabbertabhide'
	var tab_head_class = 'tabhead'

	//extend jQuery's function list to have jQuery(SELECT).menubar(var)
	//where *this* is the jQuery selection object, and *var* is pass-in param 
	base.fn.tab = function( obj ) {
		$(this)[0].tab_id_root = $(this).attr('id') + '_'
		$(this)[0].last_tab_index = -1 
		$(this)[0].active_tab_index = 0 	// default active tab 

		$(this)[0].tab_label_id_root = $(this).attr('id') + '_label_'
		$(this)[0].tab_panel_root = $(this).attr('id') + '_pane_'
		$(this)[0].tab_ul_id = $(this).attr('id') + '_tab_container'
		$(this)[0].tab_pane_div_id = $(this).attr('id') + '_tab_pane_container'

		var cnt = 0
		if( obj && obj.count )
			cnt = obj.count

		var tr = tab = ul = li = '';
		for(n=0; n<cnt; n++){
			var cls = '';
			if(n == 0)
				cls = tab_class_active 
			li += make_tab($(this)[0], n, cls) + '\n'
		}

		ul = html_tag('ul', html_tag_attr('id', $(this)[0].tab_ul_id) +  
							html_tag_attr('class', 'tabbernav'), 
							'\n' + li)
		
		tab = ul;
		for(n = 0; n<cnt; n++){
			var cls = tab_pane_class 
			if(n > 0)
				cls = tab_pane_class_hide 
			tab += make_tab_pane(obj, n, cls) 
		}

		tab = html_tag('div',	html_tag_attr('id', $(this)[0].tab_pane_div_id) +  
								html_tag_attr('class', 'tabberlive'), 
								'\n' + tab) + '\n'

		tr = html_tag('tr', '', html_tag('td', '', '\n' + tab) + '\n');
		
		this.html(tr)
		attach_tab_event_handler();
	};

	function attach_tab_event_handler(){
		//remove previously bind event handler: avoid multiple binding
		$('.'+tab_head_class).unbind('click')

		//add a new handler
		$('.'+tab_head_class).click(swapTab)
	}
	
	function swapTab(event) {
		//hide old, and show new tab
		var obj = $(this).parents('table')[0],
			newTabIdx = oldTabIdx = obj.active_tab_index,	
			tabId = $(this)[0].id;
		var tabNum = tabId.substr(tabId.lastIndexOf('_') + 1) 
		if(tabNum) 
			newTabIdx =  parseInt(tabNum)

		var thisOne = obj.tab_id_root + '_li_' + newTabIdx
		var preOne = obj.tab_id_root + '_li_' + oldTabIdx
		
		$('#'+preOne).attr('class', '')
		$('#'+thisOne).attr('class', tab_class_active)
		
		//hide old and show new panel
		thisOne = obj.tab_panel_root + newTabIdx
		preOne = obj.tab_panel_root + oldTabIdx
		
		$('#'+preOne).attr('class', tab_pane_class_hide)
		$('#'+thisOne).attr('class', tab_pane_class)

		//save the new active tab index
		obj.active_tab_index = newTabIdx
	}

	base.fn.set_html_to_tab = function(idx, html){
		$('#'+ $(this)[0].tab_panel_root + idx).html(html)
	} 

	base.fn.set_tab_label = function(idx, label){
		$('#'+ $(this)[0].tab_label_id_root + idx).text(label)
	} 

	base.fn.add_tab = function(label){
		var idx = add_a_tab($(this)[0], label) //return the index of the just added tab
		add_tab_pane($(this)[0], idx, label);
		attach_tab_event_handler();
	}

	//helper. obj is the topmost table DOM object
	function add_a_tab(obj, label){
		obj.last_tab_index++;
		var idx = obj.last_tab_index;
		var cls = tab_class_active 
		if(idx > 0)
			cls = ''

		html  = $('#' + obj.tab_ul_id).html();
		html += make_tab(obj, idx, cls, label); 
		$('#' + obj.tab_ul_id).html(html);

		return obj.last_tab_index
	}

	function add_tab_pane(obj, idx, label){
		var cls = tab_pane_class
		if(idx > 0)
			cls = tab_pane_class_hide
		html  = $('#' + obj.tab_pane_div_id).html();
		html += make_tab_pane(obj, idx, cls, label); 
		$('#' + obj.tab_pane_div_id).html(html);
	}

	function make_tab(obj, idx, cls, label){
		if(!label)
			label = 'tab ' + idx
		return html_tag('li', html_tag_attr('id', obj.tab_id_root+'_li_'+idx) + html_tag_attr('class', cls), 
							html_tag('a', '',
							html_tag('span', html_tag_attr_ext({id   :obj.tab_label_id_root + idx, 
																style:'cursor: pointer',
																class:tab_head_class}),
											label)))
	}

	function make_tab_pane(obj, idx, cls, text){
		if(!text)
			text = idx	
		return html_tag('div', html_tag_attr('id', obj.tab_panel_root + idx) + html_tag_attr('class', cls), text)
	}
 })(jQuery);
 

