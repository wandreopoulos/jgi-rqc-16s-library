// this script is for the failure mode UI synchronization
// setType: the value currently selected from the WHERE part of the mode;
// divID  : the <div> selement hosting the two mode <select>
// flag   : 0 for generic UI with default sync event handler 
//          1 for display_report_illumina.cgi UI will additional sync events
//jQuery version:
function fmSelected(selType, divId, flag)
{
	if(window.wait_cursor) //use only when it exists
    	wait_cursor(true);

	//save original params:
	var div = document.getElementById(divId);
	var inputs = div.getElementsByTagName('select');
	var ori_ids = Array(2);
	var ori_style = Array(2);
	ori_ids[0] = inputs[0].id;
	ori_ids[1] = inputs[1].id;
	ori_style[0] = inputs[0].className;
	ori_style[1] = inputs[1].className;

	var param = {failureModeSel : selType};
	if(flag == 0){
		param.fmWhereId = ori_ids[0];
		param.fmWhatId = ori_ids[1];
		param.fmDivId = divId; 
	}
	
	jQuery.ajax({
		url  : '/cgi-bin/display_report_illumina.cgi', 
		type : 'post',
		data : param,
		async: false,
		success: function(data){
					//do not use jQuery('#'+divId).html(data) here
					//as, if the divId string has '.', it will not work
					document.getElementById(divId).innerHTML = data;
					div = document.getElementById(divId);
					inputs = div.getElementsByTagName('select');
						
					//restore id:
					inputs[0].id = ori_ids[0];
					inputs[1].id = ori_ids[1];

					//restore class:
					inputs[0].className = ori_style[0]; 
					inputs[1].className = ori_style[1]; 
				}
    });

	if(window.wait_cursor)
		wait_cursor(false);
}


