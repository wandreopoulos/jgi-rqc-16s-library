/*
 * execute prototype ajax call. 
 * url 	: the url for ajax to call 
 * div 	: the <div> id for receiving the response
 * param: the {name: value, ...} associative array for ajax param
 */
function exe_ajax(url, div, param)
{
	new Ajax.Updater(div, url, { parameters: param});
}

