/**
 * TEMPLATE
 */

var gridCols = {set:false};

$(document).ready(function(){
	var grid = $('#listTable'); // JQuery object reference of table used for our grid display
	
	grid.jqGrid({
 		url:'/cgi-bin/python/rqc_resequence_job_list.py', 	// backend cgi returning the table data
		datatype: "json",								// the table data format the cgi returns 
		loadui: 'block',
		altRows: true,
		width:	1600,
		height: 700,
		colNames:['Reseq ID','FASTQ','Seq Unit Name','Start','End','Refgenome Search',
					'Bio Name','Type','Vers Ext','Vers Int','Status','Description','BAM File'], 
		colModel:[ 
			{name:'reseq_id',index:'reseq_id', width:30, align:"center", searchoptions: {sopt:['eq','bw','ew','cn','in']}},
			{name:'fastq',index:'fastq', width:62, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}},
			{name:'seq_unit_name',index:'seq_unit_name',width:62,align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}},
			{name:'start_time',index:'start_time', width:60, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
			{name:'stop_time',index:'stop_time', width:60, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
			{name:'reseq_ref_search',index:'reseq_ref_search', width:125, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
 			{name:'ref_bioname',index:'ref_bioname', width:125, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
 			{name:'ref_type',index:'ref_type', width:30, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'version',index:'version', width:30,align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}}, 
			{name:'rqc_version',index:'rqc_version', width:30,align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}}, 
			{name:'status',index:'status', width:30, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'reseq_descr',index:'reseq_descr', width:125, sortable:false,searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'bam_file',index:'bam_file', width:62, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
		], 

		rowNum:100, rowList:[100,500,1000], // for paging, default view block size, and size selection
		pager: '#listTablePager', 	// the page UI div id
		toppager: true,		// also show pager at top of table
		sortname: 'reseq_id',	// default sorting column
		sortorder: "desc", 		// default sorting order
		viewrecords: true, 		// display "View 1-100 of 12345" at pager right
		caption:"resequence jobs and status",	// sohw the top caption UI 
		
 	});

	// show the out-of-box search tool (cloneToTop:true also put all tool on top pager):
	grid.jqGrid('navGrid', '#listTablePager', { edit:false, add:false, del:false, view:true, search:true, cloneToTop:true} );

	//the summary table
	$('#summaryTable').jqGrid({
 		url:'/cgi-bin/python/rqc_resequence_job_summary.py', 	// backend cgi returning the table data
		datatype: "json",								// the table data format the cgi returns 
		loadui: 'block',
		altRows: true,
		width:	1600,
		height: 20,
		colNames:['Total','Succeeded','Failed', 'Pending', 'Oldest','Newest'], 
		colModel:[ 
			{name:'total',index:'total', width:30, align:"center", sortable:false}, 
			{name:'succeeded',index:'succeeded', width:30, align:"center", sortable:false}, 
			{name:'failed',index:'failed', width:30, align:"center", sortable:false}, 
			{name:'pending',index:'pending', width:30, align:"center", sortable:false}, 
			{name:'oldest',index:'oldest', width:30, align:"center", sortable:false}, 
			{name:'newest',index:'newest', width:30, align:"center", sortable:false}, 
		], 

		rowNum:100, rowList:[100,500,1000], // for paging, default view block size, and size selection
		sortname: 'newest',	// default sorting column
		sortorder: "desc", 		// default sorting order
		viewrecords: true, 		// display "View 1-100 of 12345" at pager right
		caption:"resequence job summary",	// sohw the top caption UI 
 	});


});
