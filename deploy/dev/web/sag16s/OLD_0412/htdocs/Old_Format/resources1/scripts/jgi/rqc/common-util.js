// js functions for general purpose untils 
// Shijie Yao
// Feb, 9, 2012

/* For a given <select> object, return the selected value string.
 * If multi-selection is enabled, concatenate all values,
 * using ";" as deliminater
 */
function join_select_values(obj)
{
    var str = "";
    for(var i=0; i<obj.options.length; i++) {
        if(obj.options[i].selected) {
            if(str.length > 0) str += ";";
            str += obj.options[i].value;
        }
    }

    return str;
}

// format seqUnits list (eg "A    B,C;D ") into "A:B:C:D";
// do not use ';' as deliminator, as web client will truncate from first ';'
function formalizeTxt(txt)
{
	var tokens = txt.split(/ /);	// by spaces
	
	var str = "";
	for(var n=0; n<tokens.length; n++)
	{
		if(tokens[n] != "")
		{
			var tmp = tokens[n].split(/[;|,]/);
			for(var m=0; m<tmp.length; m++)
			{
				if(str != "" && tmp[m] != "")
					str += ":";
				str += tmp[m];
			}
		}
	}

	return str;
}

/* for a give object of {key=val}, trim val */
function trim_obj_val(obj)
{
	for (var n in obj) {
    	obj[n] = trim(obj[n])
	}
}


function show_for_js_aa(param)
{
    var txt = "";
    for(var i in param) {
        if(txt != "")
            txt += " "
        txt += i + '=' +  param[i];
    }
    alert(txt);
}

/*
 * trim leading and trailing white spaces in the given str.
 */
function trim(str)
{
	str = '' + str;	//be sure str is a string
	return str.replace(/^\s+|\s+$/g, "")
}

function chomp(str)
{
	str = '' + str;	//be sure str is a string
	return str.replace(/(\n|\r)+$/, '')
}

// convert string in '{a:val1,b:val2}', or 'a:val1,b:val2' into js obj
function str_to_obj(jstr)
{
    var param = {}
    jstr = jstr.trim();
    if(jstr.indexOf('{')==0)
        jstr = jstr.substr(1)
    if(jstr.indexOf('}') == jstr.length -1)
        jstr = jstr.substr(0, jstr.length -1)

    var list = jstr.split(/\s*,\s*/);
    list.forEach(function(pair){
        var tup = pair.split(/\s*:\s*/)
        param[tup[0]] = tup[1]
    });
    return param
}

// To show the busy cursor and a shade pan (must be glass class) cover the entire 
// window to block user action while waiting
// toWait : boolean 
// the page need to install window resize event handler to size glass pan to cover the page  
//    jQuery(window).resize(stretch_glass)
function wait_cursor(toWait)
{
   	if(toWait){ 
		jQuery('.glass').css('cursor','progress')
		jQuery('.glass').show() 
	} else {
		jQuery('.glass').hide()
	}
}

//stretch glass panel (class='glass') to window
function stretch_glass()
{
	var w = jQuery('body')[0].scrollWidth, 
		h = jQuery('body')[0].scrollHeight; 

    jQuery('.glass').width(w);
    jQuery('.glass').height(h);
}


// return a GUID
function GUID()
{
    var S4 = function ()
    {
        return Math.floor(
                Math.random() * 0x10000 /* 65536 */
            ).toString(16);
    };

    return (
            S4() + S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + S4() + S4()
        );
}

// ajax jQuery proxy call
// url 	 : the URL
// param : query string needed by URL, in javascript object type of {key:value, ...} form
// type  : can be 'get', 'post'; default to 'post'
// return: javascript of {success : return-value} or {error : error-message}
//         if ajax call itself fails, then return null
function ajax(url, param, type){
    var rtn = null
	type = typeof type !== 'undefined' ?  type : 'post'
    jQuery.ajax({
        url  : url,
        type : type,
        data : param,
        //async: false,
        success: function(data){
            rtn = {success: data}
        },
        error : function(x, t, m) { //(xmlhttprequest, textstatus, message)
            rtn = {error: t + ":" + m}
        }
    })

    return rtn;
}
