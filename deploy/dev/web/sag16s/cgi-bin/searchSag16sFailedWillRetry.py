#!/usr/bin/env python

'''
Created on Dec 11, 2012

@author: asyed, andreopo
'''


import cgi
import json
import MySQLdb
import sys

from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb
import core.utils.commons


class PrintWeb(object):
    __header_done = False
    def __init__(self):
        pass
    
    def printHeader(self):
        print 'Content-type: application/json\n'
        self.__header_done = True
    
    def printJSON(self, response):
        if not self.__header_done : self.printHeader()
        print json.dumps(response)
        sys.exit(0)
    
    def printUsage(self):
        pass
    
    def print500(self, error=None):
        if not self.__header_done: self.printHeader()
        print "Status: 500 Internal Error database connection failed\n\n"
        if error is not None: print "Message: "+error
        sys.exit(1)
        
    def print400(self):
        pass

    


'''
Get list of records by name:
    This retrieves a list of Sag16s_Seq records by seq name, which is provided as input

@param logger: Logging object
@return : returns a list of records with this seq_name

'''
def getRecordsByName(seq_name, logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records by seq name: "+str(seq_name))
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        records = session.query(SAGSequence).filter(SAGSequence.seq_name == seq_name).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records


def getFailed(propsal, project, sample, userName, pcrPlate, sortWindow, wellType, seqNames, logger):
    retVal = None
    sql = core.utils.commons.getFailedWillRetrySQL(propsal, project, sample, userName, pcrPlate, sortWindow, wellType, seqNames)
    logger.debug("SQL to retrieve all Hits in getFailed: "+ sql)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(sql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns by getFailed in getSag16sSearch: "+str(rowsAffected))
        result = dbObj.fetchall()
        logger.debug(result)
        response = []
        hitsMap = dict()
        #prefix = " ( "
        ssid = " ( "
        for row in result:
            #sag16sList = getRecordsByName(row[1], logger)
            trim_seq = ""
            #for seq in sag16sList :
            #    trim_seq = ""
            # seq = row
            # logger.info("SEQ_INFO for " + str(row[1]))
            trim_seq_file = str(row[14]) + "/" + core.utils.commons.getSag16sFaName( str(row[1]) )+".trim"
            # logger.info("trim_seq_file location: " + trim_seq_file)
            # Read the file and all lines and set string trim_seq to the concat of the lines.
            f = open(trim_seq_file, 'r')
            # Skip the 1st header line in the fasta file
            f.readline()
            for line in f:
                trim_seq += line.strip()
            f.close()
            # logger.info("trim_seq: " + trim_seq)

            hit = dict()
            #prefix = prefix+" '"+row[0]+"', "
            ssid = ssid+" "+str(row[0])+", "
            hit['seq_name'] = row[1]
            hit['user_name'] = row[2]
            hit['proposal_id'] = row[3]
            hit['project'] = row[4]
            hit['sample'] = row[5]
            hit['sort_window'] = row[6]
            hit['plate_num'] = row[7]
            hit['mda_plate_well_pos'] = row[8]
            hit['well_type'] = row[9]
            hit['sanger_well_pos'] = row[10]
            hit['sanger_well_loc'] = row[11]
            hit['q15_tlength'] = row[12]
            hit['seq_name_marked'] = row[13]
            hit['fs_location'] = row[14]
            hit['dt_status_modified'] = row[15]            
            hit['pcr_plate'] = row[16]
            hit['trim_seq'] = trim_seq
            hitsMap[row[0]] = hit
        ssid  = ssid+" -1)"
        response.append(ssid)
        response.append(hitsMap)
        retVal = response
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getFailed() ")
    finally:
        dbObj.disconnect()
    return retVal


def getRDPHits(ssidString, logger):
    retVal = None
    rsql = ("SELECT domain_name, domain_score, kingdom_name, kingdom_score, phylum_name, phylum_score, "+
           " class_name, class_score, order_name, order_score, family_name, family_score, genus_name, "+
           " genus_score, fs_location, sag_seq_id FROM sag16s_rdp WHERE sag_seq_id IN "+ssidString)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(rsql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns getRDPHits: "+str(rowsAffected))
        result = dbObj.fetchall()
        response = dict()
        
        for row in result:
            rdpHit = dict()
            # RDP HIT
            rdpHit['domain_name'] = row[0]
            rdpHit['domain_score'] = row[1]
            rdpHit['kingdom_name'] = row[2]
            rdpHit['kingdom_score'] = row[3]
            rdpHit['phylum_name'] = row[4]
            rdpHit['phylum_score'] = row[5]
            rdpHit['class_name'] = row[6]
            rdpHit['class_score'] = row[7]
            rdpHit['order_name'] = row[8]
            rdpHit['order_score'] = row[9]
            rdpHit['family_name'] = row[10]
            rdpHit['family_score'] = row[11]
            rdpHit['genus_name'] = row[12]
            rdpHit['genus_score'] = row[13]
            rdpHit['fs_location'] = row[14]
            response[row[15]] = rdpHit
        retVal = response
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getRDPHits() ")
    finally:
        dbObj.disconnect()
    return retVal


def getBLASTHits(ssidString, logger):
    bsql = ("SELECT percenti, align_length, mismatches, gaps, q_start, q_end, s_start, s_end ,"+
            " evalue, score, fs_location, analysis_type, sag_seq_id, subject_id, phylogeny FROM sag16s_top_hit WHERE "+
            " sag_seq_id IN "+ssidString)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(bsql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns getBLASTHits: "+str(rowsAffected))
        result = dbObj.fetchall()
        response = []
        homologyHits = dict()
        contamHits = dict()
        homologyGGHits = dict()
        homologyIMGHits = dict()
        for row in result:
            blastHit = dict()
            # BLAST hit
            blastHit['percenti'] = row[0]
            blastHit['subject_id'] = row[13]
            blastHit['align_length'] = row[1]
            blastHit['mismatches'] = row[2]
            blastHit['gaps'] = row[3]
            blastHit['q_start'] = row[4]
            blastHit['q_end'] = row[5]
            blastHit['s_start'] = row[6]
            blastHit['s_end'] = row[7]
            blastHit['evalue'] = row[8]
            blastHit['score'] = row[9]
            blastHit['fs_location'] = row[10]
            blastHit['phylogeny'] = row[14]
            if ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY ) :
                homologyHits[row[12]] = blastHit
            elif ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_GREENGENES ) :
                homologyGGHits[row[12]] = blastHit
            elif ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_IMG ) :
                homologyIMGHits[row[12]] = blastHit
            else:
                contamHits[row[12]] = blastHit
        response.append(homologyHits)
        response.append(contamHits)
        response.append(homologyGGHits)
        response.append(homologyIMGHits)
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getBLASTHits() ")
    finally:
        dbObj.disconnect()
    return response


def main():
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("searchSAG16s")
    logger.debug("Starting the searchSag16s")
    form = cgi.FieldStorage()
    # Search page variables
    mProject = form.getvalue('project')
    mSample = form.getvalue('sample')
    mProposal = form.getvalue('proposalId')
    mSearchUserName = form.getvalue('searchUserName')
    mPCRPlate = form.getvalue('pcr_plate')
    mSortWindow = form.getvalue('sWindow')
    mWellType = form.getvalue('wType')
    mSeqNames = form.getvalue('seq_names')
    
    if mProject != None:
        mProject = mProject.strip()
    if mSample != None:
        mSample = mSample.strip()
    if mProposal != None:
        mProposal = mProposal.strip()
    if mSearchUserName is not None:
        mSearchUserName = mSearchUserName.strip()
    if mPCRPlate != None:
        mPCRPlate = mPCRPlate.strip()
    if mSortWindow != None:
        mSortWindow = mSortWindow.strip()
    if mWellType != None:
        mWellType = mWellType.strip()
    if mSeqNames != None:
        mSeqNames = mSeqNames.strip()
    
    #
    # Retrieve all corresponding hits for search terms, you'll get
    # relevant sequence Ids
    hitResult = getFailed(mProposal, mProject, mSample, mSearchUserName, mPCRPlate, mSortWindow, mWellType, mSeqNames, logger)
    ssidString = hitResult[0]
    hitsMap = hitResult[1]
    logger.debug("Retrieved hits: "+ssidString)
    # Retrieve all Blast Hits, these may contain either Blast Homology or Contam hits or both
    blastHits = getBLASTHits(ssidString, logger)
    blastHomology = blastHits[0]
    blastContam = blastHits[1]
    blastHomologyGG = blastHits[2]
    blastHomologyIMG = blastHits[3]
    # Retrieve RDP classifier hits
    rdpHits = getRDPHits(ssidString, logger)
    # Final output JSON HashMap
    output = []
    output.append(['Sequence Name','User','PCR Plate','Proposal', 'Project', 'Sample','Sort Window','Plate Num','MDA WellPos','Well Type','Sang WPos', 'Sang WLoc',
                   'Q15','Marked',
                   'Blast Top Hit nt','% Match nt','AlignLen nt', 'Mismatches nt','Gaps nt','Q-Start nt','Q-End nt','Sub-Start nt','Sub-End nt', 'EValue nt','Score nt',
                   'Phylogeny(blast hit nt)','ContamInfo','Domain(score)','Kingdom(score)','Phylum(score)','Class(score)','Order(score)','Family(score)',
                   'Genus(score)', 
                   'Blast Top Hit LSSUSilva','% Match LSSUSilva','AlignLen LSSUSilva', 'Mismatches LSSUSilva','Gaps LSSUSilva','Q-Start LSSUSilva','Q-End LSSUSilva','Sub-Start LSSUSilva','Sub-End LSSUSilva', 'EValue LSSUSilva','Score LSSUSilva',
                   'Phylogeny(blast hit LSSUSilva)',
                   'Blast Top Hit img','% Match img','AlignLen img', 'Mismatches img','Gaps img','Q-Start img','Q-End img','Sub-Start img','Sub-End img', 'EValue img','Score img',
                   'Phylogeny(blast hit img)',
                   'Quality Trimmed 16S Sequence'])
    for key in hitsMap.keys():
        hitList = list()
        value = hitsMap.get(key)
        logger.debug("Getting key "+str(key))
        hitList.append(value.get('seq_name'))
        hitList.append(value.get('user_name'))
        hitList.append(value.get('pcr_plate'))
        hitList.append(value.get('proposal_id'))
        hitList.append(value.get('project'))
        hitList.append(value.get('sample'))
        hitList.append(value.get('sort_window'))
        hitList.append(value.get('plate_num'))
        hitList.append(value.get('mda_plate_well_pos'))
        hitList.append(value.get('well_type'))
        hitList.append(value.get('sanger_well_pos'))
        hitList.append(value.get('sanger_well_loc'))
        hitList.append(value.get('q15_tlength'))
        hitList.append(value.get('seq_name_marked'))
        if( blastHomology.has_key(key) ):
            blastHit = blastHomology.get(key)
            hitList.append(blastHit['subject_id'])
            hitList.append(blastHit['percenti'])
            hitList.append(blastHit['align_length'])
            hitList.append(blastHit['mismatches'])
            hitList.append(blastHit['gaps'])
            hitList.append(blastHit['q_start'])
            hitList.append(blastHit['q_end'])
            hitList.append(blastHit['s_start'])
            hitList.append(blastHit['s_end'])
            hitList.append(blastHit['evalue'])
            hitList.append(blastHit['score'])
            hitList.append(blastHit['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
        if( blastContam.has_key(key) ):
            blastHit = blastContam.get(key)
            hitList.append('Sub: '+str(blastHit['subject_id'])+" % ID: "+str(blastHit['percenti'])+" AlignLen: "+str(blastHit['align_length']))
        else:
            hitList.append('-')
        if( rdpHits.has_key(key)):
            rdpHit = rdpHits.get(key)
            hitList.append(rdpHit['domain_name']+"("+str(rdpHit['domain_score'])+")")
            hitList.append(rdpHit['kingdom_name']+"("+str(rdpHit['kingdom_score'])+")")
            hitList.append(rdpHit['phylum_name']+"("+str(rdpHit['phylum_score'])+")")
            hitList.append(rdpHit['class_name']+"("+str(rdpHit['class_score'])+")")
            hitList.append(rdpHit['order_name']+"("+str(rdpHit['order_score'])+")")
            hitList.append(rdpHit['family_name']+"("+str(rdpHit['family_score'])+")")
            hitList.append(rdpHit['genus_name']+"("+str(rdpHit['genus_score'])+")")
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')

        ### Print Blast against Greengenes results.
        if( blastHomologyGG.has_key(key) ):
            blastHitGG = blastHomologyGG.get(key)
            hitList.append(blastHitGG['subject_id'])
            hitList.append(blastHitGG['percenti'])
            hitList.append(blastHitGG['align_length'])
            hitList.append(blastHitGG['mismatches'])
            hitList.append(blastHitGG['gaps'])
            hitList.append(blastHitGG['q_start'])
            hitList.append(blastHitGG['q_end'])
            hitList.append(blastHitGG['s_start'])
            hitList.append(blastHitGG['s_end'])
            hitList.append(blastHitGG['evalue'])
            hitList.append(blastHitGG['score'])
            hitList.append(blastHitGG['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')

        ### Print Blast against IMG results.
        if( blastHomologyIMG.has_key(key) ):
            blastHitIMG = blastHomologyIMG.get(key)
            hitList.append(blastHitIMG['subject_id'])
            hitList.append(blastHitIMG['percenti'])
            hitList.append(blastHitIMG['align_length'])
            hitList.append(blastHitIMG['mismatches'])
            hitList.append(blastHitIMG['gaps'])
            hitList.append(blastHitIMG['q_start'])
            hitList.append(blastHitIMG['q_end'])
            hitList.append(blastHitIMG['s_start'])
            hitList.append(blastHitIMG['s_end'])
            hitList.append(blastHitIMG['evalue'])
            hitList.append(blastHitIMG['score'])
            hitList.append(blastHitIMG['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')

        hitList.append(value.get('trim_seq'))
        output.append(hitList)

    #
    # Print the JSON object back to JQGrid
    printObj = PrintWeb()
    printObj.printJSON(output)

     
if __name__ == '__main__':
    #mainL = []
    #mainL.append(['Sequence_Name1', 'proposalId1'])
    main()
