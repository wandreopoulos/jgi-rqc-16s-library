#!/usr/bin/env python
###/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

'''
Created on Dec 11, 2012

@author: asyed, andreopo
'''


import cgi
import json
import MySQLdb
import sys
import math

from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb
import core.utils.commons

class PrintWeb(object):
    __header_done = False
    def __init__(self):
        pass
    
    def printHeader(self):
        print 'Content-type: application/json\n'
        self.__header_done = True
    
    def printJSON(self, response):
        if not self.__header_done: self.printHeader()
        print json.dumps(response)
        sys.exit(0)
    
    def printUsage(self):
        pass
    
    def print500(self, error=None):
        if not self.__header_done: self.printHeader()
        print "Status: 500 Internal Error database connection failed\n\n"
        if error is not None: print "Message: "+error
        sys.exit(1)
        
    def print400(self):
        pass
    

def getFailed(propsal, project, sample, userName, pcrPlate, sortWindow, wellType, sorti, sortd, logger):
    retVal = None
    sql = core.utils.commons.getSkippedSQL(propsal, project, sample, userName, pcrPlate, sortWindow, wellType, None, sorti, sortd)
    logger.debug("SQL to retrieve all Hits: "+ sql)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(sql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns getFailed in getSag16sSearch: "+str(rowsAffected))
        result = dbObj.fetchall()
        hitsMap = dict()
        response = []
        # populate the SAG seq IDs that are hits
        position = 0
        positionMap = dict()
        ssid = " ( "
        for row in result:
            hit = dict()
            ssid = ssid+" "+str(row[0])+", "
            hit['seq_name'] = row[1]
            hit['user_name'] = row[2]
            hit['proposal_id'] = row[3]
            hit['project'] = row[4]
            hit['sample'] = row[5]
            hit['sort_window'] = row[6]
            hit['plate_num'] = row[7]
            hit['mda_plate_well_pos'] = row[8]
            hit['well_type'] = row[9]
            hit['sanger_well_pos'] = row[10]
            hit['sanger_well_loc'] = row[11]
            hit['q15_tlength'] = row[12]
            hit['seq_name_marked'] = row[13]
            hit['fs_location'] = row[14]
            hit['dt_status_modified'] = row[15]
            hit['pcr_plate'] = row[16]
            hitsMap[row[0]] = hit
            positionMap[row[0]] = position
            position = position+1
            logger.debug(hit)
        ssid  = ssid+" -1)"
        logger.debug(ssid)
        response.append(ssid)
        response.append(hitsMap)
        response.append(positionMap)
        logger.debug(positionMap)
        retVal = response
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getFailed() ")
    finally:
        dbObj.disconnect()
    return retVal


def getRDPHits(ssidString, logger):
    retVal = None
    rsql = ("SELECT domain_name, domain_score, kingdom_name, kingdom_score, phylum_name, phylum_score, "+
           " class_name, class_score, order_name, order_score, family_name, family_score, genus_name, "+
           " genus_score, fs_location, sag_seq_id FROM sag16s_rdp WHERE sag_seq_id IN "+ssidString)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(rsql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns getRDPHits: "+str(rowsAffected))
        result = dbObj.fetchall()
        response = dict()
        for row in result:
            rdpHit = dict()
            # RDP HIT
            rdpHit['domain_name'] = row[0]
            rdpHit['domain_score'] = row[1]
            rdpHit['kingdom_name'] = row[2]
            rdpHit['kingdom_score'] = row[3]
            rdpHit['phylum_name'] = row[4]
            rdpHit['phylum_score'] = row[5]
            rdpHit['class_name'] = row[6]
            rdpHit['class_score'] = row[7]
            rdpHit['order_name'] = row[8]
            rdpHit['order_score'] = row[9]
            rdpHit['family_name'] = row[10]
            rdpHit['family_score'] = row[11]
            rdpHit['genus_name'] = row[12]
            rdpHit['genus_score'] = row[13]
            rdpHit['fs_location'] = row[14]
            response[row[15]] = rdpHit
        retVal = response
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getRDPHits() ")
    finally:
        dbObj.disconnect()
    return retVal


def getBLASTHits(ssidString, logger):
    bsql = ("SELECT percenti, align_length, mismatches, gaps, q_start, q_end, s_start, s_end ,"+
            " evalue, score, fs_location, analysis_type, sag_seq_id, subject_id, phylogeny FROM sag16s_top_hit WHERE "+
            " sag_seq_id IN "+ssidString)
    dbObj = None
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(bsql)
        rowsAffected = dbObj.get_rows_affected()
        logger.info("Total retrieved columns getBLASTHits: "+str(rowsAffected))
        result = dbObj.fetchall()
        response = []
        homologyHits = dict()
        contamHits = dict()
        homologyGGHits = dict()
        homologyIMGHits = dict()
        for row in result:
            blastHit = dict()
            # BLAST hit
            blastHit['percenti'] = row[0]
            blastHit['subject_id'] = row[13]
            blastHit['align_length'] = row[1]
            blastHit['mismatches'] = row[2]
            blastHit['gaps'] = row[3]
            blastHit['q_start'] = row[4]
            blastHit['q_end'] = row[5]
            blastHit['s_start'] = row[6]
            blastHit['s_end'] = row[7]
            blastHit['evalue'] = row[8]
            blastHit['score'] = row[9]
            blastHit['fs_location'] = row[10]
            blastHit['phylogeny'] = row[14]
            if ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY ) :
                homologyHits[row[12]] = blastHit
            elif ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_GREENGENES ) :
                homologyGGHits[row[12]] = blastHit
            elif ( row[11] == SAGTopHit.ANALYSIS_TYPE_HOMOLOGY_IMG ) :
                homologyIMGHits[row[12]] = blastHit
            else:
                contamHits[row[12]] = blastHit
        response.append(homologyHits)
        response.append(contamHits)
        response.append(homologyGGHits)
        response.append(homologyIMGHits)
    except:
        printObj = PrintWeb()
        printObj.print500("Error retrieving the getBLASTHits() ")
    finally:
        dbObj.disconnect()
    return response


def main():
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("searchSAG16s")
    logger.debug("Starting the getSag16sSearch")
    form = cgi.FieldStorage()
    # JQGrid variables
    mViewPage  = form.getvalue('page')
    mRowLimit  = form.getvalue('rows')
    sidx  = form.getvalue('sidx')
    sortd  = form.getvalue('sord')
    # Search page variables
    mProject = form.getvalue('project')
    mSample = form.getvalue('sample')
    mProposal = form.getvalue('proposalId')
    mSearchUserName = form.getvalue('searchUserName')
    mPCRPlate = form.getvalue('pcrPlateId')
    mSortWindow = form.getvalue('sWindow')
    mWellType = form.getvalue('wType')
    
    if(not mRowLimit or not int(mRowLimit)):
        mRowLimit = 60
    else:
        mRowLimit = int(mRowLimit)
    
    if(not mViewPage or not int(mViewPage)):
        mViewPage = 1 
    else:
        mViewPage = int(mViewPage)

    if mProject != None:
        mProject = mProject.strip()
    if mSample != None:
        mSample = mSample.strip()
    if mProposal != None:
        mProposal = mProposal.strip()
    if mSearchUserName is not None:
        mSearchUserName = mSearchUserName.strip()
    if mPCRPlate != None:
        mPCRPlate = mPCRPlate.strip()
    if mSortWindow != None:
        mSortWindow = mSortWindow.strip()
    if mWellType != None:
        mWellType = mWellType.strip()
    #
    # Retrieve all corresponding hits for search terms, you'll get
    # relevant sequence Ids
    hitResult = getFailed(mProposal, mProject, mSample, mSearchUserName, mPCRPlate, mSortWindow, mWellType, sidx, sortd, logger)
    ssidString = hitResult[0]
    allHits = hitResult[1]
    positionMap = hitResult[2]
    logger.debug("Retrieved hits: "+ssidString)
    # Retrieve all Blast Hits, these may contain either Blast Homology or Contam hits or both
    blastHits = getBLASTHits(ssidString, logger)
    blastHomology = blastHits[0]
    blastContam = blastHits[1]
    blastHomologyGG = blastHits[2]
    blastHomologyIMG = blastHits[3]
    # Retrieve RDP classifier hits
    rdpHits = getRDPHits(ssidString, logger)
    # Final output JSON HashMap
    response = dict()
    response['records'] = len(allHits)
    response['page'] = mViewPage
    response['rows'] = [{}]*len(allHits)
    total_pages = math.ceil( float(len(allHits))/float(mRowLimit))
    if total_pages is 0 : total_pages = 1 
    response['total'] = total_pages
    logger.debug("Total pages Computed: "+str(math.ceil( len(allHits)/mRowLimit ))+ " Actual: "+str(total_pages))
    # relative row start and stop
    rel_row_start = (mViewPage-1)*mRowLimit
    rel_row_stop = (mViewPage)*mRowLimit-1
    logger.debug("Relative start stop: "+str(rel_row_start)+" : "+str(rel_row_stop))
    for key in allHits.keys():
        hitList = list()
        value = allHits.get(key)
        logger.debug("Getting key "+str(key))
        hitList.append(value.get('seq_name'))
        hitList.append(value.get('user_name'))
        hitList.append(value.get('pcr_plate'))
        hitList.append(value.get('proposal_id'))
        hitList.append(value.get('project'))
        hitList.append(value.get('sample'))
        hitList.append(value.get('sort_window'))
        hitList.append(value.get('plate_num'))
        hitList.append(value.get('mda_plate_well_pos'))
        hitList.append(value.get('well_type'))
        hitList.append(value.get('sanger_well_pos'))
        hitList.append(value.get('sanger_well_loc'))
        hitList.append(value.get('q15_tlength'))
        hitList.append(value.get('seq_name_marked'))

        if( blastHomology.has_key(key) ):
            blastHit = blastHomology.get(key)
            if len(blastHit['subject_id'].split("|")) >= 4:
                hitList.append("<a href=\"http://www.ncbi.nlm.nih.gov/nucleotide/"+ str(blastHit['subject_id'].split("|")[3]) +"\" target=\"_blank\">" + blastHit['subject_id'] + "</a>")
            else:
                hitList.append(blastHit['subject_id'])
            hitList.append(blastHit['percenti'])
            hitList.append(blastHit['align_length'])
            hitList.append(blastHit['mismatches'])
            hitList.append(blastHit['gaps'])
            hitList.append(blastHit['q_start'])
            hitList.append(blastHit['q_end'])
            hitList.append(blastHit['s_start'])
            hitList.append(blastHit['s_end'])
            hitList.append(blastHit['evalue'])
            hitList.append(blastHit['score'])
            hitList.append(blastHit['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            
        if( blastContam.has_key(key) ):
            blastHit = blastContam.get(key)
            hitList.append('Sub: '+str(blastHit['subject_id'])+" % ID: "+str(blastHit['percenti'])+" AlignLen: "+str(blastHit['align_length']))
        else:
            hitList.append('-')

        if( rdpHits.has_key(key)):
            rdpHit = rdpHits.get(key)
            hitList.append(rdpHit['domain_name']+"("+str(rdpHit['domain_score'])+")")
            hitList.append(rdpHit['kingdom_name']+"("+str(rdpHit['kingdom_score'])+")")
            hitList.append(rdpHit['phylum_name']+"("+str(rdpHit['phylum_score'])+")")
            hitList.append(rdpHit['class_name']+"("+str(rdpHit['class_score'])+")")
            hitList.append(rdpHit['order_name']+"("+str(rdpHit['order_score'])+")")
            hitList.append(rdpHit['family_name']+"("+str(rdpHit['family_score'])+")")
            hitList.append(rdpHit['genus_name']+"("+str(rdpHit['genus_score'])+")")
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            

        ### Print Blast against Greengenes results.
        if( blastHomologyGG.has_key(key) ):
            blastHitGG = blastHomologyGG.get(key)
            hitList.append(blastHitGG['subject_id'])
            hitList.append(blastHitGG['percenti'])
            hitList.append(blastHitGG['align_length'])
            hitList.append(blastHitGG['mismatches'])
            hitList.append(blastHitGG['gaps'])
            hitList.append(blastHitGG['q_start'])
            hitList.append(blastHitGG['q_end'])
            hitList.append(blastHitGG['s_start'])
            hitList.append(blastHitGG['s_end'])
            hitList.append(blastHitGG['evalue'])
            hitList.append(blastHitGG['score'])
            hitList.append(blastHitGG['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')

        ### Print Blast against IMG results.
        if( blastHomologyIMG.has_key(key) ):
            blastHitIMG = blastHomologyIMG.get(key)
            hitList.append(blastHitIMG['subject_id'])
            hitList.append(blastHitIMG['percenti'])
            hitList.append(blastHitIMG['align_length'])
            hitList.append(blastHitIMG['mismatches'])
            hitList.append(blastHitIMG['gaps'])
            hitList.append(blastHitIMG['q_start'])
            hitList.append(blastHitIMG['q_end'])
            hitList.append(blastHitIMG['s_start'])
            hitList.append(blastHitIMG['s_end'])
            hitList.append(blastHitIMG['evalue'])
            hitList.append(blastHitIMG['score'])
            hitList.append(blastHitIMG['phylogeny'])
        else:
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')
            hitList.append('-')

        actual_key_pos = int(positionMap.get(key))
        if actual_key_pos>= rel_row_start and actual_key_pos < rel_row_stop :
            index = actual_key_pos-rel_row_start
            response['rows'][index] = dict()    
            response['rows'][index]['id'] = index
            response['rows'][index]['cell'] = hitList
            logger.debug("Apending: ")
            logger.debug(hitList)
    
    logger.debug("Response " + str(response))
    #
    # Print the JSON object back to JQGrid
    printObj = PrintWeb()
    printObj.printJSON(response)


if __name__ == '__main__':
    main()

