#!/usr/bin/env python
###/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

import json,cgi,re,time
from core.utils.rqc_logger import RQCLogger
from urllib2 import Request, urlopen, URLError, HTTPError
from core.utils.properties import Properties


def downloadCSV(searchStr, logger):
    ltime = time.localtime()
    tStamp = str(ltime.tm_year)+"_"+str(ltime.tm_mon)+"_"+str(ltime.tm_sec)+"_"
    fileName = tStamp+"Search_File_"+re.sub(r'[&,]','_',searchStr)+".csv"
    print "Content-Disposition: attachment; filename="+fileName
    print "Content-type: application/octet-stream\n"
    propObj = Properties()
    logger.debug(propObj.getSag16sURL()+"/cgi-bin/searchSag16sSkipped.py?"+searchStr)
    url = propObj.getSag16sURL()+"/cgi-bin/searchSag16sSkipped.py?"+searchStr
    req = Request(url)
    response = urlopen(req)
    result = response.read()
    rows = json.loads(result)
    
    for row in rows:
        counter = 0
        for element in row:
            row[counter] = str(element).replace(",",".")
            counter = counter+1
        print ",".join(row)
    


def main():
    logObj  = RQCLogger()
    logger  = logObj.getSag16sLogger("downloadCSV")
    logger.debug("Starting the DownloadCSV WebService")
    
    form = cgi.FieldStorage()
    logger.debug("Starting the analysis search process.")
    mProposal = form.getvalue('proposalId')
    mSample = form.getvalue('sample')
    mProject = form.getvalue('project')
    mSearchUserName = form.getvalue('searchUserName')
    mPCRPlate = form.getvalue('pcrPlateId')
    mSortWindow = form.getvalue('sWindow')
    mWellType = form.getvalue('wType')
    mSeqNames = form.getvalue('seqNames')

    mFormat = "csv"
    
    searchStr=''
    if mProject != None and mProject != '':
        mProject = mProject.strip()
        searchStr = searchStr + '&project='+str(mProject)
    if mSample != None and mSample != '':
        mSample = mSample.strip()
        searchStr = searchStr + '&sample='+str(mSample)
    if mProposal != None and mProposal != '':
        mProposal = mProposal.strip()
        searchStr = searchStr + '&proposalId='+str(mProposal)
    if mSearchUserName != None and mSearchUserName != '':
        mSearchUserName = mSearchUserName.strip()
        searchStr = searchStr + '&searchUserName='+str(mSearchUserName)
    if mWellType != None and mWellType != '':
        mWellType = mWellType.strip()
        searchStr = searchStr + '&well_type='+str(mWellType)
    if mPCRPlate != None and mPCRPlate != '':
        mPCRPlate = mPCRPlate.strip()
        searchStr = searchStr + '&pcr_plate='+str(mPCRPlate)
    if mSortWindow != None and mSortWindow != '':
        mSortWindow = mSortWindow.strip()
        searchStr = searchStr + '&sort_window='+str(mSortWindow)
    if mSeqNames != None and mSeqNames != '':
        mSeqNames = mSeqNames.strip()
        searchStr = searchStr + '&seq_names='+str(mSeqNames)
        
    if(mFormat == "csv"):
        downloadCSV(searchStr, logger)
    elif(mFormat == "tab"):
        pass
    else:
        logger.error("Unknown format requested, Format: "+mFormat)
        
if __name__ == '__main__':
    main()
