import math 

class NeedlemanWunsch:

#
#seq1 is any other sequence for comparison.
#seq2 is the search string.
#
	def __init__(self, seq1, seq2):
		self.seq1 = seq1
		self.seq2 = seq2
		self.A = ""
		self.B = ""
		self.diffs = []
		self.gap = -5;
		self.scores = {'x': 0, 'y': 0, 'pwd': 0.0}

	def align(self):
		# Var names from Wikipedia pseudocode
		d = self.gap # Gap penalty
		#print "d " + str(d)
		A = self.seq1 # First sequence to be compared
		B = self.seq2 # Second ""
		I = range(len(A)) # To help iterate (Pythonic)
		J = range(len(B)) # ""
		F = [[0 for i in B] for j in A] # Fill a 2D array with zeroes
		#print str(F)
		# Similarity matrix from Wikipedia:
		S = \
		{'A': {'A': 10, 'G': -1, 'C': -3, 'T': -4},
		 'G': {'A': -1, 'G':  7, 'C': -5, 'T': -3},
		 'C': {'A': -3, 'G': -5, 'C':  9, 'T':  0},
		 'T': {'A': -4, 'G': -3, 'C':  0, 'T':  8}}

		# Initialization
		for i in I:
			F[i][0] = d * i
		for j in J:
			F[0][j] = d * j
	
		#print str(F)
		#print str(I[1:])
		#print str(J[1:])

		# Scoring
		for i in I[1:]:
			#print str(i) + "\n"
			for j in J[1:]:
				#print "IJ " + str(i) + " " + str(j) + "\n\n"
				#print "F" + str(F[i-1][j-1]) + "\n\n"
				#print "AB" + str(A[i]) + " " + str(B[j]) + "\n\n"
				#print "S" + str(S[A[i]][B[j]]) + "\n\n"
				Match = F[i-1][j-1] + S[A[i]][B[j]]
				#print "MATCH"+str(Match) + "\n\n"
				Delete = F[i-1][j] + d
				#print "DELETE"+str(Delete) + "\n\n"
				Insert = F[i][j-1] + d
				#print "INSERT"+str(Insert) + "\n\n"
				F[i][j] = max(Match, Insert, Delete)
				#print "Fij"+str(F[i][j]) + "\n\n"

		# print "FFFFFFFFFFF" + str(F) + "\n\n"
		# Traceback
		AlignmentA = ""
		AlignmentB = ""
		#print "AlignmentA\n\n"
		i = len(A) - 1
		j = len(B) - 1
		#print "ij"+str(i) + " " + str(j) + "\n\n"


		while (i > 0 and j > 0):
			#print "ij" + str(i) + " " + str(j) + "\n\n"
			Score = F[i][j]
			ScoreDiag = F[i - 1][j - 1]
			ScoreUp = F[i][j - 1]
			ScoreLeft = F[i - 1][j]
			if (Score == ScoreDiag + S[A[i]][B[j]]):
				AlignmentA = A[i] + AlignmentA
				AlignmentB = B[j] + AlignmentB
				i -= 1
				j -= 1
			elif (Score == ScoreLeft + d):
				AlignmentA = A[i] + AlignmentA
				AlignmentB = "-" + AlignmentB
				i -= 1
			elif (Score == ScoreUp + d):
				AlignmentA = "-" + AlignmentA
				AlignmentB = B[j] + AlignmentB
				j -= 1
			else:
				print("algorithm error?")

		if j == 0: 
			while i >= 0:
				if j >= 0 and B[j] == A[i]:
					AlignmentA = A[i] + AlignmentA
					AlignmentB = B[j] + AlignmentB
					i -= 1
					j -= 1
				else:
					AlignmentA = A[i] + AlignmentA
					AlignmentB = "-" + AlignmentB
					i -= 1

		if i == 0:
			while j >= 0:
				if i >= 0 and B[j] == A[i]:
					AlignmentA = A[i] + AlignmentA
					AlignmentB = B[j] + AlignmentB
					i -= 1
					j -= 1
				else:
					AlignmentA = "-" + AlignmentA
					AlignmentB = B[j] + AlignmentB
					j -= 1

		while i >= 0:
			AlignmentA = A[i] + AlignmentA
			AlignmentB = "-" + AlignmentB
			i -= 1

		while j >= 0:
			AlignmentA = "-" + AlignmentA
			AlignmentB = B[j] + AlignmentB
			j -= 1
		
		self.A = AlignmentA
		self.B = AlignmentB

	# Similarity
	def homology(self):
		lenA = len(self.A)
		lenB = len(self.B)
		sim1 = ""
		sim2 = ""
		length = 0
		k = 0
		total = 0
		similarity = 0.0
		
		if (lenA > lenB):
			sim1 = self.A
			sim2 = self.B
			length = lenA
			k_start = 0
			k_end = lenB-1
		else:
			sim1 = self.B
			sim2 = self.A
			length = lenB
			k_start = 0
			k_end = lenA-1
		while (sim1[k_start] != sim2[k_start]) and k_start < k_end:
			k_start += 1
		while (sim1[k_end] != sim2[k_end]) and k_start < k_end:
			k_end -= 1
		comparison_sequence_len = k_end - k_start + 1
		while (k_start <= k_end):
			if (sim1[k_start] == sim2[k_start]):
				total += 1
			else:
				self.diffs.append(k)
				#total -= 1
			k_start += 1
		
		# original_sequence_minlen = min( len(self.seq1) , len(self.seq2) )
		# seqminlen = min(comparison_sequence_len , original_sequence_minlen)
		if comparison_sequence_len > 0:
			similarity = (float(total) / float(comparison_sequence_len)) * 100.0
		else:
			similarity = 0
		
		#return similarity
		return float(str("%.2f" % similarity)) ###+ "%  " + str(self.A) + " " + str(self.B)
		#return str("%.2f" % similarity) + " " + str(total) + " : " + str(self.A) + " <-> " + str(self.B)
        ###str("%.2f" % similarity) + "%"
	
	# Sets certain key values used in other MSA methods.
	def score(self):
		matches = 0
		mismatches = 0
		gapOpenings = 0
		gapExtensions = 0
		gapLast = False
		
		for pair in zip(self.A, self.B):
			if pair[0] == pair[1] and pair[0] != "-":
				matches += 2
				gapLast = False
			elif pair[0] != pair[1] and pair[0] != "-":
				mismatches -= 2
				gapLast = False
			elif (pair[0] == "-" or pair[1] == "-") and gapLast:
				gapExtensions -= 1
			elif (pair[0] == "-" or pair[1] == "-") and not gapLast:
				gapOpenings -= 2
				gapLast = True
			else:
				print("Something unaccounted for in scoring.")
				
		return sum([matches, mismatches, gapOpenings, gapExtensions])
		
