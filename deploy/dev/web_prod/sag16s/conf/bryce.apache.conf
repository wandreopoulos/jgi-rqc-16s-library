## Apache Configuration
## - configured for Bryce's environment, port 8087
## Start:
## /usr/sbin/apache2 -k start -f /global/homes/b/brycef/git/jgi-rqc-legacy/configuration/bryce.apache.conf
## Stop:
## /usr/sbin/apache2 -k stop -f /global/homes/b/brycef/git/jgi-rqc-legacy/configuration/bryce.apache.conf

#
# The accept serialization lock file MUST BE STORED ON A LOCAL DISK.
#
#LockFile /global/homes/b/brycef/apache/accept.lock

#
# PidFile: The file in which the server should record its process
# identification number when it starts.
# This needs to be set in /etc/apache2/envvars
#
#PidFile /global/homes/b/brycef/apache/logs/apache2.pid

#
# Timeout: The number of seconds before receives and sends time out.
#
Timeout 300

#
# KeepAlive: Whether or not to allow persistent connections (more than
# one request per connection). Set to "Off" to deactivate.
#
KeepAlive On

#
# MaxKeepAliveRequests: The maximum number of requests to allow
# during a persistent connection. Set to 0 to allow an unlimited amount.
# We recommend you leave this number high, for maximum performance.
#
MaxKeepAliveRequests 100

#
# KeepAliveTimeout: Number of seconds to wait for the next request from the
# same client on the same connection.
#
KeepAliveTimeout 15

##
## Server-Pool Size Regulation (MPM specific)
## 

# prefork MPM
# StartServers: number of server processes to start
# MinSpareServers: minimum number of server processes which are kept spare
# MaxSpareServers: maximum number of server processes which are kept spare
# MaxClients: maximum number of server processes allowed to start
# MaxRequestsPerChild: maximum number of requests a server process serves
<IfModule mpm_prefork_module>
    StartServers          5
    MinSpareServers       5
    MaxSpareServers      10
    MaxClients          150
    MaxRequestsPerChild   0
</IfModule>

# worker MPM
# StartServers: initial number of server processes to start
# MaxClients: maximum number of simultaneous client connections
# MinSpareThreads: minimum number of worker threads which are kept spare
# MaxSpareThreads: maximum number of worker threads which are kept spare
# ThreadLimit: ThreadsPerChild can be changed to this maximum value during a
#              graceful restart. ThreadLimit can only be changed by stopping
#              and starting Apache.
# ThreadsPerChild: constant number of worker threads in each server process
# MaxRequestsPerChild: maximum number of requests a server process serves
<IfModule mpm_worker_module>
    StartServers          2
    MinSpareThreads      25
    MaxSpareThreads      75 
    ThreadLimit          64
    ThreadsPerChild      25
    MaxClients          150
    MaxRequestsPerChild   0
</IfModule>

# event MPM
# StartServers: initial number of server processes to start
# MaxClients: maximum number of simultaneous client connections
# MinSpareThreads: minimum number of worker threads which are kept spare
# MaxSpareThreads: maximum number of worker threads which are kept spare
# ThreadsPerChild: constant number of worker threads in each server process
# MaxRequestsPerChild: maximum number of requests a server process serves
<IfModule mpm_event_module>
    StartServers          2
    MaxClients          150
    MinSpareThreads      25
    MaxSpareThreads      75 
    ThreadLimit          64
    ThreadsPerChild      25
    MaxRequestsPerChild   0
</IfModule>

# These need to be set in /etc/apache2/envvars
#User ${APACHE_RUN_USER}
#Group ${APACHE_RUN_GROUP}
User brycef 
Group brycef


#
# AccessFileName: The name of the file to look for in each directory
# for additional configuration directives.  See also the AllowOverride
# directive.
#

AccessFileName .htaccess

#
# The following lines prevent .htaccess and .htpasswd files from being 
# viewed by Web clients. 
#
<Files ~ "^\.ht">
    Order allow,deny
    Deny from all
    Satisfy all
</Files>

DefaultType text/plain
HostnameLookups Off
#ErrorLog /global/homes/b/brycef/apache/logs/error.log
LogLevel warn

# Include module configuration:
# Include mods-enabled/*.load
# Include mods-enabled/*.conf

#
# The following directives define some format nicknames for use with
# a CustomLog directive (see below).
# If you are behind a reverse proxy, you might want to change %h into %{X-Forwarded-For}i
#
#LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
#LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
#LogFormat "%h %l %u %t \"%r\" %>s %O" common
#LogFormat "%{Referer}i -> %U" referer
#LogFormat "%{User-agent}i" agent

# Include the virtual host configurations:
# Include sites-enabled/

# Bryce's dev on rqc-dev
Listen 9091
<VirtualHost *:8000>

    DocumentRoot /global/homes/q/qc_user/sag16s/sag16s/web/sag16s/htdocs
    <Directory />
        Options FollowSymLinks
        AllowOverride None
    </Directory>
    
    ScriptAlias /cgi-bin/ /global/homes/q/qc_user/sag16s/sag16s/web/sag16s/cgi-bin/
    <Directory "/global/homes/q/qc_user/sag16s/sag16s/web/sag16s/cgi-bin">
        AllowOverride None
        Options ExecCGI -MultiViews +SymLinksIfOwnerMatch

        Order Deny,Allow
        Deny from all
        # Allow the first 3 octets of the ip address: a.b.c.* 
        Allow from 128.3.44 128.3.89 128.3.90 128.3.91 128.55.54 128.55.69 128.55.71 131.243.223 198.128.40 198.128.41 198.128.44 198.129.91 198.129.96 
    </Directory>
</VirtualHost>

# setting test environment up

###
### HACK: THIS NEEDS TO BE UPDATED TO REFLECT THE NEW RQC WEB SERVER
### CONFIGURATION, AND SYNCHRONIOZED WITH QC_INSTALL.PL.
###
SetEnv  RQC_SW_TOP_LEVEL_DIR  /global/homes/b/brycef/git/jgi-rqc-legacy
SetEnv  RQC_TOP_LEVEL_DIR  /global/homes/b/brycef/git/jgi-rqc-legacy
SetEnv  PERL5LIB  /global/homes/b/brycef/git/jgi-rqc-legacy/lib
SetEnv  RQC_INTEGRATION_MODE  0
SetEnv  RQC_PRODUCTION_MODE  0
SetEnv  RQC_LOG_LEVEL  DEBUG
SetEnv  RQC_DATABASE_PREFIX brycef
SetEnv  PYTHONPATH /global/homes/b/brycef/git/jgi-rqc-legacy/Python
PassEnv PATH
PassEnv LD_LIBRARY_PATH
