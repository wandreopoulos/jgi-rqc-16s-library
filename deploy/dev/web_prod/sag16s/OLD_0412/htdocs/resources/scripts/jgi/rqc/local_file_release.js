// js functions for "local file" release 
// Shijie Yao
// Jan, 10, 2012

//Global data
var debug 		= false;
var COLOR_RED 	= "red";
var toReleaseFiles;		// [] to store <input text> DOM obj for each file
var	toReleaseFileTypes; // [] to store <td> DOM obj for each file type;
var listDeliminator = ":";

/* btnID    : the submit button that need to be in sync with the readiness of data.
 * voidTag  : the str in <select> options that denote an invalid selection.
 * param	: the associative array containing all the current values that need to 
 *            be checked against voidTag.
 */
function validate_all(btnID, param)
{
	var btn = document.getElementById(btnID);

	var flag = true; 
	var voidTag = param.voidTag;
	for(var name in param){
		if(name == "fileListID" && document.getElementById(param[name]) == null){
			return false;
		} 

		if(name != "voidTag") {
			var value = param[name];
			if( name != "seqProjID" && name != "projDir" && name != "pmoID" ){
				// any field other than seqProjID and projDir and pmoID must be non-null
				if( value == voidTag || !value ){
					flag = false;	
					break;
				}
			}
		}
	}

	if(!param['seqProjID'] && ( !param['projDir'] ||  !param['pmoID'] ) ) {
		// seqProjID has value is OK, if not BOTH projDir and pmoID has to have value
		flag = false;	
	}

	btn.disabled = !(flag && check_input_stat());

	//FF debug
}

/** 
 * Based on the selection value of selID (<SELECT>),
 * swap the display of the two table elements (for single-cell and isolate).
 * param = {tableID:index, }
 */
function data_type_selected(selID, param)
{
	var sel = document.getElementById(selID);
	var index = sel.selectedIndex;
	wait_cursor(true);
	for (var key in param){
		if(param[key] == index){
			var destID = param["trId"];

			jQuery.ajax({
					url  : '/cgi-bin/single_cell_isolate_new_release_upload.cgi',
					type : 'post',
					data : {action: 'fileUI', ui_index: param[key]},
					async: false,
					success : function(data){
							jQuery('#' + destID).html(data)
					},
					error : function(jqXHR, exeption) { 
						alert('Error:' + jqXHR.responseText); 
					}
					
			});
		}
	}
	wait_cursor(false);
}

function hide_all_tables(param)
{
	for(var index in param){
		var ftab = document.getElementById(index);
		ftab.style.display = "none";
	}
}

/*
 * if all the <Input type=file > element is populated with strings
 * and no string is set in warning color, return true; else return false. 
 */
function check_input_stat()
{
	toReleaseFiles = [];
	toReleaseFileTypes = [];
	
	var inputs = document.getElementsByTagName('input');
	
	//initialize the files array
	for(var i=0; i<inputs.length; i++)
	{
		if( inputs[i].id.indexOf("release_") == 0 )
		{
			toReleaseFiles.push(inputs[i]);
		}
	}

	inputs = document.getElementsByTagName('td');
	
	//initialize the files array
	for(var i=0; i<inputs.length; i++)
	{
		if( inputs[i].id.indexOf("type_release_") == 0 ) {
			toReleaseFileTypes.push(inputs[i]);
		}
	}

	var rtn = true;
	for(var i=0; i<toReleaseFiles.length; i++)
	{
		var oFile = toReleaseFiles[i];
		var type = toReleaseFileTypes[i].innerHTML;
		if(type.indexOf('*') == 0 && (oFile.value == "" || oFile.style.color != ""))		
		{
			rtn = false; //not ready
			break;
		} else if(oFile.style.color != ""){
			delete toReleaseFiles[i];
		}
	}

	//FF debug
	if(debug) console.log("check_input_stat:" + rtn);

	return rtn; //all inputs are filled with text
}

// To test the existance of the file provided in the text Obj
// the last 3 params for validate_all() 
function validate_file(textID, btnID, param)
{
	var textObj = document.getElementById(textID);
	var fileName = textObj.value;
	var divID = textObj.name + "_size";
	var url = "/cgi-bin/rqc_check_file_size.cgi";

	if(fileName == ""){
		jQuery('#' + divID).html('');
		jQuery('#' + textObj.name + "_exam").html('');
		return;
	}
	
	jQuery.ajax({
		url  : '/cgi-bin/rqc_check_file_size.cgi',
		type : 'post',
		data : {file: fileName},
		success: function(data) {
			jQuery('#' + divID).html(data)
			var color = "";
			if(data == "")
				color = COLOR_RED;

			set_text_color(textID, color);					

            divID = textObj.name + "_exam";
            link = "";
            if(color == "") //validation OK, build link
			{
				var cgi = "display_txt_file.cgi";
				if(fileName.indexOf("pdf") != -1)
					cgi = "display_pdf_file.cgi";
                link = "<a target=\"_blank\" href=\"" + cgi + "?file=" + fileName + "\">view</a>";

			}
			jQuery('#' + divID).html(link)

			//FF debug
			if(debug) console.log("validate_file: color=" + color);
			validate_all(btnID, param);
		} 
	});
}

/* For the <text> field, set text
 */
function set_text_color(textID, color)
{
	var textObj = document.getElementById(textID);
	textObj.style.color = color;	
}

/*
 * param : the js associative array for all the params needed for manual release 
 */
function release_local_file(param)
{
	wait_cursor(true); //from main display_report_jigsaw.cgi to block page

	// toReleaseFiles must have been filled by now!
	// construct a string of "name=file;name=file..."
	// where name is the <text> tag name, file is the tag value
	var fileListStr = "";
	for(var i=0; i<toReleaseFiles.length; i++)
	{
		if(!toReleaseFiles[i]) continue;

		var key = toReleaseFiles[i].id;
		var file = toReleaseFiles[i].value;
		if(file == "") continue;

		if(fileListStr != "")
			fileListStr += listDeliminator;

		fileListStr += key + "=" + file; 
	}

	param.action = 'submit';
	param.fileList = fileListStr;
	jQuery('#subBtn').attr('disabled', true)
	var guid = GUID();

	jQuery.ajax({
		url  : '/cgi-bin/single_cell_isolate_new_release_upload.cgi',
		type : 'post',
		data : param,
		async: true,
		success : function(data){
						alert( 'Releasing your assembly data completed:\n' +
								guid); 
						updateReleaseList(null, true);
						wait_cursor(false); 
						jQuery('#subBtn').attr('disabled', false)
					},
		error : function(x, t, m) { //(xmlhttprequest, textstatus, message)
			if(t==="timeout")
				alert('browser timed out, but your upload will still be ok')
			else
				alert(t + ":" + m);
			wait_cursor(false); 
		},	
	});

	var msg = 'Releasing your assembly data.'
		msg += '\n' + 'Please do not move away from this page'
		msg += '\n' + 'Another alert will be presented once the release process is completed, with the following GUID:'
		msg += '\n' + guid 
	alert(msg)
} 
