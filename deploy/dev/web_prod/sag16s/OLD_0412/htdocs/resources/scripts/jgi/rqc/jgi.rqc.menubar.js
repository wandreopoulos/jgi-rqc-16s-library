/*
 * A jQuery plugin javascript library for menu bar.
 * Requirement: jQuery, jgi.rqc.html_util.js
 * 
 * configuration 
 * { text  : TEXT (eg 'Info'),
 *   text_id: TAG_ID (eg 'info_id'),
 *   mate  : TAG_ID (eg 'info_div_id'), 
 *   href  : HLINK (eg 'www.google.com'),
 *   targe : HLINK TARGE (eg '_blank'),
 *   action: JAVASCRIPT_FUNCTION (eg my_test, somewhere: function my_test(){})
 *   bar   : BOOLEAN (true OR false, def false),
 *   arrow : BOOLEAN (true OR false, def false),
 *   arrow_id : TAG_ID (eg 'info_arrow_id'),
 * }
 *   
 */
 
 /* 
  * An anonymous self-invoking function that creates a namespace
  */
 (function (base){
 
	'use restrict'
	var larrow = '&#8249',
		rarrow = '&#8250',
 		lcolor = 'red',
 		rcolor = 'blue';
	
	var menu_class = 'jgi-rqc-menu'
	var menu_tag = menu_class 
	
	var $ = base
	
	var mate = []	//for those that slide-toggle HTML divs
	var hlink = []	//for those that having connect to a hyper link 

	var td_spacer = html_tag('td', '', '&nbsp;')

	//extend jQuery's function list to have jQuery(SELECT).menubar(var)
	//where *this* is the jQuery selection object, and *var* is pass-in param 
	base.fn.menubar = function( obj ) {
		//console.dir(obj)
		var trd ='', td, tdd;
		
		if(obj.left){
			tdd = td_side(obj.left, menu_tag, 'left')
		}
		tdd += html_tag('td', html_tag_attr('width', '100%'), '')
		if(obj.right){
			tdd += td_side(obj.right, menu_tag, 'right')
		}

		//add leading and trailing td for spacing
		tdd = td_spacer + tdd + td_spacer 
		
		var tr = html_tag('tr', '', tdd)
		
		this.html(tr)
		
		//attach event handler
		attach_event_handler(obj.left)
		attach_event_handler(obj.right)
	};

	//to flip the given arrow
	base.fn.flip_arrow = function(aid, side) {
		var ar, col;
		if(side == 'right') {
			ar = rarrow
			col = rcolor
		} else if(side == 'left') {
			ar = larrow
			col = lcolor
		}

		if(ar && col){
			$('#' + aid).attr('style', 'color:' + col)
			$('#' + aid).html(ar)
		}
	};

	function attach_event_handler(obj){
		if(!obj)
			return;

		for(var idx in obj){
			var ele = obj[idx]

			//if mate exits, default behavior is to slide-toggle the ui
			if(ele.mate && ele.span_id){
				var sid = ele.span_id; 
				$(document).on('click', '#'+sid, function(){
					var mate_id = $(this).attr('mate')
					aid = $(this).attr('aid')
					$('#' + mate_id).slideToggle(200, function(){
						if(aid) {
							var ar = rarrow,
								col = rcolor;

							if($('#' + mate_id).is(':hidden')){
								ar = larrow
								col = lcolor
							} 
							$('#' + aid).attr('style', 'color:' + col)
							$('#' + aid).html(ar)
						}
					})
				});
			} else {
				// no mate but have href
				if(ele.href && ele.span_id) {
					var sid = ele.span_id
					$(document).on('click', '#'+sid, function(){
						var target = $(this).attr('target');
						if(target)
							window.open($(this).attr('href'), target) 
						else
							window.location = $(this).attr('href')
					})
				} else if(ele.span_id && ele.action ){
					$('#'+ele.span_id).click(ele.action);
				}


				if(ele.arrow && ele.span_id && ele.arrow_id){
					var sid = ele.span_id 
					$(document).on('click', '#'+sid, function(){
						var aid = $(this).attr('aid') 
						var ar = rarrow,
							col = rcolor;
						if($('#' + aid).attr('style').indexOf(rcolor) > -1){
							ar = larrow
							col = lcolor
						}
								
						$('#' + aid).attr('style', 'color:' + col)
						$('#' + aid).html(ar)
					});
				}
			}
		}
	}
 
	function html_arrow(id, cls, ar){
		col = 'color:' + rcolor
		if(ar == larrow)
			col = 'color:' + lcolor

		return html_tag('text', 
						html_tag_attr('id', id) +
						html_tag_attr('class', cls) +
						html_tag_attr('pair', 'test') +
						html_tag_attr('style', col),
						ar)
	}
	
	function td_side(obj, menu_tag, side_tag){
		var td='', 
			tdd, 
			attr, 
			span_id,
			arrow_id, 
			arrow_class;

		var td_id_tag = menu_tag + '-' + side_tag + '-';
		
		//each <td>
		for(var idx in obj){
			var ele = obj[idx]

			span_id = td_id_tag + 'span-' + idx
			arrow_class = td_id_tag + 'arrow'
			arrow_id = arrow_class + '-' + idx

			if(ele.text_id)
				span_id = ele.text_id
			if(ele.arrow_id)
				arrow_id = ele.arrow_id

			ele.span_id = span_id
			ele.arrow_id = arrow_id
				
			attr = html_tag_attr('id', span_id) + 
					html_tag_attr('class', menu_class) +
					html_tag_attr('style', 'cursor:pointer; white-space:nowrap');

			//mate DOM element Id (the menu controls)
			if(ele.mate) {
				mate.push(span_id);
				attr += html_tag_attr('mate', ele.mate);
			} else if(ele.href) {
				hlink.push(span_id);
				attr += html_tag_attr('href', ele.href);
				if(ele.target)
					attr += html_tag_attr('target', ele.target);
			}

			if(ele.arrow)
				attr += html_tag_attr('aid', arrow_id);

			if(ele.title)
				attr += html_tag_attr('title', ele.title);
			else
				attr += html_tag_attr('title', ele.text);
				
			if(ele.text)
				tdd = html_tag('span', attr, ele.text)
			else
				tdd = html_tag('span', attr, 'Tool')
			
			if(ele.arrow)
				tdd += html_arrow(arrow_id, arrow_class, rarrow)
			 				
			if(ele.bar)
				tdd += '&nbsp|'
			 
			td += html_tag('td', html_tag_attr('id',  td_id_tag + idx), tdd)
		}
		return td_spacer + td + td_spacer;
	}
 })(jQuery);
 

