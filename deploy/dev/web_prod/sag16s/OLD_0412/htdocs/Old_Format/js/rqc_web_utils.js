/*
 * debug to display an associative array
 */
function show_for_js_aa(param)
{
    var txt = "";
    for(var i in param) {
        if(txt != "")
            txt += " "
        txt += i + '=' +  param[i];
    }
    alert(txt);
}

/*
 * trim leading and trailing white spaces in the given str.
 */
function trim(str)
{
	return str.replace(/^\s+|\s+$/g, "")
}

function chomp(str)
{
	return str.replace(/(\n|\r)+$/, '')
}

// convert string in '{a:val1,b:val2}', or 'a:val1,b:val2' into js obj
function str_to_obj(jstr)
{
    var param = {}
    jstr = jstr.trim();
    if(jstr.indexOf('{')==0)
        jstr = jstr.substr(1)
    if(jstr.indexOf('}') == jstr.length -1)
        jstr = jstr.substr(0, jstr.length -1)

    var list = jstr.split(/\s*,\s*/);
    list.forEach(function(pair){
        var tup = pair.split(/\s*:\s*/)
        param[tup[0]] = tup[1]
    });
    return param
}

// To show the busy cursor and a shade panle cover the entire 
// window to block user action while waiting
// toWait : boolean 
// divID  : a top level div that will be resized and show/hide by this function 
function wait_cursor(toWait, divID)
{
    var type = "wait";
    if(!toWait)
        type = "";
    document.body.style.cursor = type;

    //glass_panel(toWait);
    var gpan = document.getElementById(divID);
	if(gpan){
    	if(toWait) {
        	//match glass panel to window.screen
        	gpan.style.top      = 0;
        	gpan.style.left     = 0;
        	gpan.style.width    = window.screen.width;
        	gpan.style.height   = window.screen.height;

        	gpan.style.visibility="";
    	} else { 
			gpan.style.visibility="hidden"; 
		}
	}
}

