/*
 * javascript library for jgi.rqc ui banner.
 * pre-requirement: jQuery and jgi.rqc.html_util.js
 */
 
 /* 
  * An anonymous self-invoking function that creates a namespace
  */
(function (window){
 
	'use restrict'
	
	var $ = base
	
	var td_spacer = html_tag('td', '', '&nbsp')

	//helper to create the minor title html string
	function minor_title(ar){
		var row = ''
		for(var idx in ar){
			var pair = ar[idx];
			if(row) 
				row += '&nbsp;&nbsp;'
			row += html_tag('span', html_tag_attr('class', 'subtitle key'), ' ' + pair[0] + ' ') + ':' +
					html_tag('span', html_tag_attr('class', 'subtitle value'), ' ' + pair[1] + ' ')	
		}
		return row
	}
 })(jQuery);
 

