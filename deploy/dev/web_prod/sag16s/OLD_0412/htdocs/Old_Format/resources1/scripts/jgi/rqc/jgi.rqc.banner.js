/*
 * A jQuery plugin javascript library for jgi.rqc ui banner.
 * 
 * Author: Shijie Yao
 * Last Updated: Dec. 6, 2012
 * 
 * Require: jQuery, jgi.rqc.html_util.js
 *
 * configuration (all optional):
 * { image   : IMG (eg '/img/jgi_logo.jpg'),
 *   image_h : IMG_HEIGHT (eg '100px'),
 *   magee_w : IMG_WIDTH (eg '100px'),
 *   href    : HREF (eg 'www.google.com'),
 *   background_color : COLOR (eg '#81BEF7') ,
 * 
 *   //when minor title is in [[key:value],[key:value],...] form
 *   //the font can be optionaly configured:
 *   key_family: FONT-FAMITY (eg 'Courier New,Monospace') ,
 *   key_weight: FONT-WEIGHT (eg 900) ,
 *   key_size: FONT-SIZE (eg '100%') ,
 *   key_color: FONT-COLOR (eg 'grey') ,
 *   val_family: FONT-FAMITY (eg 'Courier New,Monospace') ,
 *   val_weight: FONT-WEIGHT (eg 900) ,
 *   val_size: FONT-SIZE (eg '100%') ,
 *   val_color: FONT-COLOR (eg 'grey') ,
 * }
 *
 */
 
 /* 
  * An anonymous self-invoking function that creates a namespace
  */
(function ($){
 
	'use restrict'
	
	//default title font style
	var subtitle_key_css_font_family = 'Courier New,Monospace',
		subtitle_key_css_font_weight = 900,
		subtitle_key_css_font_size = '100%',
		subtitle_key_css_font_color = 'yellow';
	var subtitle_val_css_font_family = 'Courier New,Monospace',
		subtitle_val_css_font_weight = 900,
		subtitle_val_css_font_size = '100%',
		subtitle_val_css_font_color = 'grey';

	//default banner image, image size, hyper link, and title strings
	var href = 'http://www.jgi.doe.gov',
		background_color = '#81BEF7',
		img_s = '/img/jgi_logo.jpg',
		img_h = '86px',
		img_w = '140px',
		m_title = 'JGI RQC Banner Default Major Title',
		n_title = 'jgi rqc banner default minor title',
		i_title = 'jgi rqc banner default info';

	// html ids
	var m_title_id = 'jgi_rqc_banner_title_major',
		n_title_id = 'jgi_rqc_banner_title_minor',
		i_title_id = 'jgi_rqc_banner_title_info';
	
	var east_td = 'banner_east'
	var west_id = 'banner_west'

	
	//extend jQuery's function list to have jQuery(SELECT).banner(var)
	//where *this* is the jQuery selection object, and *var* is pass-in param 
	$.fn.banner = function(obj){
		//console.dir(obj)
		var tr, tdd;

		if(obj){
			if(obj.background_color)
				background_color = obj.background_color 
			
			if(obj.image)
				img_s = obj.image

			if(obj.image_h)
				img_h = obj.image_h

			if(obj.image_w)
				img_w = obj.image_w

			if(obj.href)
				href = obj.href

			if(obj.title_major)
				m_title = obj.title_major	

			if(obj.title_minor)
				n_title = obj.title_minor

			if(obj.title_info)
				i_title = obj.title_info
		}
		
		// the WEST 2-row logo and link
		var img = html_tag('img', html_tag_attr('src', img_s) +
						html_tag_attr('style', 'height:' + img_h + ';width:' + img_w), '');
		var link = html_tag('a', html_tag_attr('href', href), img)
		var tdd = html_tag('td', html_tag_attr('id', west_id) + 
								html_tag_attr('rowspan', 3) + html_tag_attr('align', 'left'), link)

		// major title
		tdd += html_tag('td', 	html_tag_attr('width', '100%') + 
								html_tag_attr('align', 'center'), 
								html_tag('h3', html_tag_attr('id', m_title_id), m_title));
		
		// the EAST 2-row spacer (matching WEST logo)
		tdd += html_tag('td',	html_tag_attr('id', east_td) +
								html_tag_attr('rowspan', 3) + html_tag_attr('align', 'right') +
								html_tag_attr('style', 'height:' + img_h + ';width:' + img_w), '')

		tr = html_tag('tr', '', tdd)

		// minor title
		if( typeof(n_title) == 'string'){
			tdd = html_tag('td', 	html_tag_attr('width', '100%') +
								html_tag_attr('align', 'center'),
								html_tag('span', html_tag_attr('id', n_title_id), n_title));
		} else {
			tdd = html_tag('td', 	html_tag_attr('width', '100%') +
								html_tag_attr('align', 'center'),
								html_tag('span', html_tag_attr('id', n_title_id), minor_title({data:n_title})));
		}
		
		tr += html_tag('tr', '', tdd)

		// minor title
		tdd = html_tag('td', html_tag_attr('align', 'center'), 
							html_tag('span', html_tag_attr('id', i_title_id), i_title));
		
		tr += html_tag('tr', '', tdd)

		//console.log(tdd)
		this.html(tr)

		var style = '', s = this.attr('style')
        if(s)
            style = s + ';background:' + background_color
		else 
			style = 'background:' + background_color 
		this.attr('style', style)
	}

	
	/* For setting titles:
	 * $("#banner").set_title({type:'major', data:'This Is A New Major Title'})
	 * $("#banner").set_title({type:'minor', data:'this is a new minor title'}) or
	 * $("#banner").set_title({type:'minor', data:[['lib', 'ABCD'], ['genus', 'Escherichia']]})
	 * $("#banner").set_title({type:'info', data:'this is a info string'})
	 */
	$.fn.banner_title = function(obj){
		if(obj && obj.type){
			if(obj.type == 'major'){
				//console.log($('#'+m_title_id).html())
				$('#'+m_title_id).html(obj.data)
			} else if(obj.type == 'minor'){
				if(typeof(obj.data) == 'string'){
					$('#'+n_title_id).html(obj.data)
				} else {
					$('#'+n_title_id).html(minor_title(obj))
				}
			} else if(obj.type == 'info'){
				$('#'+i_title_id).html(obj.data)
			}
		}
	}

	//helper to create the minor title html string
	function minor_title(obj){
		var ar = obj.data	
		var row = ''
		for (var idx = 0; idx < ar.length; idx++){
			var pair = ar[idx];
			if(row) 
				row += '&nbsp;&nbsp;'

			if(obj.key_family)
				subtitle_key_css_font_family = obj.key_family
			if(obj.key_weight)
				subtitle_key_css_font_weight = obj.key_weight
			if(obj.key_size)
				subtitle_key_css_font_size = obj.key_size
			if(obj.key_color)
				subtitle_key_css_font_color = obj.key_color	

			if(obj.val_family)
				subtitle_val_css_font_family = obj.val_family
			if(obj.val_weight)
				subtitle_val_css_font_weight = obj.val_weight
			if(obj.val_size)
				subtitle_val_css_font_size = obj.val_size
			if(obj.val_color)
				subtitle_val_css_font_color = obj.val_color	

			var key_style = html_tag_attr('style', 
											'font-family:' + subtitle_key_css_font_family + ';' +
											'font-weight:' + subtitle_key_css_font_weight + ';' +
											'font-size:' + subtitle_key_css_font_size + ';' +
											'color:' + subtitle_key_css_font_color + ';'
                                         );
			var val_style = html_tag_attr('style', 
											'font-family:' + subtitle_val_css_font_family + ';' +
											'font-weight:' + subtitle_val_css_font_weight + ';' +
											'font-size:' + subtitle_val_css_font_size + ';' +
											'color:' + subtitle_val_css_font_color + ';'
                                         );

			row += html_tag('span', html_tag_attr('class', 'subtitle key') + key_style, ' ' + pair[0] + ' ') + ':' +
					html_tag('span', html_tag_attr('class', 'subtitle value') + val_style, ' ' + pair[1] + ' ')	
		}
		return row
	}

	$.fn.set_html_to_banner = function(place, html){
		var id;
		if(place == 'east')
			id = east_td
		else if(place == 'west')
			id = east_td
		else if(place == 'title-major')
			id = m_title_id
		else if(place == 'title-minor')
			id = n_title_id
		else if(place == 'title-info')
			id = i_title_id

		if(id && html)
			$('#' + id).html(html)
	}
 })(jQuery);
 

