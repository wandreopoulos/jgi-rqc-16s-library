
var gSelectedNewArray = [];
var gSelectedDupArray = [];
var gRegisterRetData = null;

function populateCheckedDuplicates(){
    var index = 0;
    gSelectedDupArray = [];
    $(":checkbox").each(function(){
        var val = $(this).attr('value');
        if( val != "CheckAll"){
            var isChecked = $(this).is(':checked');
            if(isChecked){
                gSelectedDupArray[index] = val;
                index = index+1;
            }
        }
    });
}


function sag16sSubmit(){
    var toBeRegisteredFilesAry = [];
    var index = 0;
    var dataStr = "";
    /*
     * As there could be a change, lets freshly populate what has been selected
     * in duplicates 
     */
    populateCheckedDuplicates();
    
    for(var i=0; i<gSelectedNewArray.length; i++){
        toBeRegisteredFilesAry[index] = gSelectedNewArray[i];
        dataStr = dataStr+"fileName="+gSelectedNewArray[i]+"&";
        index = index+1;
    }
    for(var i=0; i<gSelectedDupArray.length; i++){
        toBeRegisteredFilesAry[index] = gSelectedDupArray[i];
        dataStr = dataStr+"fileName="+gSelectedDupArray[i]+"&";
        index = index+1;
    }
    dataStr = dataStr+"stagingDir="+$('#stagingDir').val();
    dataStr = dataStr+"&userName="+$('#userName').val();
    dataStr = dataStr+"&runType="+$('#runType').val();
    dataStr = dataStr+"&pcrPlateId="+$('#pcrPlateId').val();
    //alert(dataStr);
    var retConfirm = confirm("Registering the data into RQC Sag16s System. Are you sure?");
    
    if(!retConfirm){
    	return true;
    }
    //alert(dataStr);
    // Now perform the AJAX call to register the data
    $.ajax({
        type: "POST",
        url: "/cgi-bin/sag16s_register.py", 
        data: dataStr,
        success: function(msg){
            registerPostProcessing(msg);
        },
        error: function(jxHR, textStatus, errorThrown){
              alert("Error :"+testStatus+"\nMsg: "+errorThrown);  
        }
    });
    
    $('#uploadDiv').html('<img align="center" src="/img/loading45.gif" alt="Loading Registration Status">');
};


function registerPostProcessing(result){
    //alert(result);
    $("#uploadDiv").html(result)
    return true;
}


function checkAllEvent(){
    var checkedVal = $("#CheckAll").is(':checked');
    $(":checkbox").each(function(){
        $(this).attr("checked", checkedVal);
    });
};


$(document).ready(function() {
    var newStr = 'New Sequences to Upload\n';
    var dupStr = 'Duplicate Sequences to Upload\n';
    var index = 0;
    
    $("#banner").banner(); 
    $().banner_title({
        type:'major',
        data:'RollingQC SAG16s UI'
    });
    $().banner_title({
        type:'minor',
        data:'RQC Sag16s Validation Status',
    });
    $().banner_title({type:'info', data: ''});
    
    
    $('#CheckAll').wrap("<span style='background-color:red' />");
    
    $('#CheckAll').click(function(){
        checkAllEvent();
    });
    
    
    $("#submitSag16s").click(function(){
        var val = $("#cantUpload").val();
        if(val != null){
            alert("Can't submit, as one of more 16s Sequence files are \neither of unknown format or no associated phd files.");
            return true;
        }
        sag16sSubmit();
    });

    
    $("input").each(function(){
        var type = $(this).attr('type');
        var val = $(this).attr('value');
        var tname = $(this).attr('name');
        if(type == 'hidden' && tname != 'stagingDir' && tname != 'userName' && tname != 'runType' && tname != 'pcrPlateId'){
            gSelectedNewArray[index] = val;
            index = index+1;
        }
    });
    
    $("#cancelUpload").click(function(){
        document.location.href = "/sag16s.html"
    });
    
    $("#cantUpload").each(function(){
         alert("You will not be allows to upload the 16s Sequences as one of more 16s Sequence files are \neither of unknown format or no associated phd files.");
        //$("#submitSag16s").attr("disabled", false);
        //document.getElementById("submitSag16s").disabled=true
    });
    
});

