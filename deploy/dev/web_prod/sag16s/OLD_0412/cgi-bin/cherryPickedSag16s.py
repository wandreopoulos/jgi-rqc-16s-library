#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

'''
Created on Dec 11, 2012

@author: asyed
'''


import cgi
import json
import MySQLdb
import sys, time

from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb


class PrintHTML(object):
    FORMAT_JSON = 'json'
    FORMAT_HTML = 'html'
    
    __format = None
    __header_printed = False
    
    def __init__(self, hformat):
        self.__format = hformat
    
    def printUsage(self):
        print "Usage:"
        print "/cgi-bin/markSequenceSelected.py?markSequence=FILE_NAME_PREFIX&format=[html|json][&markSequence=FILE_NAME_PREFIX]"
        print "   FILE_NAME_PREFIX is the SAG16S file name without the .ab1/.seq/.phd.1 extension"
        print "   You may specify multiple file name prefixes. Format should be either 'json' or 'html'."
        print "Try again. Good luck!"
    
    def printHeader(self):
        if (self.__format == self.FORMAT_JSON):
            print 'Content-type: application/json\n'    
        else:
            print 'Content-type: text/html\n'
        self.__header_printed = True
    
    def printError(self, message):
        if not self.__header_printed:
            self.printHeader()
        print message
    
    def printResults(self, resultMap):
        if not self.__header_printed:
            self.printHeader()
        if self.__format == self.FORMAT_JSON :
            self.__printJSON(resultMap)
        elif self.__format == self.FORMAT_HTML :
            self.__printHTML(resultMap)
        else :
            # should never be here
            pass
    
    def __printJSON(self, result):
        print json.dumps(result)
    
    def __printHTML(self, resultMap):
        keysList = resultMap.keys()
        print '<h2><center>RQC Sag16s Mark Sequence Selected Status</center></h2>'
        print '   <table name="markResultId" id="markResultId" border="2" cellpadding="2">'
        for key in keysList:
            value = resultMap.get(key)
            dispVal = ""
            if value == 1:
                print '    <tr  bgcolor="green">'
                dispVal = "Successfully Marked"
            elif value == 0:
                print '    <tr  bgcolor="orange">'
                dispVal = "Duplicate Request or No Existing Seq"
            else:
                print '    <tr  bgcolor="red">'
                dispVal = "Failed To Dequeue"
            print '     <td>'+key+'</td>'
            print '     <td>'+dispVal+'</td>'
            print '    </tr>'
        print '   </table>'



def updateChecked(prefix, logger):
    rowsAffected = 0
    ltime = time.localtime()
    timestamp=str(ltime.tm_year)+'-'+str(ltime.tm_mon)+'-'+str(ltime.tm_mday)+" "+str(ltime.tm_hour)+':'+str(ltime.tm_min)+':'+str(ltime.tm_sec)
    updatesql = ("UPDATE sag16s_seq SET seq_name_marked=1, dt_data_modified='"+timestamp+"' WHERE seq_name='"+prefix+"'")
    logger.debug(updatesql)
    try:
        dbObj = RQCdb()
        dbObj.connect()
        dbObj.execute(updatesql)
        rowsAffected = dbObj.get_rows_affected()
        dbObj.disconnect()
        logger.info("Dequeue operation successfully performed")
    except:
        logger.warn("An exception arose while picking the cherry")
        htmlObj = PrintHTML()
        htmlObj.printError("Status: 500 Internal Server Error\n\n")
        sys.exit(1)
    return(rowsAffected)


def main():
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("cherrypickSAG16s")
    #
    # Get list of all key-value pairs that were specified in the URL
    form = cgi.FieldStorage()
    mPrefixList = form.getlist('markSequence')
    mFormat = form.getvalue('format')
    # If format not specified, default is HTML
    if mFormat != None or mFormat != '': mFormat = mFormat.lower()
    if mFormat == None or mFormat == '': mFormat = PrintHTML.FORMAT_HTML
    logger.debug("BEFORE Format :"+str(mFormat)+" PrefixList: "+str(mPrefixList))
    #
    # Sanity check
    if not isinstance(mPrefixList, list):
        mPrefixList = [mPrefixList]
    if not isinstance(mFormat, str):
        mFormat = str(mFormat)
    #
    # standardizing the format case
    mFormat = mFormat.lower()
    mFormat.rstrip()
    logger.debug("Format :"+str(mFormat)+" PrefixList: "+str(mPrefixList));
    if mFormat != PrintHTML.FORMAT_HTML and mFormat != PrintHTML.FORMAT_JSON :
        logger.debug("Issue with FORMAT, format: "+mFormat)
        errorStr = "ERROR Format: "+mFormat+" is not recognized. "+" Acceptable: 'json' or 'html'"
        htmlObj = PrintHTML(mFormat)
        htmlObj.printError(errorStr)
        sys.exit(1)

    if (mPrefixList == None or mPrefixList == ''):
        logger.debug("Empty prefix list")
        htmlObj = PrintHTML(mFormat)
        errorStr = "Status: 400 Insufficient Input Error\n\n"
        htmlObj.printError(errorStr)
        sys.exit()
    
    result = dict()
    for prefix in mPrefixList:
        prefix = str(prefix)
        logger.debug("Now :"+prefix)
        if prefix.endswith('.seq') :
            prefix = prefix.replace('.seq','')
        elif prefix.endswith('.ab1') :
            prefix = prefix.replace('.ab1','')
        elif prefix.endswith('.phd.1') :
            prefix = prefix.replace('.phd.1','')
            
        try:
            rowsUpdated = updateChecked(prefix, logger)
            if(rowsUpdated > 0 ):
                result[prefix] = 1
            else:
                result[prefix] = 0
        except:
            result[prefix] = -1
    # Display back to the client
    logger.debug("Printing Web Results")
    htmlObj = PrintHTML(mFormat)
    htmlObj.printResults(result)
    # Exit successfully no matter what, the HTTP Status code gives the actual exit status code
    sys.exit(0)

     
if __name__ == '__main__':
    main()
