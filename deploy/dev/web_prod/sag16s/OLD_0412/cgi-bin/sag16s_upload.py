#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python
'''
Created on Dec 20, 2012

@author: asyed
'''

import os, sys, commands, cgi
import cgitb; cgitb.enable()
import shutil
import core.utils.commons
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqc_sag_file import SAGFile

class DataSet:
    '''
    This is a container class which holds all files for a specific pattern
    Currently holds, *.ab1, *.phd.1, and *.seq files for a specific '*' pattern
    '''
    _phdFile = None
    _ab1File = None
    _seqFile = None
    
    FILE_TYPE_PHD = "phd"
    FILE_TYPE_AB1 = "ab1"
    FILE_TYPE_SEQ = "seq"
    
    def __init__(self, dfile, ftype):
        self.setData(dfile, ftype)
    
    def getPhd(self):
        return self._phdFile
    
    def getAB1(self):
        return self._ab1File
    
    def getSeq(self):
        return self._seqFile
    
    def setPhd(self, phd):
        self._phdFile = phd
        
    def setAB1(self, ab1):
        self._ab1File = ab1
        
    def setSeq(self, seq):
        self._seqFile
        
    def setData(self, dfile, ftype):
        if(ftype == self.FILE_TYPE_AB1):
            self._ab1File = dfile
        if(ftype == self.FILE_TYPE_PHD):
            self._phdFile = dfile
        if(ftype == self.FILE_TYPE_SEQ):
            self._seqFile = dfile


class UploadHTML():
    
    _validFiles = []
    _unknownFiles = []
    _noPhdFiles = []
    _duplicateFiles = []
    _stagingDir = None
    _userName = None
    _pcrPlateId = None
    _runType = 0
    _logger = None
    
    def __init__(self, validList, unknownList, noPhdList, duplicateList, stagingDir, uname, pcrPlateId, runType, logger):
        self._stagingDir = stagingDir 
        self._userName = uname
        self._duplicateFiles = duplicateList
        self._noPhdFiles = noPhdList
        self._unknownFiles = unknownList
        self._validFiles = validList
        self._pcrPlateId = pcrPlateId
        self._runType = runType
        self._logger = logger
        logger.debug("Got for UploadHTML: "+runType+" "+pcrPlateId)

    def printScriptTag(self):
        print ' <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>'
        print ' <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.banner.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.html_util.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.menubar.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.tab.js"></script>'
        print ' <script type="text/JavaScript" src="/upload_handler.js"> </script>'

    
    def printStyle(self):
        print '''
        <style>
        #submitSag16s{
            background: #6f9ff1;
            color: #333300;
            font-weight: 700;
            font-style: normal;
            border: 0;
            cursor: pointer;
            font-size: 14px;
            padding:0.35em
        }
        
        #submitSag16s:hover{
            background: #89a7f1;
        }
        
        #cancelUpload{
            background: #FF5555;
            color: #333300;
            font-weight: 700;
            font-style: normal;
            font-size: 14px;
            border: 0;
            cursor: pointer;
            padding:0.35em

        }
        
        #cancelUpload:hover{
            background: #FF8888;
        }
        </style>
        '''
        
        
    def printStart(self):
        print 'Content-type: text/html\n'
        #print 'Status: 200 OK\n\n'
        print '<html>'
        print ' <title>RQC SAG16S Upload Validation</title>'
        self.printScriptTag()
        self.printStyle()
        print ' <body>'
        print ' <table id="banner"></table>'
        #print "Plate: "+self._pcrPlateId
        #print "Run Type: "+self._runType
        print ' <div id="uploadDiv" name="uploadDiv">'
        print '  <input type="hidden" id="stagingDir" name="stagingDir" value="'+self._stagingDir+'"> </input>'
        print '  <input type="hidden" id="userName" name="userName" value="'+self._userName+'"> </input>'
        print '  <input type="hidden" id="pcrPlateId" name="pcrPlateId" value="'+self._pcrPlateId+'"> </input>'
        print '  <input type="hidden" id="runType" name="runType" value="'+self._runType+'"> </input>'
    
    
    def printStop(self):
        print '  <table name="submitTable" id="submitTable">'
        print '   <tr> '
        print '    <td><input type="button" name="submitSag16s" id="submitSag16s" value="Upload 16s Sequences"> </td>'
        print '    <td><input type="button" name="cancelUpload" id="cancelUpload" value="Cancel 16s Upload"> </td>'
        print '   </tr>'
        print '  </table>'
        print ' </div>'
        print ' </body>'
        print '</html>'
    
    
    def printUnknowns(self):
        unknownFiles = self._unknownFiles
        if(unknownFiles == None ): return True
        if(len(unknownFiles) == 0): return True
        print '  <table name="unknownTable" id="unknownTable" border="2" cellpadding="2">'
        print '  <font color="red"> <h3>UNKNOWN Formatted 16s Sequences: </h3> </font>'
        for dfile in unknownFiles:
            print '   <tr>'
            print '    <td><font color="red">'+dfile+"</font></td>"
            print '   </tr>'
        print '  </table>'
        print '<b><u><font size="3">Required 16s Sequence File Name Format:<font></u></b><br>'
        print '  <p><font size="2" color="blue">'
        print '    <i>WellPositionByColumn</i>-.<i>Proposal</i>.<i>Project</i>.<i>Sample</i>.<i>SortWindow</i>.<i>Plate #</i>.<i>MDAPlateWellPosition</i>.<i>WellType</i>_<i>SangerWellPosition</i>_<i>SangerWellLocation</i><br>'
        print '  </font>'
        print '  <font size="2"><i>Please note that the delimiters ".", "_" and "-" need to specified at appropriate location, as shown above.</i></font><br>'
        print "<br>"
        print ' <input type="hidden" id="cantUpload" name="cantUpload" value="something"> </input>'
    
    
    def printSuccess(self):
        validFiles = self._validFiles
        print '  <table name="successTable" id="successTable" border="2" cellpadding="2">'
        print '  <font color="green"> <h3>SUCCESSFULLY Validated 16s Sequences: </h3> </font>'
        print '  <font color="green"> <h4>Count: '+str(len(validFiles))+'</h4> </font>'
        for dfile in validFiles:
            print "<input type='hidden' class='newData' name='fileName' id='fileName' value='"+dfile+"'>"
            #print '   <tr>'
            #print '    <td><font color="green">'+"   <input type='hidden' class='newData' name='file1' value='"+dfile+"'>"+dfile+"<br>"+"</font></td>"
            #print '   </tr>'
        print '  </table>'
        print "<br>"
    
    
    def printNoPhds(self):
        noPhdFiles = self._noPhdFiles
        if(noPhdFiles == None ): return True
        if(len(noPhdFiles) == 0): return True
        print ' <input type="hidden" id="cantUpload" name="cantUpload" value="something"> </input>'
        print '  <table name="noPhdsTable" id="noPhdsTable" border="2" cellpadding="2">'
        print '  <font color="orange"> <h3>No Associated <i><u>phd.1</u></i> 16s Sequences: </h3> </font>'
        for dfile in noPhdFiles:
            print '   <tr>'
            print '    <td><font color="orange">'+dfile+"</font></td>"
            print '   </tr>'
        print '  </table>'
        print "<br>"
    
    
    def printDuplicates(self):
        duplicateList = self._duplicateFiles
        if(duplicateList == None ): return True
        if(len(duplicateList) == 0): return True
        print '<font color="blue"> <h3>DUPLICATE 16s Sequences (already registered): </h3></font>'
        print '<font color="blue"> </font>'
        print '<table name="duplicatesTable" id="duplicatesTable" border="2" cellpadding="2">'
        for dfile in duplicateList:
            print '<tr>'
            print '<td><input type="checkbox" id="'+dfile+'" name="'+dfile+'" value="'+dfile+'" onclick=></td>'
            print '<td><font color="blue">'+dfile+"</font></td>"
            print '</tr>'
        print '<tr><td><input type="checkbox" id="CheckAll" name="CheckAll" value="CheckAll"></td>'
        print '<td><font color="blue"><b>Check All (re-register sequences)</b></font></td>'
        print '</table>'
        print "<br>"
        
        
    def printHTML(self):
        self.printStart()
        self.printSuccess()
        self.printNoPhds()
        self.printUnknowns()
        self.printDuplicates()
        self.printStop()


def makeDataSet(fileList, logger):
    '''
    This method actually makes the DataSet objects which are for each 
    type of DataSet:
        - New DataSet
        - Duplicate DataSet
        - MissingPHD DataSet
        - UnKnown DataSet
    '''
    dsMap = dict()
    unKnownMap = dict()
    noPhdMap = dict()
    duplicateMap = dict()
    finalValidFileMap = dict()
    # For each file in file list, organize the data
    for fileitem in fileList:
        fileName = fileitem.filename
        #
        # Validate the file name pattern
        formatValidator = SAGFile(fileName)
        if(not formatValidator.isValidFormat()):
            unKnownMap[fileName] = "Unknown"
            continue
        pattern = formatValidator.getPattern()
        #
        # pattern seems to be valid
        ext = fileName[-4:]
        ext1 = fileName[-6:]
        file_type = None
        #
        # get the file type
        if(ext == ".seq"):
            file_type = DataSet.FILE_TYPE_SEQ
        elif(ext == ".ab1"):
            file_type = DataSet.FILE_TYPE_AB1
        elif(ext1 == ".phd.1"):
            file_type = DataSet.FILE_TYPE_PHD
        else:
            logger.error("Unknown file name requested for registration. Name: "+fileName)
            unKnownMap[fileName] = "Unknown"
            continue
        #
        # Create or add to the data set
        if(dsMap.has_key(pattern)):
            logger.debug( "exists adding "+fileName+" type "+file_type+" pattern "+pattern)
            dsMap.get(pattern).setData(fileName, file_type)
        else:
            logger.debug("New adding "+fileName+" type "+file_type+" pattern "+pattern)
            dsMap[pattern] = DataSet(fileName, file_type)
    # End of for loop
    
    #
    # Now clean up the file list to exclude the duplicate data and files with no associated phd files
    cleanUpDuplicateData(dsMap, duplicateMap, logger)
    cleanUpNoPhdData(dsMap, noPhdMap, logger)
    #
    # From the cleaned up list, construct the final GOOD data, a HashMap
    patternList = dsMap.keys()
    for pattern in patternList:
        # get the data set with this pattern
        ds = dsMap.get(pattern)
        finalValidFileMap[ds.getPhd()] = 1
        finalValidFileMap[ds.getAB1()] = 1
        finalValidFileMap[ds.getSeq()] = 1
    #if finalValidFileMap is not None:  finalValidFileMap.pop(None)
    #
    # Return both cleaned up Valid and invalid
    return [finalValidFileMap, unKnownMap, noPhdMap, duplicateMap, dsMap]

#
# Identify the file pattern with no associated Phd files
# Essentially these would be ab1 and seq files with no associated phd.1 file
def cleanUpNoPhdData(dsMap, noPhdMap, logger):
    '''
    Finds valida patterns with no PHD files associated with them.
    No PHDs not possible to process.
    '''
    patternList = dsMap.keys()
    for pattern in patternList:
        # get the data set with this pattern
        ds = dsMap.get(pattern)
        phdFile = ds.getPhd()
        ab1File = ds.getAB1()
        seqFile = ds.getSeq()
        
        if(phdFile == None):
            if ab1File is not None:
                noPhdMap[ab1File] = "NoPhds"
            if seqFile is not None:
                noPhdMap[seqFile] = "NoPhds"
            dsMap.pop(pattern)
    return True


#
# Identify already registered file pattern. This wouldn't necessarily preclude them from registering.
# They could be registered, however, a confirmation is needed to overwrite the file content and mark them for reprocessing
def cleanUpDuplicateData(aMap, duplicateMap, logger):
    patternList = aMap.keys()
    for pattern in patternList:
        # get the data set with this pattern
        ds = aMap.get(pattern)
        phdFile = ds.getPhd()
        ab1File = ds.getAB1()
        seqFile = ds.getSeq()
        logger.debug("Checking if the pattern is duplicate: "+pattern)
        if core.utils.commons.isDuplicateSagPattern(pattern, logger):
            duplicateMap[phdFile] = "Duplicate"
            if ab1File is not None:
                duplicateMap[ab1File] = "Duplicate"
            if seqFile is not None:
                duplicateMap[seqFile] = "Duplicate"
            aMap.pop(pattern)
    return True


#
# Main program here
def main(logger, prop):
    # Staging directory for temporarily copying the user uploaded data to here
    # register script will actually do the copying into the RQC fs locations
    staging_dir = prop.getFSSag16sStagingDir()
    form = cgi.FieldStorage()
    uName = None
    
    if 'sag16sFiles' in form:
        filelist = form['sag16sFiles']
        uName = form.getvalue('userName')
        pcrPlateId = form.getvalue('pcrPlateId')
        rndRun = form.getvalue('rndSeq')
        run_type = "CSP"
        if rndRun: run_type = "RnD"
        if not isinstance(uName, str):
            uName = str(uName)
        if not isinstance(filelist, list):
            filelist = [filelist]
        #
        # Make data set of what is and what isn't valid format
        result = makeDataSet(filelist, logger)
        # valid data sets
        validFilesMap   = result[0]
        unKnownFileMap = result[1]
        noPhdsFileMap  = result[2]
        duplicateFileMap = result[3]
        
        for fileitem in filelist:
            if not fileitem.file: return
            if( not validFilesMap.has_key(fileitem.filename) and not duplicateFileMap.has_key(fileitem.filename)):
                continue
            fout = file(os.path.join(staging_dir, fileitem.filename), 'wb')
            while 1:
                chunk = fileitem.file.read(100000)
                if not chunk: break
                fout.write (chunk)
            fout.close()
            #if(validFilesMap.has_key(fileitem.filename)):
            #    validFileList.append(fileitem.filename)
            #else:
            #    duplicateFileList.append(fileitem.filename)
    else:
        print "No elements found"
    
    htmlObj = UploadHTML(validFilesMap.keys(), unKnownFileMap.keys(), noPhdsFileMap.keys(),duplicateFileMap.keys(), staging_dir, uName, pcrPlateId, run_type, logger)
    htmlObj.printHTML()
    
#
# Default calling script as a CLI
if __name__ == '__main__':
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("uploadSAG16s")
    prop = Properties()
    main(logger, prop)
