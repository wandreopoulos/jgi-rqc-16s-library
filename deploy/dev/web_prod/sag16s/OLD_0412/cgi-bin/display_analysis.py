#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

'''
Created on Jan 11, 2013

@author: asyed
'''

import cgi
import cgitb; cgitb.enable()
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
import json
import urllib


class SearchHTML():
    
    _analysisResults = []
    _displayString = None
    def __init__(self, searchResult, dispStr):
        self._analysisResults = searchResult
        self._displayString = dispStr

    def printStart(self):
        print 'Content-type: text/html\n'
        print '<html>'
        print ' <title>RQC Sag16s Analysis Search ('+self._displayString+')</title>'
        self.printScript()
        self.printStyle()
    
    def printScript(self):
        print ' <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>'
        print ' <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.banner.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.html_util.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.menubar.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.tab.js"></script>'
        print '''
		<link rel="stylesheet" href="/tablelib/themes/green/style.css" type="text/css" id="" media="print, projection, screen" />
		<style>
            th.header { 
                background-image: url(/img/sort-none.png);     
                cursor: pointer; 
                font-weight: bold; 
                background-repeat: no-repeat; 
                background-position: center left; 
                padding-left: 20px;
                font-size: 14px;
                background-color: #89a7f1;
                border-right: 1px solid #dad9c7; 
                margin-left: -1px; 
            }
            
            th.headerSortUp { 
                background-image: url(/img/sort-asc.png); 
                background-color: #3399FF; 
            }
            
            th.headerSortDown { 
                background-image: url(/img/sort-desc.png); 
                background-color: #3399FF; 
            }
            
            tr:nth-child(even) {background: #FF8888}
            tr:nth-child(odd) {background: #CCC}
            
		</style>
		<script type="text/javascript" src="/tablelib/jquery-latest.js"></script>
		<script type="text/javascript" src="/tablelib/jquery.tablesorter.js"></script>
		<script type="text/javascript">
            $(function() {
                $("table").tablesorter({debug: true});
            });
            
            $(document).ready(function() {
                $("#banner").banner(); 
                $().banner_title({
                    type:'major',
                    data:'RollingQC SAG16s UI'
                });
                $().banner_title({
                    type:'minor',
                    data:'RQC Sag16s Validation Status',
                });
                $().banner_title({type:'info', data: ''});
            });
		</script>
'''

    
    def printBody(self):
        
        print ' <body>'
        print ' <table id="banner"></table>'
        print ' <div id="uploadDiv" name="uploadDiv">'
        print ' <h2><center>RQC Sag16s Analysis Results<i>'+self._displayString+'</i></center></h2>'
        print '''
    <table cellpadding="1" cellspacing="1" border="1" class="display" name ="analysisTable" id="rowspan" class="tablesorter">
    <thead>
      <tr>
        <th>Sequence Name</th> <th>User</th> <th>Proposal</th> <th>Cell Material</th> <th>PCR Plate</th> <th>PCR Well</th> <th>Cherry</th> <th>Protocol</th>
        <th>Seq. Well</th> <th>Direction</th> <th>Seq. Row</th> <th>Q15 TrimLen</th> <th>Marked</th> <th>Subject</th> <th>PercentId</th> <th>AlignLen</th> <th>MisMatches</th>
        <th>Gaps</th> <th>QueryStart</th> <th>QueryEnd</th> <th>SubStart</th> <th>SubEnd</th> <th>EValue</th> <th>Score</th> <th>ContamInfo</th> <th>Domain</th> <th>Kingdom</th>
        <th>Phylum</th> <th>Class</th> <th>Order</th> <th>Family</th> <th>Genus</th>
      </tr>
    </thead>
'''
        for hit in self._analysisResults:
            print '<tr>'
            for element in hit:
                print '<td>'+str(element)+'</td>'
            print '</tr>'
        print '</table>'
        print '</div>'
        print '</body>'
    
    def printEnd(self):
        print '</html>'
    
    def printStyle(self):
        pass
    
    def printHTML(self):
        self.printStart()
        self.printBody()
        self.printEnd()


def getSearchResults(searchStr, logger):
    
    url = 'http://monrovia.jgi-psf.org:8081/cgi-bin/searchSag16s.py?'+searchStr
    logger.debug("URL: "+url);
    u = urllib.urlopen(url)
    data = u.read()
    logger.debug(data);
    data = json.loads(data)   
    hits = []
    #logger.debug("Data after Json: "+data);
    for hit in data:
        cHit = []
        cHit.append(hit['seq_name'])
        cHit.append(hit['user_name'])
        cHit.append(hit['proposal_id'])
        cHit.append(hit['cell_material'])
        cHit.append(hit['pcr_plate'])
        cHit.append(hit['pcr_well'])
        cHit.append(hit['cherry'])
        cHit.append(hit['protocol'])
        cHit.append(hit['seq_well_location'])
        cHit.append(hit['seq_direction'])
        cHit.append(hit['seq_row_position'])
        cHit.append(hit['q15_tlength'])
        cHit.append(hit['cherry_picked'])
        cHit.append(hit['blast_subject_id'])
        cHit.append(hit['blast_percenti'])
        cHit.append(hit['blast_align_length'])
        cHit.append(hit['blast_mismatches'])
        cHit.append(hit['blast_gaps'])
        cHit.append(hit['blast_q_start'])
        cHit.append(hit['blast_q_end'])
        cHit.append(hit['blast_s_start'])
        cHit.append(hit['blast_s_end'])
        cHit.append(hit['blast_evalue'])
        cHit.append(hit['blast_score'])
        cHit.append(hit['contam_info'])
        cHit.append(hit['rdp-domain'])
        cHit.append(hit['rdp-kingdom'])
        cHit.append(hit['rdp-phylum'])
        cHit.append(hit['rdp-class'])
        cHit.append(hit['rdp-order'])
        cHit.append(hit['rdp-family'])
        cHit.append(hit['rdp-genus'])
        hits.append(cHit)
    return hits


if __name__ == '__main__':
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("analysisSAG16s")
    prop = Properties()
    form = cgi.FieldStorage()
    logger.debug("Starting the analysis search process.")
    proposalId = form.getvalue('proposal')
    cellMaterialId = form.getvalue('cellmaterial')
    pcrPlateId = form.getvalue('pcrplate')
    cherryId = form.getvalue('cherry')
    searchStr=''
    dispStr = '(Searching for: '
    if( proposalId != None and proposalId != '') :
        searchStr = searchStr + 'proposal='+str(proposalId)
        dispStr = dispStr+' Proposal: '+str(proposalId)
    if( cellMaterialId != None and cellMaterialId != '') :
        searchStr = searchStr + '&cellmaterial='+str(cellMaterialId)
        dispStr = dispStr+' CellMaterial: '+str(cellMaterialId)
    if( pcrPlateId != None and pcrPlateId != '') :
        searchStr = searchStr + '&pcrplate='+str(pcrPlateId)
        dispStr = dispStr+' PCR Plate: '+str(pcrPlateId)
    if( cherryId != None and cherryId != '') :
        searchStr = searchStr + '&cherry='+str(cherryId)
        dispStr = dispStr+' Cherry: '+str(cherryId)
    dispStr = dispStr+')'
    htmlObj = SearchHTML(getSearchResults(searchStr, logger), dispStr)
    htmlObj.printHTML()
    
