#!/house/homedirs/a/asyed/Python/Versions/2.7/bin/python

#
# Prototyping python cgi for sag16s search page
#
# Created: Jun 20 2013
# Modified: Jun 26 2013
#
# andreopo
#

#import math            # ceil
import cgi, cgitb
import json
import MySQLdb as mdb
import os
import re            # regex
import base64

#import Util.Jgi_rqc_db as rqcdb

##
## Need to install gviz table module
## Ref) http://gviz-data-table.readthedocs.org/en/latest/#
##
# from gviz_data_table import Table, encode
import datetime
import sys
#from collections import defaultdict

from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb
import core.utils.commons

from needlemanwunsch import NeedlemanWunsch


# Create instance of FieldStorage
#form = cgi.FieldStorage()

#debug = False



def pairwise_seq_alignment(s,t):
    return 90

def sortedDictValues2(adict):
    keys = adict.keys()
    keys.sort(reverse=True)
    result = []
    for key in keys:
        temp = adict[key]
        for t in temp:
            result.append( t )
    return result



def db_query():

    #db_pwd = "b1Jpb24uVGhyeWxsLnJhdDo5OQ=="
    #db_pwd = "dHJvd3QudGFYZXMub0Jsb25nLjIyMzs="
    #db_pwd = base64.decodestring(db_pwd)

    con = mdb.connect('sequoia.jgi-psf.org', 'jazz', 'jazz234', 'rqc_16s')
    cursor = con.cursor(mdb.cursors.DictCursor)

    sag16s_for_display = [];

    # Create instance of FieldStorage
    form = cgi.FieldStorage()

    # Get the search string
    # Get data from fields
    if form.getvalue('search'):
        search_string = form.getvalue('search').upper()
        search_string.strip()
    else:
        search_string = ""


#######################################
# Check if search is empty, in which case call hierdenc and return all sag16s
# else perform the search below.
#######################################
    words = re.findall('[ACGT]+', search_string) #words(search_string); #search_string.split(" ");

    search_string = "";
    for w in words:
        search_string = search_string + w;

    print search_string

    #The set of all references that resemble the search string
    sag16s_data = {}

    cursor.execute("SELECT * FROM sag16s_seq ss inner join sag16s_top_hit sth ON ss.sag_seq_id = sth.sag_seq_id WHERE ss.ss_status_cv_id = 10")
    sag16s_rows = cursor.fetchall()
    print str(sag16s_rows)
    for r in sag16s_rows:
        #print "sag_seq_id " +  str(r['sag_seq_id'])
        sag16s_as_list = []
        sag16s_as_list.append(str(r['sag_seq_id']))
        #sag16s_as_list.append(str(r['fs_location']))# + ' ' + str(r['type']) + ' ' + str(r['description']) + ' ' + str(r['external_version_num']) + ' ' + str(r['comments'])

        #Get the sequence
        trim_seq = ""
        #for seq in sag16sList :
        #    trim_seq = ""
        # seq = row
        # logger.info("SEQ_INFO for " + str(row[1]))
        trim_seq_file = str(r['fs_location']) + "/" + core.utils.commons.getSag16sFaName( str(r['seq_name']) )+".trim"
        #print "trim_seq_file: " + trim_seq_file + "\n\n"
        # logger.info("trim_seq_file location: " + trim_seq_file)
        # Read the file and all lines and set string trim_seq to the concat of the lines.
        f = open(trim_seq_file, 'r')
        # Skip the 1st header line in the fasta file
        f.readline()
        for line in f:
            trim_seq += line.strip()
        f.close()
        # logger.info("trim_seq: " + trim_seq)
        # trim_seq = trim_seq.replace("N", "")
        words = re.findall('[ACGT]+', trim_seq) #words(search_string); #search_string.split(" ");
        trim_seq_clean = "";
        for w in words:
            trim_seq_clean = trim_seq_clean + w;
        sag16s_as_list.append(str(trim_seq_clean))

        # Get the Blast tophit
        sag16s_as_list.append(str(r['subject_id']))
        # Get the seq name
        sag16s_as_list.append(str(r['seq_name']))
        # Get the proposal
        sag16s_as_list.append(str(r['proposal_id']))
        # Get the project
        sag16s_as_list.append(str(r['project']))
        # Get the sample
        sag16s_as_list.append(str(r['sample']))
        # Get the sort window
        sag16s_as_list.append(str(r['sort_window']))
        #append to dictionary
        sag16s_data[str(r['sag_seq_id']) ] = sag16s_as_list;



    #give to each reference(search_string) a rank, which is the levenshtein distance to the user search string
    ranked_sag16s = {}
    for sag16s_id in sag16s_data.keys():
#        print "\n_______________\n"
#        print search_string
        ndlw = NeedlemanWunsch(list( sag16s_data[sag16s_id][1] ), list( search_string) )
#        print sag16s_data[sag16s_id][1]
        ndlw.align()
        #print "\nld 0\n\n"
        ld = ndlw.homology()
#        print "pairwise_seq_alignment: " + str(ld) + "\n\n";
        sag16s_data[sag16s_id].insert(0, ld);
        if not ld in ranked_sag16s.keys():
            ranked_sag16s[ld] = []
        ranked_sag16s[ld].append( sag16s_data[sag16s_id] );

    sag16s_for_display = sortedDictValues2(ranked_sag16s);

  #  print "{  \"Seqs\" : [ { \"Sequence\" : \"Micssshael\" , \"Organism\" : \"Msssale\", \"Precision\" : \"12\", \"Donuts eaten\" : \"5\" } ]  }";
    table = Table() ## gviz data table
    table.add_column("Index", int, "Index")
    table.add_column("Similarity", float, "Similarity")
    table.add_column("Sequence Name", str, "Sequence Name")
    table.add_column("Proposal", str, "Proposal")
    table.add_column("Project", str, "Project")
    table.add_column("Sample", str, "Sample")
    table.add_column("Sort Window", str, "Sort Window")
    table.add_column("Blast Top Hit", str, "Blast Top Hit")
    table.add_column("Trimmed Sequence", str, "Trimmed Sequence")


    index = 0
    for ranked_16s in sag16s_for_display:
        index += 1
        to_add = [ index, ranked_16s[0], ranked_16s[4], ranked_16s[5], ranked_16s[6], ranked_16s[7], ranked_16s[8], ranked_16s[3], ranked_16s[2] ]
#        print "toadd " + str(to_add) + "\n\n"
        table.append( to_add )

    cursor.close()
    con.close()

#    print "THEEND " + str(table)

    return encode(table)


# Call main function.
print('Content-type: application/json\r\n\r\n')
print(db_query())

