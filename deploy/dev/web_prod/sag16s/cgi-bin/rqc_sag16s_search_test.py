#!/usr/bin/env python

#
# Prototyping python cgi for sag16s search page
#
# Created: Jun 20 2013
# Modified: Jun 26 2013
#
# andreopo
#

#import math            # ceil
import cgi, cgitb
import json
import MySQLdb as mdb
import os
import re            # regex
import base64

#import Util.Jgi_rqc_db as rqcdb

##
## Need to install gviz table module
## Ref) http://gviz-data-table.readthedocs.org/en/latest/#
##
from gviz_data_table import Table, encode
import time
import datetime
import sys
#from collections import defaultdict

from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb
import core.utils.commons

###from needlemanwunsch import NeedlemanWunsch
from subprocess import Popen, PIPE



# Create instance of FieldStorage
#form = cgi.FieldStorage()

#debug = False




#
# Create a timestamp with the format YYYYMMDDHHMMSS.
#
def create_timestamp():
    # 14 digits YYYYMMDDHHMMSS
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    hour = datetime.datetime.now().hour
    minute = datetime.datetime.now().minute
    second = datetime.datetime.now().second
    if (month < 10):
        month = "0" + str(month);
    if (day < 10):
        day = "0" + str(day);
    if (hour < 10):
        hour = "0" + str(hour);
    if (minute < 10):
        minute = "0" + str(minute);
    if (second < 10):
        second = "0" + str(second);
    res = str(year) + str(month) + str(day) + str(hour) + str(minute) + str(second);
    return res;




def pairwise_seq_alignment(s,t):
    return 90

def sortedDictValues2(adict):
    keys = adict.keys()
    keys.sort(reverse=True)
    result = []
    for key in keys:
        temp = adict[key]
        for t in temp:
            result.append( t )
    #print "RESULT " + str(result)
    return result



def db_query():

    #db_pwd = "b1Jpb24uVGhyeWxsLnJhdDo5OQ=="
    #db_pwd = "dHJvd3QudGFYZXMub0Jsb25nLjIyMzs="
    #db_pwd = base64.decodestring(db_pwd)

    con = mdb.connect('draw.jgi-psf.org', 'jazz', 'jazz234', 'rqc_16s')
    cursor = con.cursor(mdb.cursors.DictCursor)

    sag16s_for_display = [];

    # Create instance of FieldStorage
    form = cgi.FieldStorage()

    # Get the search string
    # Get data from fields
    if form.getvalue('search'):
        search_string = form.getvalue('search').upper()
        search_string.strip()
    else:
        search_string = ""

    # search_string = "TCGA"

#######################################
# Check if search is empty, in which case call hierdenc and return all sag16s
# else perform the search below.
#######################################
    words = re.findall('[ACGT]+', search_string) #words(search_string); #search_string.split(" ");

    search_string = "";
    for w in words:
        search_string = search_string + w;

    #print search_string

    #The set of all references that resemble the search string
    sag16s_data = {}

    cursor.execute("SELECT * FROM sag16s_seq ss inner join sag16s_top_hit sth ON ss.sag_seq_id = sth.sag_seq_id WHERE ss.ss_status_cv_id = 10 and sth.analysis_type = 'homology'" )
    sag16s_rows = cursor.fetchall()
    # print str(sag16s_rows)
    for r in sag16s_rows:
        #print "sag_seq_id " +  str(r['sag_seq_id'])
        sag16s_as_list = []
        sag16s_as_list.append(str(r['sag_seq_id']))
        #sag16s_as_list.append(str(r['fs_location']))# + ' ' + str(r['type']) + ' ' + str(r['description']) + ' ' + str(r['external_version_num']) + ' ' + str(r['comments'])

        #Get the sequence
        trim_seq = ""
        #for seq in sag16sList :
        #    trim_seq = ""
        # seq = row
        # logger.info("SEQ_INFO for " + str(row[1]))
        # Not the trimmed file since sometimes in dev sequences do not have a trimmed file
        trim_seq_file = str(r['fs_location']) + "/" + core.utils.commons.getSag16sFaName( str(r['seq_name']) ) ###+".trim"
        #print ">" + core.utils.commons.getSag16sFaName( str(r['seq_name']) )+"\n"
        #print "trim_seq_file: " + trim_seq_file + "\n\n"
        # logger.info("trim_seq_file location: " + trim_seq_file)
        # Read the file and all lines and set string trim_seq to the concat of the lines.
        f = open(trim_seq_file, 'r')
        # Skip the 1st header line in the fasta file
        f.readline()
        for line in f:
            trim_seq += line.strip()
        f.close()
        # logger.info("trim_seq: " + trim_seq)
        # trim_seq = trim_seq.replace("N", "")
        words = re.findall('[ACGT]+', trim_seq) #words(search_string); #search_string.split(" ");
        trim_seq_clean = "";
        for w in words:
            trim_seq_clean = trim_seq_clean + w;
        sag16s_as_list.append(str(trim_seq_clean))
        #print (str(trim_seq_clean) + "\n")

        # Get the Blast tophit
        sag16s_as_list.append(str(r['subject_id']))
        # Get the seq name
        sag16s_as_list.append(str(r['seq_name']))
        # Get the proposal
        sag16s_as_list.append(str(r['proposal_id']))
        # Get the project
        sag16s_as_list.append(str(r['project']))
        # Get the sample
        sag16s_as_list.append(str(r['sample']))
        # Get the sort window
        sag16s_as_list.append(str(r['sort_window']))
        #append to dictionary
        sag16s_data[str(r['sag_seq_id']) ] = sag16s_as_list;

    timestamp = create_timestamp()

    # Open the query and target fasta files
    key = search_string[:min(10,len(search_string))]
    target_fasta = os.path.join("tmp", key + '_target_'+timestamp+'.fasta')
    query_fasta = os.path.join("tmp", key + '_query_'+timestamp+'.fasta')
    key_query_timestamp = open(query_fasta, 'w')
    key_query_timestamp.write(">" + key + "\n" + str(search_string) + "\n")
    key_query_timestamp.close()
    key_target_timestamp = open(target_fasta, 'w')

    #give to each reference(search_string) a rank, which is the levenshtein distance to the user search string
    ranked_sag16s = {}
    for sag16s_id in sag16s_data.keys():
         key_target_timestamp.write(">"+ str(sag16s_id) +"\n" + str(sag16s_data[sag16s_id][1]) + "\n")
    key_target_timestamp.close()

    # Run Smith-Waterman on query (key_query_timestamp.fasta) and target (key_target_timestamp.fasta) and output results to file key_results_timestamp.out
    outfile = os.path.join("tmp", key + '_results_'+timestamp+'.out')
    cmd = ["test_smith_waterman/Complete-Striped-Smith-Waterman-Library-master/src/ssw_test", "-cr", target_fasta, query_fasta] ###, ">" , outfile]
    ###print "cmd " + str(cmd)
    with open(outfile, 'w') as output:
        p = Popen(cmd, stdout = output, stderr = PIPE)
        std_out, std_err = p.communicate()
        #print str(std_err)
        #print str(p.returncode)


    # Each block in the results corresponds to a key2, so foreach block parse the key2 and the similarity n.
    RESULTS = open(outfile, 'r')
    while True:
        line = RESULTS.readline()
        #print line
        if line == '':
            break
        if line.startswith("target_name"):
            sag16s_id = line.strip().replace("target_name: ", "")
            #print "line " + line + " key2 " + str(sag16s_id)
            line = RESULTS.readline()
            line = RESULTS.readline()
            ld = 0
            if line.startswith("optimal_alignment_score"):
                myarray = re.findall("(\d+)", line)
                alignment = float(myarray[0])
                target_begin = float(myarray[-4])
                target_end = float(myarray[-3])
                query_begin = float(myarray[-2])
                query_end = float(myarray[-1])
                ld = alignment / ((target_end - target_begin + 1) + (query_end - query_begin + 1))

#        print "\n_______________\n"
#        print search_string
#        ndlw = NeedlemanWunsch(list( sag16s_data[sag16s_id][1] ), list( search_string) )
#        print sag16s_data[sag16s_id][1]
#        ndlw.align()
        #print "\nld 0\n\n"
#        ld = ndlw.homology()
            #print "pairwise_seq_alignment: " + str(ld) + "\n\n";
            sag16s_data[sag16s_id].insert(0, ld);
            if not ld in ranked_sag16s.keys():
                ranked_sag16s[ld] = []
            ranked_sag16s[ld].append( sag16s_data[sag16s_id] );

    sag16s_for_display = sortedDictValues2(ranked_sag16s);

  #  print "{  \"Seqs\" : [ { \"Sequence\" : \"Micssshael\" , \"Organism\" : \"Msssale\", \"Precision\" : \"12\", \"Donuts eaten\" : \"5\" } ]  }";
    table = Table() ## gviz data table
    table.add_column("Index", int, "Index")
    table.add_column("Similarity", float, "Similarity")
    table.add_column("Sequence Name", str, "Sequence Name")
    table.add_column("Proposal", str, "Proposal")
    table.add_column("Project", str, "Project")
    table.add_column("Sample", str, "Sample")
    table.add_column("Sort Window", str, "Sort Window")
    table.add_column("Blast Top Hit", str, "Blast Top Hit")
    table.add_column("Trimmed Sequence", str, "Trimmed Sequence")

    #print "sag16s_for_display " + str(sag16s_for_display)

    #max_flt = 0
    index = 0
    for ranked_16s in sag16s_for_display:
        #print "ranked_16s " + str(index) + " " + str(ranked_16s) + "\n\n"
        index += 1
        #if max_flt == 0:
        #    max_flt = float(ranked_16s[0])
        to_add = [ index, round(float(ranked_16s[0])*100, 2), ranked_16s[4], ranked_16s[5], ranked_16s[6], ranked_16s[7], ranked_16s[8], ranked_16s[3], ranked_16s[2] ]
        #print "toadd " + str(to_add) + "\n\n"
        table.append( to_add )

    cursor.close()
    con.close()

#    print "THEEND " + str(table)

    return encode(table)


# Call main function.
print('Content-type: application/json\r\n\r\n')
print(db_query())

