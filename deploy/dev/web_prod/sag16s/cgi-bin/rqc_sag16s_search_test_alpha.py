#!/house/homedirs/a/asyed/Python/Versions/2.7/bin/python

#
# Prototyping python cgi for sag16s search page
#
# Created: Jun 20 2013
# Modified: Jun 26 2013
#
# andreopo
#
# RQC-130 and RQC-188
#

#import math            # ceil
import cgi, cgitb
import json
#import MySQLdb as mdb
#import os
#import re            # regex

#import Util.Jgi_rqc_db as rqcdb

##
## Need to install gviz table module
## Ref) http://gviz-data-table.readthedocs.org/en/latest/#
##
from gviz_data_table import Table, encode
import time, datetime
#import sys
#from collections import defaultdict

# Create instance of FieldStorage
#form = cgi.FieldStorage()

#debug = False

def db_query():

    # Create instance of FieldStorage
    form = cgi.FieldStorage()

    search_string = ""

    # Get the search string
    # Get data from fields
    if form.getvalue('search'):
        search_string = form.getvalue('search').lower()
        search_string.strip()
    else:
        search_string = ""

  #  print "{  \"Seqs\" : [ { \"Sequence\" : \"Micssshael\" , \"Organism\" : \"Msssale\", \"Precision\" : \"12\", \"Donuts eaten\" : \"5\" } ]  }";
    table = Table() ## gviz data table
    table.add_column("Sequence", str, "Sequence")
    table.add_column("Organism", str, "Organism")
    table.add_column("Precision", int, "Precision")
    table.add_column("Index", int, "Index")
    table.append([ search_string, "Saccharomyces", 90, 1])
    table.append([ "ACGGGGT", "Populus", 80, 2])
    table.append([ "ACTTTGT", "Saccharomyces", 70, 3])
    table.append([ "ACAAAGT", "Trichoderma", 10, 4])
    return encode(table)


# Call main function.
print('Content-type: application/json\r\n\r\n')
print(db_query())

