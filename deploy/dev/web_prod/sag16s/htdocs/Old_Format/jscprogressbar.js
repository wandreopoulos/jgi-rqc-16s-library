$(function() {
	$('.progressbar').each(function() {
		pb_init($(this));
	});
});
function pb_createIn(j, min, max, val) {
	if(typeof j=="undefined") return;
	
	j.append('<div class="progressbar"><div class="pbfill">'+
		(typeof min!="undefined"?min:0)+'/'+
		(typeof max!="undefined"?max:100)+'/'+
		(typeof val!="undefined"?val:1)+'/</div></div>');
	pb = j.children('.progressbar').last();
	pb_init(pb);
	return pb;
}
function pb_init(j) {
	if(!j.children('.pbfill').length) {
		j.prepend('<div class="pbfill"></div>');
		j.data({"min":0,"max":100,"val":1,"txt":''});
		min = 0; max = 100; val = 1;
	} else {
		t = j.children('.pbfill').text().split('/');
		j.children('.pbfill').text('');
		min = (isNaN(parseInt(t[0]))?0:parseInt(t[0]));
		max = (t.length>1 && !isNaN(parseInt(t[1]))?parseInt(t[1]):100);
		val = (t.length>2 && !isNaN(parseInt(t[2]))?parseInt(t[2]):1);
		if(min>=max) { min = 0; max = 100; }
		if(val<min || val>max) val = min;
		j.data({"min":min,"max":max,"val":val});
	}
	
	w = j.width();
	s = (w/(max-min));
	j.children('.pbfill').css("width", s*val);
}
function pb_setval(j, v) {
	if(!j || !j.hasClass || !j.hasClass('progressbar')) return;
	if(isNaN(parseInt(v)) || typeof j.data('min')=="undefined") return;
	v = parseInt(v);
	
	min = j.data('min');
	max = j.data('max');
	if(v<min || v>max) return;
	
	j.data('val', v).stop(true, true);
	
	w = j.width();
	s = (w/(max-min));
	j.children('.pbfill').css("width", (s*(v-min)));
}
function pb_scrolltoval(j, v, speed) {
	if(!j || !j.hasClass || !j.hasClass('progressbar')) return;
	if(isNaN(parseInt(v)) || typeof j.data('min')=="undefined") return;
	v = parseInt(v);
	speed = (isNaN(speed) || speed<100 || speed>20000?500:speed);
	
	min = j.data('min');
	max = j.data('max');
	if(v<min || v>max) return;
	
	j.data('val', v).stop(true, true);
	
	w = j.width();
	s = (w/(max-min));
	j.children('.pbfill').animate({"width":(s*(v-min))}, speed);
}
