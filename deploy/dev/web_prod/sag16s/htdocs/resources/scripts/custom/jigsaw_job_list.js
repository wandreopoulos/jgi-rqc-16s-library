/**
 * TEMPLATE
 */

var gridCols = {set:false};

$(document).ready(function(){
	var grid = $('#listTable'); // JQuery object reference of table used for our grid display
	
	grid.jqGrid({
 		url:'/cgi-bin/python/rqc_jigsaw_job_list.py', 	// backend cgi returning the table data
		datatype: "json",								// the table data format the cgi returns 
		loadui: 'block',
		altRows: true,
		width:	1500,
		height: 700,
		colNames:['ID','Seq Unit','Lib','Data Type','Status',
					'Count','Work Dir','Reg. Time','Start Time','Stop Time'], 
		colModel:[ 
			{name:'id',index:'id', width:22, align:"center", searchoptions: {sopt:['eq','bw','ew','cn','in']}}, 
			{name:'seq_unit_name',index:'seq_unit_name',width:62,align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'lib',index:'lib', width:20, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'data_type',index:'data_type', width:30, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'status',index:'status', width:28, align:"center",searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'count',index:'count', width:18,align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}}, 
			{name:'work_dir',index:'work_dir', width:135, sortable:false,searchoptions:{sopt:['eq','bw','ew','cn']}}, 
			{name:'reg_time',index:'reg_time', width:60, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
			{name:'start_time',index:'start_time', width:60, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}},
			{name:'stop_time',index:'stop_time', width:60, align:"center",searchoptions:{sopt:['eq','bw','ew','cn','in']}} 
		], 

		rowNum:100, rowList:[100,500,1000], // for paging, default view block size, and size selection
		pager: '#listTablePager', 	// the page UI div id
		toppager: true,		// also show pager at top of table
		sortname: 'stop_time',	// default sorting column
		sortorder: "desc", 		// default sorting order
		viewrecords: true, 		// display "View 1-100 of 12345" at pager right
		caption:"jigsaw jobs and status",	// sohw the top caption UI 
		
 	});

	// show the out-of-box search tool (cloneToTop:true also put all tool on top pager):
	grid.jqGrid('navGrid', '#listTablePager', { edit:false, add:false, del:false, view:true, search:true, cloneToTop:true} );

	//the summary table
	$('#summaryTable').jqGrid({
 		url:'/cgi-bin/python/rqc_jigsaw_job_summary.py', 	// backend cgi returning the table data
		datatype: "json",								// the table data format the cgi returns 
		loadui: 'block',
		altRows: true,
		width:	1500,
		height: 20,
		colNames:['Total','Single Cell','Isolate','Succeeded','Failed', 'Other', 'Oldest','Newest'], 
		colModel:[ 
			{name:'total',index:'total', width:30, align:"center", sortable:false}, 
			{name:'singlecell',index:'singlecell', width:30, align:"center", sortable:false}, 
			{name:'isolate',index:'isolate', width:30, align:"center", sortable:false}, 
			{name:'succeeded',index:'succeeded', width:30, align:"center", sortable:false}, 
			{name:'failed',index:'failed', width:30, align:"center", sortable:false}, 
			{name:'other',index:'other', width:30, align:"center", sortable:false}, 
			{name:'oldest',index:'oldest', width:30, align:"center", sortable:false}, 
			{name:'newest',index:'newest', width:30, align:"center", sortable:false}, 
		], 

		rowNum:100, rowList:[100,500,1000], // for paging, default view block size, and size selection
		sortname: 'stop_time',	// default sorting column
		sortorder: "desc", 		// default sorting order
		viewrecords: true, 		// display "View 1-100 of 12345" at pager right
		caption:"jigsaw job summary",	// sohw the top caption UI 
 	});


});
