#!/jgi/tools/bin/perl

##################
# Converts the headers of nt file into format for blast.
#
# wandreopo             06/03/2103
##################
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage ;

my ( $sample_percent, $sample_count, $fsize, $stats, $help );

#print "\nstats subsampling0000\n";

GetOptions(
        'help!'    => \$help
   ) or pod2usage(2);
pod2usage(1) if $help;

#print "\nstats subsampling1111\n";

#print "\nstats subsampling2222\n";

    my $nt_file = "/house/groupdirs/QAQC/ref_databases/ncbi/CURRENT/nt/nt";

    local(*NT_FILE);

    if (!open(NT_FILE, "$nt_file")) {
        print "Cannot open\n\n";
        return 1;
    }


while ( my $line = <NT_FILE> ) 
{		
        # header check is simply done by counting every 4 lines.
	if ( defined($line) && $line =~ /^>/) #check if header
	{
              ### print "LINEREADIS: $line";
              chomp($line);
              my $first_part = "";
              my $second_part = "";
              #Get 1st part
              #my $first_part = `echo "$line" | perl -anle "s/\\s.\*\$/:/g;print"`;
              $first_part .= $line;
              $first_part =~ s/\s.*$/:::::/g;
              ###print "first: $first_part\n\n";
              #Get 2nd part
              # my $second_part = `echo "$line" | perl -anle "s/^.\*\\s//g;s/\\W/_/g;print" | xargs /projectb/projectdirs/PI/qc/databases/taxonomy/2012.08.22/whatdatax.pl -n `;
              $second_part .= $line;
              $second_part =~ s/^.*?\s//g;
              $second_part =~ s/\W/_/g;
              ######my $cmd = "echo $second_part | /house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-legacy/support_scripts/parallel -j 8 /house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-legacy/ill-bin/whatdatax.pl -n ";
              my $ssecond_part = $second_part; ########`$cmd`;
              chomp($ssecond_part);
              ### print "second: $second_part\n\n";
              #Print 1st part:2nd part
              if (length($ssecond_part) > 1) { print "$first_part$ssecond_part\n"; }
              else { print "$first_part$second_part\n"; }
  	} else {
              print "$line";
        }
}

close(NT_FILE);

