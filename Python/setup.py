import os
from setuptools import setup

setup(
      name='RQC.Core',
      version='1.0',
      description='Rolling QC Software',
      author_email='ASyed@lbl.gov',
      packages=['core','core.resources', 'core.dao', 'core.utils', 'core.utils.io', 'core.utils.io.formats', 'core.webservices', 'core.parsers', 'core.tools', 'core.utils.io'],
      package_data = { '': ['*.properties', '*.config', '*.entries']}
      )
