#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

import commands, cgi
import cgitb; cgitb.enable()
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from sag16s_upload import DataSet


class RegisterHTML():
    
    _success = {}
    _failed = {}
    
    def __init__(self, successDict, failedDict):
        self._success = successDict
        self._failed = failedDict
    
    def printStart(self):
        print 'Content-type: text/html\n'
        print '<html>'
        print ' <title>RQC Sag16s Registration Status</title>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.banner.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.html_util.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.menubar.js"></script>'
        print ' <script type="text/javascript" src="/resources/scripts/jgi/rqc/jgi.rqc.tab.js"></script>'
        
    def printEnd(self):
        print '</html>'
        
    def printBody(self):
        #print ' <body>'
        print ' <table id="banner"></table>'
        print ' <font color="green"> <h3>Successfully Registered 16s Sequences: </h3> </font>'
        print ' <table name="successTable" id="successTable"  border="2" cellpadding="2">'
        sKeys = self._success.keys()
        for sKey in sKeys:
            print '   <tr>'
            print '    <td>'+sKey+'</td><td bgcolor="green">'+self._success.get(sKey)+'</td>'
            print '   </tr>'
        print ' </table><br>'
        
        print ' <font color="red"> <h3>Failed Registeration 16s Sequences: </h3> </font>'
        print ' <table name="failedTable" id="failedTable"  border="2" cellpadding="2">'
        fKeys = self._failed.keys()
        for fKey in fKeys:
            print '   <tr>'
            print '    <td>'+fKey+'</td><td bgcolor="red">'+self._failed.get(fKey)+'</td>'
            print '   </tr>'
        print ' </table>'
        #print ' </body>'
        
    
    def printHTML(self):
        self.printStart()
        self.printBody()
        self.printEnd()
        


def makeDataSet(fileList, logger):
    '''
    This method actually makes the DataSet objects which are for each 
    type of DataSet:
        - New DataSet
        - Duplicate DataSet
        - MissingPHD DataSet
        - UnKnown DataSet
    '''
    dsMap = dict()
    # For each file in file list, organize the data
    for fileName in fileList:
        logger.debug("Processing file :"+fileName)
        ext = fileName[-4:]
        ext1 = fileName[-6:]
        pattern = None
        file_type = None
        # get the pattern and file type
        if(ext == ".seq"):
            pattern = fileName[:-4]
            file_type = DataSet.FILE_TYPE_SEQ
        elif(ext == ".ab1"):
            pattern = fileName[:-4]
            file_type = DataSet.FILE_TYPE_AB1
        elif(ext1 == ".phd.1"):
            pattern = fileName[:-6]
            file_type = DataSet.FILE_TYPE_PHD
        # Create or add to the data set
        if(dsMap.has_key(pattern)):
            dsMap.get(pattern).setData(fileName, file_type)
        else:
            dsMap[pattern] = DataSet(fileName, file_type)
    if(dsMap.has_key(None)): dsMap.pop(None)
    logger.debug("Done with creating the Map. Count="+str(len(dsMap)))
    # not all data sets have a PHD file
    return dsMap


def registerData(aMap, staging_dir, prop, userName, pcrPlateId, runType, logger):
    patternList = aMap.keys()
    logger.debug("Actual registration process started.")
    successDict = dict()
    failedDict = dict()
    for pattern in patternList:
        # get the data set with this pattern
        ds = aMap.get(pattern)
        phdFile = ds.getPhd()
        ab1File = ds.getAB1()
        seqFile = ds.getSeq()
        pattern = phdFile[:-6]
        if(ab1File == None):
            ab1File = staging_dir+pattern+".ab1"
            open(ab1File, "w")
        else:
            ab1File = staging_dir+ab1File
        if(seqFile == None):
            seqFile = staging_dir+pattern+".seq"
            open(seqFile, "w")
        else:
            seqFile = staging_dir+seqFile
        phdFile = staging_dir+phdFile
        propObj = Properties()
        cmd = (propObj.getFSSag16sSoftware()+"/sag16sSync.py  -p  "+
               phdFile+"  -a  "+ab1File+"  -s  "+seqFile+" -u  '"+userName+"'  -r  "+runType +" -i "+pcrPlateId )
        try:
            logger.debug("Command: "+cmd)
            outputTuple = commands.getstatusoutput(cmd)
        except:
            logger.error("Command FAILED. Cmd: "+cmd)
            outputTuple = (-1, "Failed")
        status = outputTuple[0]
        output = outputTuple[1]
        if( status == 0):
            successDict[pattern] = "Successfully Registered"
            logger.debug("SUCCESSFULLY Registered (phd.1, ab1, seq files) for pattern: "+pattern)
            logger.debug("Sync command SUCCESSFULLY completed.")
        else:
            failedDict[pattern] = "Failed Registration"
            logger.error("FAILED to register (phd.1, ab1, seq files) for pattern: "+pattern)
            logger.error("Sync command FAILED.(message: "+output+")")
        logger.debug("Actual registration process function done.")
    return [successDict, failedDict]



if __name__ == '__main__':
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("registerSAG16s")
    logger.debug("Starting the registration process.")
    prop = Properties()
    form = cgi.FieldStorage()
    stagingDir = form.getvalue('stagingDir')
    userName = form.getvalue('userName')
    pcrPlateId = form.getvalue('pcrPlateId')
    runType = form.getvalue('runType')
    fileList = form.getlist('fileName')
    logger.debug(pcrPlateId)
    logger.debug(runType) 
    if not isinstance(fileList, list):
        fileList = [fileList]
    if not isinstance(stagingDir, str):
        stagingDir = str(stagingDir)
    if not isinstance(pcrPlateId, str):
        pcrPlateId = str(pcrPlateId)
    if not isinstance(runType, str):
        runType = str(runType)
    logger.debug(stagingDir)
    logger.debug(fileList)
    dsMap = makeDataSet(fileList, logger)
    logger.debug("Map Count="+str(len(dsMap)))
    result = registerData( dsMap, stagingDir, prop, userName, pcrPlateId, runType, logger )
    logger.debug("Done with the registration process.")
    rHTML = RegisterHTML(result[0], result[1] )
    rHTML.printHTML()
    
