#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python


import cgi, time, sys, MySQLdb, re

from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties
from core.utils.rqcdb import RQCdb
import core.utils.commons


def printUsage():
    pass


def getSearchSequences(mProposal, mProject, mSample, mPCRPlate, mSortWindow, mWellType, mUserName, logger):
    sag16sList = list()
    sqlSearchPhrase = ''
    searchStr = ''
    if mProject  is not None and mProject is not '':
        mProject = mProject.strip()
        sqlSearchPhrase = sqlSearchPhrase+' AND project LIKE "%'+mProject+'%" '
        searchStr = searchStr + 'project='+str(mProject)
    if mPCRPlate  is not None and mPCRPlate  is not '':
        mPCRPlate = mPCRPlate.strip()
        sqlSearchPhrase = sqlSearchPhrase+' AND pcr_plate LIKE "%'+mPCRPlate+'%"'
        searchStr = searchStr + 'pcrPlate='+str(mPCRPlate)
    if mProposal  is not None and mProposal  is not '':
        mProposal = mProposal.strip()
        sqlSearchPhrase = sqlSearchPhrase+' AND proposal_id LIKE "%'+mProposal+'%"'
        searchStr = searchStr + 'proposal='+str(mProposal)
    if mSample  is not None and mSample  is not '':
        mSample = mSample.strip()
        sqlSearchPhrase = sqlSearchPhrase+'  AND sample LIKE "%'+mSample+'%"'
        searchStr = searchStr + 'sample='+str(mSample)
    if mUserName  is not None and mUserName  is not '':
        mUserName = mUserName.strip()
        sqlSearchPhrase = sqlSearchPhrase+'  AND user_name LIKE "%'+mUserName+'%"'
        searchStr = searchStr + 'searchUserName='+str(mUserName)
    if mWellType != None and mWellType != '':
        mWellType = mWellType.strip()
        sqlSearchPhrase = sqlSearchPhrase+'  AND well_type LIKE "%'+mWellType+'%"'
        searchStr = searchStr + '&well_type='+str(mWellType)
    if mSortWindow != None and mSortWindow != '':
        mSortWindow = mSortWindow.strip()
        sqlSearchPhrase = sqlSearchPhrase+'  AND sort_window LIKE "%'+mSortWindow+'%"'
        searchStr = searchStr + '&sort_window='+str(mSortWindow)

    logger.debug("Retrieving the Sag16s TrimSeqence Info. SearchString: "+searchStr)
    try:
        dbObj = RQCdb()
        dbObj.connect()
        sql = 'SELECT seq_name, q15_tlength, fs_location FROM sag16s_seq WHERE (ss_status_cv_id=10) '+sqlSearchPhrase
        logger.debug("SQL to retrieve all Hits: "+ sql)
        dbObj.execute(sql)
        result = dbObj.fetchall()
        for row in result:
            hit = dict()
            hit['seq_name'] = row[0]
            hit['q15_tlength'] = row[1]
            hit['fs_location'] = row[2]
            sag16sList.append(hit)
    except:
        logger.error("Some error has occured while processing the Sag16s TrimSeq sequence info retrieval")
        return []
    logger.debug("Processing completed and retrieved the Trimmed Sequence unit info.")
    return (sag16sList, searchStr)




def getAllTrimmedFasta(sag16sList, searchStr, logger):
    #
    # build the file name to be downloaded
    ltime = time.localtime()
    tStamp = str(ltime.tm_year)+"_"+str(ltime.tm_mon)+"_"+str(ltime.tm_sec)+"_"
    fileName = tStamp+"Trimmed_Fasta_"+searchStr+".fasta"
    print "Content-Disposition: attachment; filename="+fileName    
    print "Content-type: application/octet-stream\n"
    trimSeqSting = ''
    for sag16s in sag16sList:
        logger.debug(sag16s)
        seq_name = sag16s.get('seq_name')
        q15_tlen = sag16s.get('q15_tlength')
        fs_loc   = sag16s.get('fs_location')
        # If the trim length is '0' dont print out stuff
        if q15_tlen == 0:
            logger.warning("The sequence trim length is '0' so skipping this")
            continue 
        #
        # get the location of the Trimmed fasta
        fastaLoc = fs_loc+"/"+core.utils.commons.getSag16sTrimFaName(seq_name)
        try:
            fp = open(fastaLoc)
            name = ''
            seq = []
            #
            # Do not print the lengthy top hit name. instead print out the 
            # sequence name which is more comprehensible by the analysts.
            fp.readline().rstrip()
            print ">"+seq_name
            #
            # Print the FASTA sequence
            for line in fp:
                line = line.rstrip()
                print line
            f.close()
        except:
            logger.error("Error reading the file: "+fastaLoc)
    
        


def main():
    logObj  = RQCLogger()
    logger  = logObj.getSag16sLogger("downloadTrimSeq")
    logger.debug("Starting the DownloadTrimSeq WebService")
    form = cgi.FieldStorage()
    logger.debug("Starting the analysis search process.")
    mProposal = form.getvalue('proposalId')
    mProject = form.getvalue('project')
    mSample = form.getvalue('sample')
    mPCRPlate = form.getvalue('pcrPlateId')
    mSortWindow = form.getvalue('sWindow')
    mWellType = form.getvalue('wType')
    mUserName = form.getvalue('searchUserName')
    
    #
    # get the list of sequence unit Info
    (seqInfoList, searchStr) = getSearchSequences(mProposal, mProject, mSample, mPCRPlate, mSortWindow, mWellType, mUserName, logger)
    getAllTrimmedFasta(seqInfoList, searchStr, logger)
        
        
if __name__ == '__main__':
    main()
