#!/house/homedirs/q/qc_user/Python/Versions/2.7/bin/python

'''
Created on Jan 11, 2013

@author: asyed
'''

import cgi
import cgitb; cgitb.enable()
from core.utils.rqc_logger import RQCLogger
from core.utils.properties import Properties


def printHTML(replaceDict, logger):
    print 'Content-type: text/html\n'
    fh = open("sag16s_analysis.html", "r")
    for line in fh:
        line.rstrip()
        for key in replaceDict.keys():
            line = line.replace(key, replaceDict.get(key))
        print line.rstrip()


if __name__ == '__main__':
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("analysisSAG16s")
    prop = Properties()
    form = cgi.FieldStorage()
    logger.debug("Starting the analysis search process.")
    mProject = form.getvalue('project')
    mSample = form.getvalue('sample')
    mProposal = form.getvalue('proposalId')
    mSearchUserName = form.getvalue('searchUserName')
    mPCRPlateId = form.getvalue('pcrPlateId')
    mSortWindow = form.getvalue('sWindow')
    mWellType = form.getvalue('wType')
    
    searchStr=''
    dispStr = '(Searching for: '
    if mProject != None and mProject != '':
        mProject = mProject.strip()
        searchStr = searchStr + '&project='+str(mProject)
        dispStr = dispStr+' Project: '+str(mProject)
    if mSample != None and mSample != '':
        mSample = mSample.strip()
        searchStr = searchStr + '&sample='+str(mSample)
        dispStr = dispStr+' Sample: '+str(mSample)
    if mProposal != None and mProposal != '':
        mProposal = mProposal.strip()
        searchStr = searchStr + '&proposalId='+str(mProposal)
        dispStr = dispStr+' Proposal: '+str(mProposal)
    if mSearchUserName != None and mSearchUserName != '':
        mSearchUserName = mSearchUserName.strip()
        searchStr = searchStr + '&searchUserName='+str(mSearchUserName)
        dispStr = dispStr+' Proposal: '+str(mSearchUserName)
    if mWellType != None and mWellType != '':
        mWellType = mWellType.strip()
        searchStr = searchStr + '&wType='+str(mWellType)
        dispStr = dispStr+' WellType: '+str(mWellType)
    if mPCRPlateId != None and mPCRPlateId != '':
        mPCRPlateId = mPCRPlateId.strip()
        searchStr = searchStr + '&pcrPlateId='+str(mPCRPlateId)
        dispStr = dispStr+' PCRPlate: '+str(mPCRPlateId)
    if mSortWindow != None and mSortWindow != '':
        mSortWindow = mSortWindow.strip()
        searchStr = searchStr + '&sWindow='+str(mSortWindow)
        dispStr = dispStr+' Sample: '+str(mSortWindow)
    dispStr = dispStr+')'
    
    #
    # Strings that are used for replacing from the HTML code
    replaceDict = dict()
    replaceDict['{TITLE}'] = "RQC SAG16S Analysis Search Results "+dispStr
    replaceDict['{URL}'] = "/cgi-bin/getSag16sSearch.py?"+searchStr
    replaceDict['{SAG_HOME_URL}'] = "/sag16s.html"
    replaceDict['{JIRA_URL}'] = "http://issues.jgi-psf.org/secure/Dashboard.jspa"
    replaceDict['{DOWNLOAD_CSV_URL}'] = "/cgi-bin/downloadSearchCSV.py?"+searchStr
    replaceDict['{DOWNLOAD_FASTA_URL}'] = "/cgi-bin/downloadTrimSeq.py?"+searchStr
    printHTML(replaceDict, logger)
