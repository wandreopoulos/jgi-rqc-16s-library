'''
Created on Dec 9, 2012

This is the main SAG16s Analysis pipleine, running in the backend. Checks if newly generated data
is available, and if available subsequently performs the BLAST 16s, BLAST 16s contam, and RDP.

REQUIRES:
    SQL Alchemy Installed
    Reference to BLAST tool
    Reference to RDP tool
    16s blast database
    16s contam blast database
    
@author: asyed

'''

from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from core.dao.sag16s_seq import SAGSequence, SAGSequenceStatusCV, SAGSequenceHist
from sqlalchemy.exc import IntegrityError, DatabaseError, SQLAlchemyError, InvalidRequestError
from core.utils.rqc_logger import RQCLogger, Properties
import core.utils.commons
from core.tools.blast import BlastNTool
from core.tools.rdp import RDPTool
from core.parsers.blast import BlastTopHitParser
from core.parsers.rdp import RDPParser
from core.dao.sag16s_top_hit import SAGTopHit
from core.utils.rqc_16s_constants import SAG16sConstants
import sys, commands, re


'''
Get list of records by status:
    This retrieves a list of Sag16s_Seq records by status ID, Status Id is provided as input
    
@param status_id: Status ID for the records to be retrieved
@param logger: Logging object
@return : returns a list of records with this status Id

'''
def getRecordsByStatus(status_id, logger):
    records = []
    session=None
    try:
        logger.info("Retrieving records by status: "+str(status_id))
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Please make sure only the sequences with length more than 100 basepairs are selected for the analysis
        records = session.query(SAGSequence).filter(SAGSequence.ss_status_cv_id == status_id,SAGSequence.q15_tlength>100).all()
    except DatabaseError as er:
        print "Exception ",er
    except SQLAlchemyError as er:
        print "Exception ",er
    except InvalidRequestError as er:
        print "Exception ",er
    except:
        logger.error("Error checking the physical run name")
    finally:
        if(session != None):
            session.close()
    return records


'''
During processing various stages, this sets the status of the Sag16s Sequence

@param prefix: sag16s_seq_name (actual sequence name pattern stored in the RQC database)
@param status_id: The status ID to be set for this sequence name
@param logger: logging object
@return Returns True for a successfully updated status, otherwise returns False
'''
def setSeqStatus(prefix, status_id, logger):
    session=None    
    try:
        logger.info("Setting status: "+str(status_id)+" for seq_name"+prefix)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        session.query(SAGSequence).filter_by(seq_name=prefix).update({"ss_status_cv_id": status_id})
        session.commit()
        seqhist = SAGSequenceHist(prefix,
                            status_id,
                            datetime.now(),
                            datetime.now())
        session.add(seqhist)
        session.commit()
    except IntegrityError, e:
        session.rollback()
        session.close()
        logger.error("Failed updated Status ID "+str(status_id)+" for sequence: "+prefix)
        print("Integrity error! %s", e)
        return False
    except:
        session.rollback()
        session.close()
        logger.error("Failed updated Status ID "+str(status_id)+" for sequence: "+prefix)
        logger.error("An unknown exception occurred!")
        return False
    finally:
        if(session != None):
            session.close()
    logger.debug("Successfully updated Status ID "+str(status_id)+" for sequence: "+prefix)
    return True

'''
Runs an instance of Blast against SAG16S database. Specifically green gene database
    1. Pull all the records that are of status ready
    2. For each record, run BLAST over SAG16S database
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: BLAST output is stored in the standard location as PATH_TO_seq_name_prefix/sequence_name_prefix.blast
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
          returns False
'''
def runSag16sBlastTask(logger):
    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_REGISTERED_STATUS_ID, logger)    
    for seq in sag16sList :
        # Set the status to in progress
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastName(seq.seq_name)
        db = SAG16sConstants.FS_GREEN_GENE_BLAST_DB
        # debugging
        logger.debug("BLAST FA: "+faSeq+" Out: "+outFile+" DB "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        if not blastTool.run():
            logger.error("Unable to run the BLAST hit for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
            return False
        else:
            logger.debug("Parsing the BLAST ggDB output")
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_HOMOLOGY, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST  hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                phylogeny = getPhylogeny(outFile, logger)
                if bParser.load(phylogeny):
                    logger.debug("Successfully loaded BLAST  hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST  hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID, logger)
                    return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no data retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
                
    # You are here because parsing for Blast successfully completed
    return True


'''
Runs an instance of Blast against SAG16S Contamination database.
    1. Pull all the records that are of status BLAST 16s completed
    2. For each record, run BLAST over SAG16S Contamination database
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: BLAST output is stored in the standard location PATH_TO_seq_name_prefix/sequence_name_prefix.blast
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
        returns False
'''
def runSag16sBlastContamTask(logger):
    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID, logger)
    for seq in sag16sList :
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_IN_PROGRESS_STATUS_ID, logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastContamName(seq.seq_name)
        db = SAG16sConstants.FS_SAG16S_CONTAM_BLAST_DB
        # debugging
        logger.debug("Calling BLAST contam tool with FASTA: "+faSeq+" Out: "+outFile+" DB: "+db)
        blastTool = BlastNTool(faSeq, outFile, db)
        blastTool.setPI(95)
        if not blastTool.run():
            logger.error("Unable to run the BLAST Contam hit for file : "+faSeq)
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
            #return False
        else:
            logger.debug("Parsing the BLAST contam16s output")
            bParser = BlastTopHitParser(outFile, SAGTopHit.ANALYSIS_TYPE_CONTAM, seq.sag_seq_id)
            if not bParser.parse():
                logger.error("Unable to parse the BLAST Contam hit for file : "+outFile)
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
                #return False
            parseCode = bParser.getParseCode()
            if(parseCode == BlastTopHitParser.SUCCESSFULLY_PARSED):
                if bParser.load(None):
                    logger.debug("Successfully loaded BLAST contam hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
                else:
                    logger.error("Unable to load the BLAST Contam hit for file : "+outFile)
                    setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_FAILED_STATUS_ID, logger)
                    #return False
            if( parseCode == BlastTopHitParser.NOTHING_TO_PARSE ):
                logger.debug("There was no data retrieved to parse")
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
                
    # return true
    return True


'''
Runs RDP classifier on sequences that successfully completed BLAST on 16S contam database.
    1. Pull all the records that are of status BLAST 16s CONTAM completed
    2. For each record, run RDP classifier
    3. Record the SUCCESS/FAILED status into the DATABASE
    NOTE: RDP output is stored in the standard location PATH_TO_seq_name_prefix/sequence_name_prefix.rdp
@param logger: Logging object
@return : Returns true for a successful run-event (could contain multiple BLAST events), otherwise
        returns False
'''
def runSag16sRDPTask(logger):
    sag16sList = getRecordsByStatus(SAGSequenceStatusCV.SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID, logger)
    logger.debug("Processing RDP task for: %s" %(len(sag16sList)))
    for seq in sag16sList :
        setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_IN_PROGRESS_STATUS_ID,logger)
        faSeq = seq.fs_location+"/"+core.utils.commons.getSag16sFaName(seq.seq_name)
        outFile = seq.fs_location+"/"+core.utils.commons.getSag16sBlastRDPName(seq.seq_name)
        rdpTool = RDPTool(faSeq, outFile)
        if not rdpTool.run():
            logger.error("Failed to run the RDP tool.")
            setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
            return False
        else:
            rParser = RDPParser(outFile, seq.sag_seq_id)
            if not rParser.parse():
                logger.error("Unable to parse the RDP file : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
                return False
            
            if rParser.load():
                logger.debug("Successfully loaded RDP hit for : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_COMPLETE_STATUS_ID, logger)
            else:
                logger.error("Unable to load the RDP hit for file : %s"%(outFile))
                setSeqStatus(seq.seq_name, SAGSequenceStatusCV.SEQ_RDP_FAILED_STATUS_ID, logger)
                return False
    # return true
    return True


'''
Creates a TopHit FASTA sequence Id and FASTA sequence
@param seq_name: 16S sequence name for which FASTA is to be created
@param fasta_file_name: FASTA file name for the top hit FASTA
@param logger: Logging object
@return: returns True for successfully created FASTA, otherwise returns False
'''
def createTopHitFasta(seq_name, fasta_file_name, logger):
    allFasta = SAG16sConstants.FS_GREEN_GENE_FASTA
    hitName = None
    try:
        logger.info("Retrieving top hit record for query_name: "+seq_name)
        Session = sessionmaker(bind = ORMBase.globalEngine)
        session = Session()
        # Update the status in the database
        records = session.query(SAGTopHit).filter(SAGTopHit.query_name == seq_name).all()
        rowResult = records[0]
        hitName = rowResult.subject_id
    except:
        logger.error("Error occurred while retrieving the top_hit name")
        return False
    # Now the hitName is populated with the BLAST Top Hit
    try:
        f = open(fasta_file_name,'w')
        with open(allFasta) as fp:
            for name, seq in read_fasta(fp):
                if( name == '>'+hitName ):
                    f.write(name+'\n')
                    f.write(seq)
        f.close()
    except:
        logger.error("TopHit FASTA file not successfully created")
        return False
    return True


'''
A sequence iterator to get the FASTA of interest
@param fp: file pointer
@return: A tuple (SequenceId, FASTASequence)
'''
def read_fasta(fp):
    name, seq = None, []
    for line in fp:
        line = line.rstrip()
        if line.startswith(">"):
            if name: yield (name, ''.join(seq))
            name, seq = line, []
        else:
            seq.append(line)
    if name: yield (name, ''.join(seq))


'''
Run the Phylogeny lookup module and return the Phylogeny information for the 
BLAST top-hit.

@param blastOutFile : BLAST output file, to pull out the Blast hit name
@param logger: logging object
@return : Returns the phylogeny information for the top hit
'''
def getPhylogeny(blastOutFile, logger):
    phylogeny = None
    subject_id = None
    try:
        f = file(blastOutFile, "r")
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        blastop = f.readline()
        ary = blastop.split()
        subject_id = ary[1]
    except:
        logger.error("TopHit output is not successfully parsed to find the Phylogeny")
        return None
    # extract anything after 593322_FN421897.1_ which will be proteogenomics_physiology_phyllosphere_clover_clone_2_H11_k_Bacteria;_p_Proteobacteria;_c_Betaproteobacteria;_o_Burkholderiales;_f_Comamonadaceae;_g_Variovorax;_s_Variovorax_paradoxus;_otu_3057
    logger.debug("Matching the pattern Subject: "+subject_id)
    m = re.search('\.1_(.*)',subject_id)
    pattern = None
    if m != None:
        pattern = m.group(1)
    else:
        try:
            # This needs to be done to extract a useable hit name, otherwise, we end up with absolutely no hits
            subject_id = subject_id.replace("'","")
            regexCmd = "echo '"+subject_id+"' | perl -nle 's/;\w_$//;s/.+;[\w]_//; print'"
            output = commands.getstatusoutput(regexCmd)
            pattern = output[1]
        except:
            logger.error("Error while matching [perl regex] for the pattern Subject: "+subject_id)
            return None

    if pattern == None or pattern == '' or pattern == ' ':
        logger.error("Nothing matching group[1] for the pattern Subject: "+subject_id)
        return None
    # replace the single quotes so they don't interfere with the command line single quotes
    pattern = pattern.replace("'","") # avoid unwanted termination of the command line string
    pattern = pattern.replace("-","_") # can pretend to be a command line option, so replace the - with _
    phyCmd = SAG16sConstants.TAXA_LOOKUP_CMD+" -n '%s'"%(pattern)
    try:
        outputTuple = commands.getstatusoutput(phyCmd)
    except:
        logger.error("Phylogeny command was not successfully performed. Cmd: "+phyCmd)
        return None
    #
    # status and output
    status = outputTuple[0]
    output = outputTuple[1]
    if( status == 0):
        if "find kingdom" in output or "unknown":
            output = ''
        logger.debug("Phylogeny command SUCCESSFULLY completed. Cmd: "+phyCmd)
        logger.debug("Phylogeny for topHit :"+output)
        return output
    else:
        logger.error("Phylogeny command FAILED with status: "+str(status))
        return None
    # should never be here, but however
    return None
    

#
# Main program that will run when this script is executed from the command line
#
if __name__ == '__main__':
    prop    = Properties()
    logObj  = RQCLogger()
    logger = logObj.getSag16sLogger("Sag16sPipeline")
    # Run Blast on 16s and 16sContam DBs
    if not runSag16sBlastTask(logger):
        logger.error("FAILED processing the BLASTNTool on ggDB, so exiting")
        sys.exit(1)
        
    if not runSag16sBlastContamTask(logger):
        logger.error("FAILED processing the BLASTNTool on Contam16s, so exiting")
        sys.exit(1)
        
    # Run RDP 
    if not runSag16sRDPTask(logger):
        logger.error("FAILED processing the RDP, so exiting")
        sys.exit(1)
        
    sys.exit(0)
    # clean up
    
