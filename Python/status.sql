-- MySQL dump 10.11
--
-- Host: trace    Database: asyed_rqc
-- ------------------------------------------------------
-- Server version	5.1.42-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sag16s_seq_status_cv`
--


--
-- Dumping data for table `sag16s_seq_status_cv`
--

LOCK TABLES `sag16s_seq_status_cv` WRITE;
/*!40000 ALTER TABLE `sag16s_seq_status_cv` DISABLE KEYS */;
INSERT INTO `sag16s_seq_status_cv` VALUES (1,'SAG16s_Seq_Registered','Sequence is registered successfully'),(2,'SAG16s_Seq_Homology_In_Progress','BLAST Homology search in progress'),(3,'SAG16s_Seq_Homology_Failed','BLAST Homology search failed'),(4,'SAG16s_Seq_Homology_Complete','BLAST Homology search successfully completed'),(5,'SAG16s_Seq_Contam_In_Progress','BLAST 16s Contam search in progress'),(6,'SAG16s_Seq_Contam_Failed','BLAST 16s Contam search failed'),(7,'SAG16s_Seq_Contam_Complete','BLAST 16s Contam search successfully completed'),(8,'SAG16s_Seq_RDP_In_Progress','BLAST 16s RDP classifier in progress'),(9,'SAG16s_Seq_RDP_Failed','BLAST 16s RDP classifier failed'),(10,'SAG16s_Seq_RDP_Complete','BLAST 16s RDP classifier successfully completed'),(11,'SAG16s_Seq_Finalized','Finalized the 16s pipeline processing'),(12,'ON_HOLD','Holding from running');
/*!40000 ALTER TABLE `sag16s_seq_status_cv` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-05 19:40:46
