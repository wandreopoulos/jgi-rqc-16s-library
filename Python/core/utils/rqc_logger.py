"""
$Source: /repository/rqc/Python/core/utils/Attic/rqc_logger.py,v $
$Revision: 1.1.4.7 $ $Date: 2013-01-15 02:55:52 $
$Author: asyed $

This is a central logger module which will provide various loggers for various pipeline. This is always a
singleton, and creates all corresponding loggers through the singleton call. Multiple calls to the logger
class will use the same instance, however could create channels with respective formatting through a
LoggerAdapter object. This provides customized logging capabilities for calling clients.

@author Aijaz Syed
@contact ASyed@lbl.gov

"""

import logging
from core.utils.properties import Properties


class RQCLogger(object):
    
    _instance = None
    _sag16_logger = None
    _assemblyQC_logger = None
    _alignmentQC_logger = None
    _readQC_logger = None
    _jigsaw_logger = None
    _reseq_logger = None
    _generic_logger = None
    
    _sag16s_logfile  = "sag16s.log"
    _readQC_logfile  = "readQC.log"
    _generic_logfile = "generic.log"
    _jigsaw_logfile  = "jigsaw.log"
    _reseq_logfile   = "reseq.log"
    _alignment_logfile = "alignment.log"
    _assembly_logfile  = "assembly.log"
     
    '''
    Constructor for the RQCLogger
    '''
    def __init__(self):
        #print "Object initialization completed"
        pass
    
    
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(RQCLogger, cls).__new__(cls, *args, **kwargs)
            prop = Properties()
            cls._properties = prop
            cls._generic_logfile = prop.getLogFile()
            cls._readQC_logfile = prop.getReadQCLogFile()
            cls._sag16s_logfile = prop.getSag16sLogFile()
            cls._alignment_logfile = prop.getAlignmentQCLogFile()
            cls._assembly_logfile  = prop.getAssemblyQCLogFile()
            cls._jigsaw_logfile    = prop.getJigsawLogFile()
            cls._reseq_logfile     = prop.getReseqLogFile()
            cls._sag16_logger      = _getSag16sLogger(RQCLogger,"SAG16s")
            cls._readQC_logger     = _getReadQCLogger(RQCLogger, "ReadQC")
            cls._generic_logger    = _getGenericLogger(RQCLogger, "Generic")
            cls._alignmentQC_logger= _getAlignmentLogger(RQCLogger, "AlignmentQC")
            cls._assemblyQC_logger = _getAssemblyLogger(RQCLogger, "AssemblyQC")
            cls._jigsaw_logger     = _getJigsawLogger(RQCLogger, "Jigsaw")
            cls._reseq_logger      = _getReseqLogger(RQCLogger, "ReSeqQC")
        return cls._instance

    def getSag16sLogger(self, scriptName):
        loggerSag = logging.LoggerAdapter(self._sag16_logger, {'scriptID':scriptName})
        return loggerSag

    def getGenericLogger(self, scriptName=None):
        loggerGen = logging.LoggerAdapter(self._generic_logger, {'scriptID':scriptName})
        return loggerGen

    def getReadQCLogger(self, scriptName):
        loggerSag = logging.LoggerAdapter(self._readQC_logger, {'scriptID':scriptName})
        return loggerSag

    def getReSeqLogger(self, scriptName=None):
        loggerGen = logging.LoggerAdapter(self._reseq_logger, {'scriptID':scriptName})
        return loggerGen

    def getAlignmentQCLogger(self, scriptName):
        loggerSag = logging.LoggerAdapter(self._alignmentQC_logger, {'scriptID':scriptName})
        return loggerSag

    def getAssemblyQCLogger(self, scriptName=None):
        loggerGen = logging.LoggerAdapter(self._assemblyQC_logger, {'scriptID':scriptName})
        return loggerGen

    def getJigsawLogger(self, scriptName=None):
        loggerGen = logging.LoggerAdapter(self._jigsaw_logger, {'scriptID':scriptName})
        return loggerGen

'''
Creates a ReadQC logging object.

Note: NEVER call this from 
'''
def _getReadQCLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._readQC_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj
    
def _getGenericLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._generic_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj
    
def _getSag16sLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._sag16s_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj
    
def _getAlignmentLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._alignment_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj
    
def _getAssemblyLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._assembly_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj
    
def _getJigsawLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._jigsaw_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj

def _getReseqLogger(self, name):
    loggerObj = logging.getLogger(name)
    handler = logging.FileHandler( self._reseq_logfile )
    formatter = logging.Formatter(name+':  %(asctime)s %(levelname)s Script:%(scriptID)-15s %(message)s' )
    handler.setFormatter(formatter)
    loggerObj.addHandler(handler)
    loggerObj.setLevel( "DEBUG" )
    # Now return the Logging object
    return loggerObj

if(__name__=="__main__"):
    logger = RQCLogger()
    sagLogger = logger.getSag16sLogger("Script-1")
    genericLogger = logger.getGenericLogger()
    sagLogger.debug("This is testing sag")
    genericLogger.debug("This is test from generic")
    sagLogger1 = logger.getSag16sLogger("Script-2")
    sagLogger1.debug("From saglogger2")
    sagLogger.debug("From 1")
    reseqLogger = logger.getReSeqLogger("From getPendingJobs")
    reseqLogger.error("ReSeq logger ERROR")
    alignmentLogger = logger.getAlignmentQCLogger("RNA Seq Alignment")
    alignmentLogger.warning("Warning message entered")
    assemblyLogger = logger.getAssemblyQCLogger("AssemblyQC Logger")
    assemblyLogger.critical("CRITICAL MESSAGE, memory issue")
    jigsawLogger = logger.getJigsawLogger("Jigsaw logger")
    jigsawLogger.info("No more issues with Jigsaw!!")
    readLogger = logger.getReadQCLogger("ReadQC")
    readLogger.debug("ReadQC can be memory intensive")
