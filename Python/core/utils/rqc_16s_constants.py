
class SAG16sConstants(object):
    ###This is not used:
    FS_GREEN_GENE_BLAST_DB="/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/gg16s/ggDB"
    FS_SILVA_BLAST_DB="/global/dna/shared/rqc/ref_databases/silva/20170418/LSSURef_tax_silva"
    ###"/global/dna/shared/rqc/ref_databases/misc/CURRENT/LSSURef_115_tax_silva.fasta"
    ###"/global/projectb/scratch/andreopo/LSSUSilva/LSSURef_115_tax_silva.fasta"
    ###/global/dna/shared/rqc/ref_databases/misc/CURRENT/LSSURef_115_tax_silva.fasta"
    FS_NT_BLAST_DB="/global/dna/shared/rqc/ref_databases/ncbi/20171101/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted"
    ###"/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted"
    ###"/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/nt/nt.headerformat4blast"
    FS_SAG16S_CONTAM_BLAST_DB = "/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/contam16s/contam16s"
    ###This is not used:
    FS_GREEN_GENE_FASTA = "/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/gg16s/greengenes_corrected_synth_dna_chloro_mito_euk.MOD.fasta"
    FS_IMG_BLAST_DB = "/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/img/11102017/isolate16s.fna"
    ### "/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/img/microbe.img.16s.public.fa"
    ### "/global/dna/shared/rqc/sag16s/repo/sag16s/blastdb/img/microbe.img.5s.16s.23s.jgiinternalonly"
    
    PHD2FASTA_CMD = "phd2fasta "
    JAZZ_TRIM_CMD = "jazz_trim "
    TAXA_LOOKUP_CMD = "/global/projectb/sandbox/rqc/andreopo/deploy/sag16s/prod/misc/software/taxdb/whatdatax.pl "
    BLASTN_CMD = "blastall  -p blastn "
    BLASTNEW_CMD = "module load jgi-rqc ; run_blastplus.py " ###"module load jigsaw ; run_blast.pl "
    RDP_CMD = "/global/projectb/sandbox/rqc/andreopo/deploy/sag16s/prod/misc/software/rdp/rdp_classifier-2.3.jar -t /global/dna/shared/rqc/sag16s/repo/sag16s/rdp/rRNAClassifier.properties "
    MOTHUR_CLASSIFY_SEQS = "/global/projectb/sandbox/rqc/andreopo/deploy/sag16s/prod/misc/software/mothur/mothur "

