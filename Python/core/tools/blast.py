'''
Created on Nov 25, 2012

@author: asyed
'''

from core.utils.rqc_logger import RQCLogger, Properties
import os
import commands
from core.utils.rqc_16s_constants import SAG16sConstants



class BlastNTool(object):
    '''
    classdocs
    '''
    _input = None
    _output = None
    _blast_db = None
    _prop  = None
    _logger = None
    _pi = 90
    

    def __init__(self, inFile, outFile, db):
        '''
        Constructor
        '''
        self._input = inFile
        self._output = outFile
        self._blast_db = db
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("BlastNTool")
        
    def run(self):
        if (self._input == None or self._output== None or self._blast_db == None):
            self._logger.error("Input file or output file is not specified, so nothing done")
            return False
        
        if (not os.path.exists(self._input)):
            self._logger.error("Input file specified does not exist in the path. Nothing done. (file: "+self._input+")")
            return False
        
        '''
        try:
            f = open(self._output, "w")
            f.close()
        except Exception as e:
            self._logger.error("Output file is not writable. Nothing done. (file: "+self._output+")")
            return False
        '''
        
        #
        # make BLAST command
        blastn_cmd = (SAG16sConstants.BLASTNEW_CMD +  
                      ###" -p \"-outfmt 9 -perc_identity 97 -evalue 1e-30 -dust 'yes' -num_threads 8\" "+
                      " -d "+self._blast_db+
                      " -o "+self._output+
                      " -q "+self._input + " -t 8 -s")
        
        outputTuple = None
        try:
            #
            # run BLAST command and get status and output
            outputTuple = commands.getstatusoutput(blastn_cmd)
        except Exception as e:
            self._logger.error("Command FAILED. Cmd: "+blastn_cmd)
            outputTuple = (-1, "Failed")
            
        self._logger.debug("Command: "+blastn_cmd)
        
        #
        # status and output
        status = outputTuple[0]
        output = outputTuple[1]
        
        if( status == 0):
            self._logger.debug("Blastn command SUCCESSFULLY completed.")
            return True
        else:
            self._logger.error("Blastn command FAILED.")
            return False
            
        
    def setPI(self, percentId):
        self._pi = percentId
        
    def setQueryFile(self, queryFile):
        self._input = queryFile
        
    def setOutputFile(self, outFile):
        self._output = outFile
        
    def setBlastDB(self, db):
        self._blast_db = db    
        
        
        
class BlastPTool(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def run(self):
        pass
    
    

class BlastXTool(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def run(self):
        pass
    
    
class TBlastNTool(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
    def run(self):
        pass
