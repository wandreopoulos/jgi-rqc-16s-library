'''
Created on Nov 25, 2012

RDP has been switched to / replaced with the mothur classify.seqs tool

@author: asyed, andreopo
'''

from core.utils.rqc_logger import RQCLogger, Properties
import os, commands
from core.utils.rqc_16s_constants import SAG16sConstants


class RDPTool(object):
    '''
    classdocs
    '''
    _input = None
    _output = None
    _prop  = None
    _logger = None
    

    def __init__(self, inFile, outFile):
        '''
        Constructor
        '''
        self._input = inFile
        self._output = outFile
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("RDPTool")
        
    
    def run(self):
        if (self._input == None or self._output== None):
            self._logger.error("Input file or output file is not specified, so nothing done")
            return False
        
        if(not os.path.exists(self._input)):
            self._logger.error("Input file specified does not exist in the path. Nothing done. (file: "+self._input+")")
            return False
        
        try:
            f = open(self._output, "w")
            f.close()
        except Exception as e:
            self._logger.error("Output file is not writable. Nothing done. (file: "+self._output+")")
            return False
        #
        # make RDP command
        # rdp_cmd = ("java -jar /global/projectb/sandbox/rqc/andreopo/deploy/sag16s/prod/misc/software/rdp/rdp_classifier-2.3.jar  "+
        #           " -t /global/dna/shared/rqc/sag16s/repo/sag16s/rdp/rRNAClassifier.properties "+
        #           " -q "+self._input+" -o "+self._output)
        rdp_cmd = SAG16sConstants.MOTHUR_CLASSIFY_SEQS +  "\"#classify.seqs(fasta="+ self._input.replace("-", "\-") +", template=" + os.path.join( os.path.dirname( SAG16sConstants.MOTHUR_CLASSIFY_SEQS ) , "gg_13_5_99.fasta" ) + ", taxonomy=" + os.path.join( os.path.dirname( SAG16sConstants.MOTHUR_CLASSIFY_SEQS ) , "gg_13_5_99.gg.tax" ) + "); system(mv " +  self._input.replace(".fasta", ".gg.wang.taxonomy")  + " " + self._output +")\""
       # os.path.join( os.path.dirname( SAG16sConstants.MOTHUR_CLASSIFY_SEQS ) , os.path.basename(self._input).replace(".fasta", ".gg.wang.taxonomy") )


        outputTuple = None
        try:
            #
            # run RDP command and get status and output
            outputTuple = commands.getstatusoutput(rdp_cmd)
        except Exception as e:
            self._logger.error("Command FAILED. Cmd: "+rdp_cmd)
            outputTuple = (-1, "Failed")
            
        self._logger.debug("Command: "+rdp_cmd)
        
        #
        # status and output
        status = outputTuple[0]
        output = outputTuple[1]
        
        if( status == 0):
            self._logger.debug("RDP command SUCCESSFULLY completed.")
            return True
        else:
            self._logger.error("RDP command FAILED.")
            return False
        
        return True
        
        
