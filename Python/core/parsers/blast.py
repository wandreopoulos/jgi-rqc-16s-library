'''
Created on Nov 25, 2012

@author: asyed, andreopo
'''

from core.utils.rqc_logger import RQCLogger, Properties
from sqlalchemy.exc import IntegrityError, DatabaseError, SQLAlchemyError, InvalidRequestError
from core.dao.sag16s_top_hit import SAGTopHit
from sqlalchemy.orm import sessionmaker
from core.dao.base import ORMBase
from datetime import datetime
import os

class BlastParser:
    
    _blast_output = None
    _prop  = None
    _logger = None
    _parse_flag = False
    
    def __init__(self, outFile):
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("BlastParser")
        self._blast_output = outFile
    
    def parse(self):
        if(self._blast_output == None or not os.path.exists(self._blast_output)):
            self._logger.error("Blast output file is not specified or not existing, so nothing done")
            return False
        


class BlastTopHitParser:
    '''
    classdocs
    '''
    NOT_PARSED = 1
    SUCCESSFULLY_PARSED = 2
    NOTHING_TO_PARSE = 3
    FAILED_PARSING = 4
    
    _blast_output = None
    _prop  = None
    _logger = None
    _parse_flag = False
    
    # Top hit attributes
    _query_name = None
    _subject_id = None
    _per_id = 0.0
    _length = 0
    _mismatches = 0
    _gaps = 0
    _qstart = 0
    _qend = 0
    _sstart = 0
    _send = 0
    _eval = 0
    _score = 0
    
    _analysis_type = None
    _ssid = -1

    
    def __init__(self, outFile, analysisType, ssid):
        self._prop    = Properties()
        logger  = RQCLogger()
        self._logger = logger.getSag16sLogger("BlastParser")
        self._blast_output = outFile
        self._analysis_type = analysisType
        self._ssid = ssid
        self._logger.debug("BLASTTopHit Parser created")
        self._parse_flag = self.NOT_PARSED
    
    
    def parse(self):
        if(self._blast_output == None or not os.path.exists(self._blast_output)):
            self._logger.error("Blast output file is not specified or not existing, so nothing done")
            return False
        
        try:
            f = file(self._blast_output, "r")
            #f.readline()
            #f.readline()
            #f.readline()
            #f.readline()
            blastop = f.readline()
            while blastop.startswith("#"):
                blastop = f.readline()
            if blastop == None or blastop == "" or blastop == " ":
                self._logger.debug("Nothing to parse")
                self._parse_flag = self.NOTHING_TO_PARSE
                return True
            self._logger.debug("Blast Output: "+blastop+"Done");
            ary = blastop.split("\t")
            #
            # Now gather the parsed data
            # Example of line to be parsed:  46-1933.DeLong.HC20003.SmLr.02.D21.S77_F06_038  gi|392976362|gb|JQ248078.1|:::::UNCULTURED_BACTERIUM_CLONE_N_2_16S_RIBOSOMAL_RNA_GENE__PARTIAL_SEQUENCE N/A     7710.0     425     99.06   30      454     479     1       964     1388    1523    1
            self._query_name  = ary[0]
            self._subject_id = ary[1]
            self._per_id = float(ary[5])
            self._length = int(float(ary[4]))
            self._mismatches = 0
            self._gaps = 0
            self._qstart = int(float(ary[6]))
            self._qend = int(ary[7])
            self._sstart = int(ary[9])
            self._send = int(ary[10])
            self._eval = float(ary[3])
            self._score = 0
        except:
            self._logger.error("Parsing error parsing BLAST hit: "+blastop)
            self._parse_flag = self.FAILED_PARSING
            return False
        
        self._parse_flag = self.SUCCESSFULLY_PARSED
        return True
    
    
    '''
    This load is called for a Blast run against nt.
    The phylogeny hit is passed as argument.
    This load is also called for a Blast run against greengenes, in which case the phylogeny is None.
    The two cases are distinguished by self._analysis_type .
    '''
    def load(self, phylogeny):
        retVal = True
        if( self._parse_flag == self.NOT_PARSED):
            self._logger.error("Cannot load the top hit on " + str(self._analysis_type) + " without parsing the file.")
            return False

        if( self._parse_flag == self.FAILED_PARSING):
            self._logger.error("Cannot load the top hit on " + str(self._analysis_type) + " as parsing top hit output failed.")
            return False
            
        if( self._parse_flag == self.NOTHING_TO_PARSE):
            self._logger.error("Cannot load the top hit on " + str(self._analysis_type) + " without parsing the file")
            return True
        
        # here because the BLAST top hit file is successfully parsed SUCCESSFULLY_PARSED
        try:
            self._logger.info("Registering top hit on " + str(self._analysis_type) + " : "+self._subject_id+" for Query: "+self._query_name)
            Session = sessionmaker(bind = ORMBase.globalEngine)
            session = Session()
            seq = SAGTopHit(self._analysis_type, 
                            self._query_name,
                            self._ssid,
                            self._subject_id, 
                            self._per_id, 
                            self._length, 
                            self._mismatches, 
                            self._gaps, 
                            self._qstart, 
                            self._qend, 
                            self._sstart, 
                            self._send, 
                            self._eval,
                            self._score,
                            phylogeny,
                            self._blast_output, 
                            datetime.now())
            session.add(seq)
            session.commit()
            self._logger.info("Successfully registered top hit on " + str(self._analysis_type) + " : "+self._subject_id+" for Query: "+self._query_name)
        except IntegrityError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except DatabaseError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except SQLAlchemyError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        except InvalidRequestError as er:
            session.rollback()
            print "Exception ",er
            retVal  = False
        finally:
            if(session != None):
                session.close()
        
        # return to caller
        self._logger.debug("DB update complete on " + str(self._analysis_type) )
        return retVal




    def getParseCode(self):
        return self._parse_flag
    
