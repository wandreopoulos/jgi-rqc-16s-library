'''
This module is for RQC SAG16s sequence , Sequence status CV, and Sequence Status history ORM objects
Created on Nov 16, 2012

@author: asyed
'''

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.dialects.mysql import DATETIME, VARCHAR
from core.dao.base import ORMBase
from datetime import datetime

'''

'''
class SAGSequenceStatusCV(ORMBase.globalBase):
    SEQ_REGISTERED_STATUS_ID = 1
    SEQ_HOMOLOGY_SEARCH_IN_PROGRESS_STATUS_ID = 2
    SEQ_HOMOLOGY_SEARCH_FAILED_STATUS_ID = 3
    SEQ_HOMOLOGY_SEARCH_COMPLETE_STATUS_ID = 4
    SEQ_CONTAM_SEARCH_IN_PROGRESS_STATUS_ID = 5
    SEQ_CONTAM_SEARCH_FAILED_STATUS_ID = 6
    SEQ_CONTAM_SEARCH_COMPLETE_STATUS_ID = 7
    SEQ_RDP_IN_PROGRESS_STATUS_ID = 8
    SEQ_RDP_FAILED_STATUS_ID = 9
    SEQ_RDP_COMPLETE_STATUS_ID = 10
    SEQ_FINALIZED = 11
    SEQ_ON_HOLD_ID = 12
    SEQ_LOW_TRIM_LEN_HOLD_ID = 13
    #/projectb/projectdirs/PI/rqc

       
    __tablename__ = 'sag16s_seq_status_cv'
    __table_args__= {'mysql_engine':'InnoDB'} # PLEASE DONT REMOVE THIS, MySQL is configured by default as MyISAM on JGI-Debian
    ss_status_cv_id = Column(Integer, primary_key=True)
    name            = Column(VARCHAR(100), nullable=False, unique=True)
    description     = Column(VARCHAR(300), nullable=False)
    
    def __init__(self, sid, name, desc):
        self.ss_status_cv_id = sid
        self.name            = name
        self.description     = desc
        
    def __repr__(self):
        return "SAG Sequence Status CV Object"
    

class SAGSequence(ORMBase.globalBase):
    __tablename__ = 'sag16s_seq'
    __table_args__= {'mysql_engine':'InnoDB'} # PLEASE DONT REMOVE THIS, MySQL is configured by default as MyISAM on JGI-Debian
    sag_seq_id        = Column(Integer, primary_key=True)
    seq_name          = Column(VARCHAR(200), nullable=False, unique=True)
    ss_status_cv_id   = Column(Integer, ForeignKey('sag16s_seq_status_cv.ss_status_cv_id'))
    user_name         = Column(VARCHAR(20), nullable=False)
    proposal_id       = Column(VARCHAR(10), nullable=False)
    project           = Column(VARCHAR(10), nullable=False)
    sample            = Column(VARCHAR(10), nullable=False)
    sort_window       = Column(VARCHAR(10), nullable=False)
    plate_num         = Column(VARCHAR(10), nullable=False)
    pcr_plate         = Column(VARCHAR(30), nullable=False)
    mda_plate_well_pos= Column(VARCHAR(10), nullable=False)
    well_type         = Column(VARCHAR(3), nullable=False)
    sanger_well_pos   = Column(VARCHAR(3), nullable=False)
    sanger_well_loc   = Column(VARCHAR(3), nullable=False)
    well_position_col = Column(VARCHAR(20), nullable=False)
    q15_tlength       = Column(Integer, default=0)
    seq_name_marked   = Column(Integer, default=0)
    fs_location       = Column(VARCHAR(1024), nullable=False)
    run_type = Column(VARCHAR(10), default="CSP")
    dt_status_modified= Column(DATETIME, nullable=True)
    dt_data_modified  = Column(DATETIME, nullable=True, onupdate=datetime.now)
    retried_flag      = Column(Integer, default=0)
    email_sent_flag   = Column(Integer, default=0)




    def __init__(self, name, status, user, pid, proj, samp, sw, pn, pp, mdawp, wtype, swp, swl, wpos, q15, fsloc, rt, statusMod, dataMod):
        self.seq_name = name
        self.ss_status_cv_id = status
        self.user_name = user
        self.proposal_id = pid
        self.project = proj
        self.sample = samp
        self.sort_window = sw
        self.plate_num = pn
        self.pcr_plate = pp
        self.mda_plate_well_pos = mdawp
        self.well_type = wtype
        self.sanger_well_pos = swp
        self.sanger_well_loc = swl
        self.well_position_col = wpos
        self.q15_tlength = q15
        self.fs_location = fsloc
        self.run_type = rt
        self.dt_status_modified = statusMod
        self.dt_data_modified = dataMod
        self.retried_flag = 0
        self.email_sent_flag = 0
        

    #def getSeqPath(self):
    #    return self.fs_location+"/"+self.file_prefix+".fasta"
    
    
    def __repr__(self):
        return "Sag Seq ORM object"


class SAGSequenceHist(ORMBase.globalBase):
    __tablename__ = 'sag16s_seq_status_history'
    __table_args__= {'mysql_engine':'InnoDB'} # PLEASE DONT REMOVE THIS, MySQL is configured by default as MyISAM on JGI-Debian
    ss_status_history_id = Column(Integer, primary_key=True)
    ###sag_seq_id      = Column(Integer, ForeignKey('sag16s_seq.sag_seq_id'), nullable=False)
    sag_seq_name         = Column(VARCHAR(200), ForeignKey('sag16s_seq.seq_name'), nullable=False)
    ss_status_cv_id      = Column(Integer, nullable=False)
    dt_begin             = Column(DATETIME, nullable=False)
    dt_end               = Column(DATETIME, nullable=False)

    def __init__(self, seq_name, status, dtBegin, dtEnd):
        self.sag_seq_name = seq_name
        self.ss_status_cv_id = status
        self.dt_begin = dtBegin
        self.dt_end = dtEnd

    def __repr__(self):
        return "Physical Run Status History ORM"

#
# ORM base to create the tables
#ormBase = ORMBase()
#ormBase.getGlobalBase().metadata.create_all(ormBase.getGlobalEngine())
ORMBase.globalBase.metadata.create_all(ORMBase.globalEngine)



