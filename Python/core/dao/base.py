from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from core.utils.properties import Properties
from core.utils.rqc_logger import RQCLogger

'''

This module parses the respective Python configuration file/module and
loads up the database connection information and subsequently connects
to the database creating a GLOBAL DSN and GLOBAL declarative base.

These are subsequently used within the data access objects to actually
create the database ORMs and actually create the database table

SQLAlchemy MySQL DB Connection string looks like this:
    mysql+mysqldb://<user>:<password>@<host>[:<port>]/<dbname>

'''

class ORMBase(object):
    
    _instance = None
    _logger = None
    prop = Properties()
    dsn_source = 'mysql+mysqldb://'+prop.getDBUser()+":"+prop.getDBPass()+'@'+prop.getDBServer()+":3306"+'/'+prop.getDBName()
    globalEngine = create_engine(dsn_source, echo=False)
    globalBase = declarative_base()
    #globalEngine = None
    #globalBase   = None

    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        if ( cls._instance == None or not cls._instance ):
            cls._instance = super(ORMBase, cls).__new__(cls, *args, **kwargs)
            prop = Properties()
            cls._logger = RQCLogger().getGenericLogger("ORMBase")
            dsn_source = 'mysql+mysqldb://'+prop.getDBUser()+":"+prop.getDBPass()+'@'+prop.getDBServer()+":3306"+'/'+prop.getDBName()
            cls._logger.info("The DSN_Source for SQLAlchemy: "+dsn_source)
            cls.globalEngine = create_engine(dsn_source, echo=False)
            cls.globalBase = declarative_base()
        return cls._instance
    

    @staticmethod
    def getGlobalBase(self):
        return self.globalBase
    
    @staticmethod
    def getGlobalEngine(self):
        return self.globalEngine
   
 
### Python 3 ALERT ### Currently MySQLdb is only avialble through Python 2.7, a different connector must be used for Python 3

