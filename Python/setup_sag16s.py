import os
from setuptools import setup

setup(
      name='RQC.Sag16s',
      version='1.0',
      description='Rolling QC SAG16s  Software',
      author_email='ASyed@lbl.gov',
      packages=['16s','16s.pipeline','16s.ui'],
      package_data = { '': ['*.properties', '*.config', '*.entries']}
      )
