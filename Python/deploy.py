#!/usr/bin/env python

import os
import sys
import time
import getpass
from shutil import copyfile
from optparse import OptionParser
from subprocess import call
from shutil import rmtree



def getSWDestDir(userName):
    if userName == "qc_user":
        return "/global/projectb/sandbox/rqc/andreopo/sag16s_deploy/prod/jgi-rqc-16s-library/deploy/dev/software/sag16s/"
    else:
        return "/global/projectb/sandbox/rqc/andreopo/sag16s_deploy/dev/jgi-rqc-16s-library/deploy/dev/software/sag16s/"

def deploySag16s(sourceDir, userName, versionedDestDir):
    binDir = sourceDir+"/16s/pipeline/"
    versionedBinDir = versionedDestDir+"/bin/"
    try:
        if not os.path.exists(versionedBinDir):
            print "The deploy directory you provided DOES NOT EXIST creating new. Dir: "+versionedBinDir
            os.mkdir(versionedBinDir)
        if not os.path.isdir(versionedBinDir):
            print "The deploy directory provided is not a DIRECTORY. Check and try again! Dir: "+versionedBinDir
            sys.exit(1)
    except:
        print "Versioned BIN directory is not created"
        return False
    
    return True


def deployCore(keep, userName, source):
    '''
     Sanity check if the deploy directory exists and if it is a directory
    '''
    try:
        destDir =  getSWDestDir(userName)
        if not os.path.exists(destDir):
            print "The deploy directory you provided DOES NOT EXIST creating new. Dir: "+destDir
            os.mkdir(destDir)
        if not os.path.isdir(destDir):
            print "The deploy directory provided is not a DIRECTORY. Check and try again! Dir: "+destDir
            sys.exit(1)
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        sys.exit(1)
    #
    #get versioned directory
    #
    versionedDestDir = createVersionedDir(destDir)
    if(not versionedDestDir ):
        print "Versioned directory for CORE not created so exiting"
        sys.exit(1)
        
    '''
     Run the Setup.py script which will actually create the EGG file
    '''
    try:
        cmd = "module load python; python setup.py bdist_egg --bdist-dir build  --dist-dir deploy"
        print "Executing the Build command using setuptools COMMAND: "+cmd
        exit_code = call(cmd, shell=True)
        if(exit_code < 0):
            print >>sys.stderr, "Child was terminated by signal: ", -exit_code
        else:
            print >>sys.stderr, "Child returned EXIT code: ", exit_code
        #
        # Now COPY the Egg file to deploy directory
        listing = os.listdir("deploy")
        localEgg = "deploy/"+listing[0]
        prodEggPath = destDir+"/RQC.Core.Prod.Egg"
        distEggPath = versionedDestDir+listing[0]
        copyfile(localEgg, distEggPath)
        if os.path.exists(prodEggPath):
            os.remove(prodEggPath)
        os.symlink(distEggPath, prodEggPath)
        #
        # Now delete the deploy and Egg info directories
        if not keep :
            rmtree("deploy")
            rmtree("RQC.Core.egg-info")
    except OSError as e:
        print >>sys.stderr, "Execution failed:", e
    
    deploySag16s(source, userName, versionedDestDir)
    return True


def createVersionedDir(baseDir):
    
    '''
    Get the version of the Python from the Setup file
    '''
    try:
        versionsDir = baseDir+"Versions"
        print versionsDir
        if(not os.path.isdir(versionsDir)):
            print "The versions directory doesn't exist creating new. Dir: "+versionsDir
            os.mkdir(versionsDir)
        print "Now Versions dir existing"
        if not os.path.isdir(versionsDir):
            print "The Versions directory not created: "+versionsDir
            return False
        print "Now building the timestamped dir name"
        #
        # Versions directory already created
        ltime = time.localtime()
        timestamp=str(ltime.tm_year)+str(ltime.tm_mon)+str(ltime.tm_mday)+"_"+str(ltime.tm_hour)+str(ltime.tm_min)+str(ltime.tm_sec)
        versionedDestDir = versionsDir+"/"+timestamp+"/"
    except:
        print "Error occured while retrieving the version of the Software"
        return False
    try:
        if(not os.path.isdir(versionedDestDir)):
            print "The versioned destination directory doesnt exist creating new. Dir: "+versionedDestDir
            os.mkdir(versionedDestDir)
        if not os.path.isdir(versionedDestDir):
            print "The Versioned destination directory not created: "+versionedDestDir
            return False
    except:
        print "Error occured while creating versioned deploy directory"
        return False
    return versionedDestDir

if(__name__=="__main__"):
    '''
     Create Command line options parser object
    '''
    parser = OptionParser()
    '''
     Now add options that would be made available from the command line
    '''
#    parser.add_option("-c","--core-deploy-dir", dest="coreDeployDir", help="The directory where RQC.Core Egg file will be copied to..",metavar="DEPLOY-DIRECTORY-ABSOLUTE-PATH", type="string", action="store", default=False)
    parser.add_option("-c","--deploy-core", dest="deployCore", help="A flag that indicates if Core needs to be deployed",metavar="DEPLOY_CORE_FLAG", action="store_true", default=True)
    parser.add_option("-k", "--keep", dest="keepDirs", help="[DONT USE THIS UNLESS TESTING DEPLOY SCRIPT] Keep the temporarily created Egg-Info and local deploy/build directory", default=False)
    parser.add_option("-s", "--deploy-sag16s", dest="deploySag16s", action="store_true", help="A flag that indicates if Sag16s needs to be deployed", default=False)
    parser.add_option("-i", "--input-source-dir", dest="inputSource", action="store", help="Source FS location of the Python code base to be installed (hint: SANDBOX_LOC/rqc/Python)", default=None)
    
    '''
     Now actually parse the arguments
    '''
    (options, args) = parser.parse_args()
    source = options.inputSource
    core  = options.deployCore
    keepIntFiles = options.keepDirs
    sag   = options.deploySag16s
    #
    # Get user name
    userName = getpass.getuser()
    print "Running deployment script as " + userName
    
    if( source == None ):
        print "No input source chosen to be deployed. Nothing done."
        sys.exit(1)
    
    #    
    # Now deploy the RQC Core Egg
    if(not deployCore(keepIntFiles, userName, source)):
        print "\nERROR:\nUnable to Deploy RQC-Core library"
    else:
        print "\nSUCCESS:\nDeployed RQC-Core library"
    
    if( sag ):
        print "Deploying Sag16s pipeline now . . ."
        if not deploySag16s(keepIntFiles, userName):
            print "\nERROR:\nUnable to Deploy RQC-Sag16s library"
    else:
        print "Sag16s pipeline not opted for deployment, so skipping Sag16s deployment . . ."

